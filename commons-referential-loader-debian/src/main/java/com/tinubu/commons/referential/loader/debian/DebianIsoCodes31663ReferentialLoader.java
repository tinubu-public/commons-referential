/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.loader.debian;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.time.temporal.ChronoField.DAY_OF_MONTH;
import static java.time.temporal.ChronoField.MONTH_OF_YEAR;
import static java.time.temporal.ChronoField.YEAR;
import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.SignStyle;
import java.time.temporal.ChronoField;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tinubu.commons.referential.DefaultTranslatableName.DefaultTranslatableNameBuilder;
import com.tinubu.commons.referential.EntryDataLoader;
import com.tinubu.commons.referential.EntryDataLoadingException;
import com.tinubu.commons.referential.TranslatableName;
import com.tinubu.commons.referential.loader.debian.DebianIsoCodes31663ReferentialLoader.Iso31663Data.Iso31663DataBuilder;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2Code;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha3Code;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661NumericCode;
import com.tinubu.commons.referential.referentials.iso31663.Iso31663Alpha4;
import com.tinubu.commons.referential.referentials.iso31663.Iso31663Alpha4.Iso31663Alpha4Data;
import com.tinubu.commons.referential.referentials.iso31663.Iso31663Alpha4Code;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code;

/**
 * ISO 3166-3 {@link EntryDataLoader} from {@value DEFAULT_JSON_RESOURCE_PATH} data file from Debian
 * project.
 */
public class DebianIsoCodes31663ReferentialLoader
      implements EntryDataLoader<Iso31663Alpha4.Iso31663Alpha4Data> {

   private static final ObjectMapper OBJECT_MAPPER = objectMapper();

   private static final String DEFAULT_JSON_RESOURCE_PATH = "resources/iso_3166-3.json";

   /** JSON key used in Debian iso-codes definitions. */
   private static final String JSON_CONTENT_KEY = "3166-3";

   private final List<Iso31663Data> entryDatas;

   private DebianIsoCodes31663ReferentialLoader(List<Iso31663Data> entryDatas) {
      this.entryDatas = noNullElements(entryDatas, "entryDatas");
   }

   /**
    * Instantiates loader from {@value DEFAULT_JSON_RESOURCE_PATH} provided data file.
    *
    * @return loader instance
    */
   public static DebianIsoCodes31663ReferentialLoader fromDefaultSource() {
      return new DebianIsoCodes31663ReferentialLoader(loadDataFromDefaultSource());
   }

   /**
    * Instantiates loader from user-provided data source.
    *
    * @return loader instance
    */
   public static DebianIsoCodes31663ReferentialLoader fromSource(InputStream source) {
      notNull(source, "source");

      return new DebianIsoCodes31663ReferentialLoader(loadDataFromSource(source));
   }

   private static List<Iso31663Data> loadDataFromDefaultSource() {
      try (InputStream jsonStream = jsonContentStream()) {
         return loadDataFromSource(jsonStream);
      } catch (IOException e) {
         throw new EntryDataLoadingException(e.getMessage(), e);
      }
   }

   private static List<Iso31663Data> loadDataFromSource(InputStream source) {
      return parseJsonContent(source)
            .get(JSON_CONTENT_KEY)
            .stream()
            .map(c -> new Iso31663DataBuilder()
                  .iso31663Alpha4(Iso31663Alpha4Code.of(c.iso31663Alpha4))
                  .iso31661Alpha2(Iso31661Alpha2Code.of(c.iso31661Alpha2))
                  .iso31661Alpha3(Iso31661Alpha3Code.of(c.iso31661Alpha3))
                  .iso31661Numeric(nullable(c.iso31661Numeric).map(Iso31661NumericCode::of).orElse(null))
                  .name(new DefaultTranslatableNameBuilder().name(Iso6392Alpha3Code.ENG, c.name).build())
                  .withdrawalDate(LocalDate.parse(c.withdrawalDate, lenientLocalDateFormat()))
                  .build())
            .collect(toList());
   }

   private static DateTimeFormatter lenientLocalDateFormat() {
      return new DateTimeFormatterBuilder()
            .appendValue(YEAR, 4, 10, SignStyle.EXCEEDS_PAD)
            .optionalStart()
            .appendLiteral('-')
            .appendValue(MONTH_OF_YEAR, 2)
            .optionalStart()
            .appendLiteral('-')
            .appendValue(DAY_OF_MONTH, 2)
            .optionalEnd()
            .optionalEnd()
            .parseDefaulting(ChronoField.MONTH_OF_YEAR, 1)
            .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
            .toFormatter();
   }

   @Override
   public List<Iso31663Alpha4Data> loadEntryDatas() {
      try {
         return entryDatas.stream().map(Iso31663Alpha4Data.class::cast).collect(toList());
      } catch (EntryDataLoadingException e) {
         throw e;
      } catch (Exception e) {
         throw new EntryDataLoadingException(e.getMessage(), e);
      }

   }

   @Override
   public boolean hasChanged() {
      return false;
   }

   private static Map<String, List<JsonCountryData>> parseJsonContent(InputStream source) {
      try {
         return OBJECT_MAPPER.readValue(source, new TypeReference<Map<String, List<JsonCountryData>>>() {});
      } catch (IOException e) {
         throw new EntryDataLoadingException(e.getMessage(), e);
      }
   }

   private static InputStream jsonContentStream() {
      return DebianIsoCodes31663ReferentialLoader.class.getResourceAsStream(DEFAULT_JSON_RESOURCE_PATH);
   }

   private static ObjectMapper objectMapper() {
      return new ObjectMapper();
   }

   @JsonIgnoreProperties(ignoreUnknown = true)
   private static class JsonCountryData {
      private String iso31663Alpha4;
      private String iso31661Alpha2;
      private String iso31661Alpha3;
      private String iso31661Numeric;
      private String name;
      private String withdrawalDate;

      @JsonProperty("alpha_4")
      public String iso31663Alpha4() {
         return iso31663Alpha4;
      }

      @JsonProperty("alpha_2")
      public String iso31661Alpha2() {
         return iso31661Alpha2;
      }

      @JsonProperty("alpha_3")
      public String iso31661Alpha3() {
         return iso31661Alpha3;
      }

      @JsonProperty("numeric")
      public String iso31661Numeric() {
         return iso31661Numeric;
      }

      @JsonProperty("name")
      public String name() {
         return name;
      }

      @JsonProperty("withdrawal_date")
      public String withdrawalDate() {
         return withdrawalDate;
      }
   }

   static class Iso31663Data implements Iso31663Alpha4Data {

      private final Iso31663Alpha4Code iso31663Alpha4;
      private final Iso31661Alpha2Code iso31661Alpha2;
      private final Iso31661Alpha3Code iso31661Alpha3;
      private final Iso31661NumericCode iso31661Numeric;
      private final TranslatableName name;
      private final LocalDate withdrawalDate;

      private Iso31663Data(Iso31663DataBuilder builder) {
         this.iso31663Alpha4 = notNull(builder.iso31663Alpha4, "iso31663Alpha4");
         this.iso31661Alpha2 = notNull(builder.iso31661Alpha2, "iso31661Alpha2");
         this.iso31661Alpha3 = notNull(builder.iso31661Alpha3, "iso31661Alpha3");
         this.iso31661Numeric =
               nullable(builder.iso31661Numeric, numeric -> notNull(numeric, "iso31661Numeric"));
         this.name = notNull(builder.name, "name");
         this.withdrawalDate = notNull(builder.withdrawalDate, "withdrawalDate");
      }

      @Override
      public Iso31663Alpha4Code iso31663Alpha4() {
         return iso31663Alpha4;
      }

      @Override
      public Iso31661Alpha2Code iso31661Alpha2() {
         return iso31661Alpha2;
      }

      @Override
      public Iso31661Alpha3Code iso31661Alpha3() {
         return iso31661Alpha3;
      }

      @Override
      public Optional<Iso31661NumericCode> iso31661Numeric() {
         return nullable(iso31661Numeric);
      }

      @Override
      public TranslatableName name() {
         return name;
      }

      @Override
      public LocalDate withdrawalDate() {
         return withdrawalDate;
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", Iso31663Data.class.getSimpleName() + "[", "]")
               .add("iso31663Alpha4='" + iso31663Alpha4 + "'")
               .add("iso31661Alpha2='" + iso31661Alpha2 + "'")
               .add("iso31661Alpha3='" + iso31661Alpha3 + "'")
               .add("iso31661Numeric='" + iso31661Numeric + "'")
               .add("name='" + name + "'")
               .add("withdrawalDate='" + withdrawalDate + "'")
               .toString();
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         Iso31663Data that = (Iso31663Data) o;
         return Objects.equals(iso31663Alpha4, that.iso31663Alpha4)
                && Objects.equals(iso31661Alpha2,
                                  that.iso31661Alpha2)
                && Objects.equals(iso31661Alpha3, that.iso31661Alpha3)
                && Objects.equals(iso31661Numeric, that.iso31661Numeric)
                && Objects.equals(name, that.name)
                && Objects.equals(withdrawalDate, that.withdrawalDate);
      }

      @Override
      public int hashCode() {
         return Objects.hash(iso31663Alpha4,
                             iso31661Alpha2,
                             iso31661Alpha3,
                             iso31661Numeric,
                             name,
                             withdrawalDate);
      }

      static class Iso31663DataBuilder {
         private Iso31663Alpha4Code iso31663Alpha4;
         private Iso31661Alpha2Code iso31661Alpha2;
         private Iso31661Alpha3Code iso31661Alpha3;
         private Iso31661NumericCode iso31661Numeric;
         private TranslatableName name = new DefaultTranslatableNameBuilder().build();
         private LocalDate withdrawalDate;

         public Iso31663DataBuilder iso31663Alpha4(Iso31663Alpha4Code iso31663Alpha4) {
            this.iso31663Alpha4 = iso31663Alpha4;
            return this;
         }

         public Iso31663DataBuilder iso31661Alpha2(Iso31661Alpha2Code iso31661Alpha2) {
            this.iso31661Alpha2 = iso31661Alpha2;
            return this;
         }

         public Iso31663DataBuilder iso31661Alpha3(Iso31661Alpha3Code iso31661Alpha3) {
            this.iso31661Alpha3 = iso31661Alpha3;
            return this;
         }

         public Iso31663DataBuilder iso31661Numeric(Iso31661NumericCode iso31661Numeric) {
            this.iso31661Numeric = iso31661Numeric;
            return this;
         }

         public Iso31663DataBuilder name(TranslatableName name) {
            this.name = name;
            return this;
         }

         public Iso31663DataBuilder withdrawalDate(LocalDate withdrawalDate) {
            this.withdrawalDate = withdrawalDate;
            return this;
         }

         public Iso31663Data build() {
            return new Iso31663Data(this);
         }
      }
   }
}
