/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.loader.debian.providers;

import static com.tinubu.commons.referential.referentials.iso31661.Iso31661NumericReferential.DEFAULT_REFERENTIAL_CODE;

import com.tinubu.commons.referential.ReferentialCode;
import com.tinubu.commons.referential.ReferentialProvider;
import com.tinubu.commons.referential.loader.debian.DebianIsoCodes31661ReferentialLoader;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Numeric;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661NumericReferential;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661NumericReferential.Iso31661NumericReferentialBuilder;

public class DebianIso31661NumericReferentialProvider implements ReferentialProvider {

   @Override
   public ReferentialCode<Iso31661Numeric, Iso31661NumericReferential> referentialCode() {
      return DEFAULT_REFERENTIAL_CODE;
   }

   @Override
   public Class<Iso31661NumericReferential> referentialClass() {
      return Iso31661NumericReferential.class;
   }

   @Override
   public Iso31661NumericReferential build() {
      return new Iso31661NumericReferentialBuilder()
            .referentialCode(referentialCode())
            .referentialLoader(DebianIsoCodes31661ReferentialLoader
                                     .fromDefaultSource()
                                     .iso31661NumericDataLoader())
            .build();
   }
}
