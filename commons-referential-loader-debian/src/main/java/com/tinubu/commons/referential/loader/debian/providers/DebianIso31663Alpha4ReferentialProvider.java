/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.loader.debian.providers;

import static com.tinubu.commons.referential.referentials.iso31663.Iso31663Alpha4Referential.DEFAULT_REFERENTIAL_CODE;

import com.tinubu.commons.referential.ReferentialCode;
import com.tinubu.commons.referential.ReferentialProvider;
import com.tinubu.commons.referential.loader.debian.DebianIsoCodes31663ReferentialLoader;
import com.tinubu.commons.referential.referentials.iso31663.Iso31663Alpha4;
import com.tinubu.commons.referential.referentials.iso31663.Iso31663Alpha4Referential;
import com.tinubu.commons.referential.referentials.iso31663.Iso31663Alpha4Referential.Iso31663Alpha4ReferentialBuilder;

public class DebianIso31663Alpha4ReferentialProvider implements ReferentialProvider {

   @Override
   public ReferentialCode<Iso31663Alpha4, Iso31663Alpha4Referential> referentialCode() {
      return DEFAULT_REFERENTIAL_CODE;
   }

   @Override
   public Class<Iso31663Alpha4Referential> referentialClass() {
      return Iso31663Alpha4Referential.class;
   }

   @Override
   public Iso31663Alpha4Referential build() {
      return new Iso31663Alpha4ReferentialBuilder()
            .referentialCode(referentialCode())
            .referentialLoader(DebianIsoCodes31663ReferentialLoader.fromDefaultSource())
            .build();
   }
}
