/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.loader.debian;

import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.referential.referentials.IsoReferentials.IsoUserAssignedCodes.ISO_6392_ALPHA3_RESERVED_CODES;
import static com.tinubu.commons.referential.referentials.IsoReferentials.IsoUserAssignedCodes.alpha3Range;
import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.referential.DefaultEntryCodeMapping.DefaultEntryCodeMappingBuilder;
import com.tinubu.commons.referential.DefaultTranslatableName.DefaultTranslatableNameBuilder;
import com.tinubu.commons.referential.EntryCodeMapping;
import com.tinubu.commons.referential.EntryDataLoader;
import com.tinubu.commons.referential.EntryDataLoadingException;
import com.tinubu.commons.referential.ReferentialCode;
import com.tinubu.commons.referential.TranslatableName;
import com.tinubu.commons.referential.loader.debian.DebianIsoCodes639ReferentialLoader.Iso6392Data.Iso6392DataBuilder;
import com.tinubu.commons.referential.referentials.iso6391.Iso6391Alpha2;
import com.tinubu.commons.referential.referentials.iso6391.Iso6391Alpha2.Iso6391Alpha2Data;
import com.tinubu.commons.referential.referentials.iso6391.Iso6391Alpha2Code;
import com.tinubu.commons.referential.referentials.iso6391.Iso6391Alpha2Referential;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3.Iso6392Alpha3Data;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Referential;

/**
 * ISO 639-2 {@link EntryDataLoader} from {@value DEFAULT_JSON_RESOURCE_PATH} data file from Debian
 * project.
 * This loader also programmatically generates alpha-3 reserved codes defined in ISO.
 * <p>
 * This is a common loader for {@link Iso6391Alpha2Data} and {@link Iso6392Alpha3Data}, use respectively
 * {@link #iso6391Alpha2DataLoader()} and {@link #iso6392Alpha3DataLoader()} to access each specialized
 * loaders.
 */
public class DebianIsoCodes639ReferentialLoader {
   private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

   private static final String DEFAULT_JSON_RESOURCE_PATH = "resources/iso_639-2.json";

   /** JSON key used in Debian iso-codes definitions. */
   private static final String JSON_CONTENT_KEY = "639-2";

   private final List<Iso6392Data> entryDatas;

   private DebianIsoCodes639ReferentialLoader(List<Iso6392Data> entryDatas) {
      this.entryDatas = noNullElements(entryDatas, "entryDatas");
   }

   public static DebianIsoCodes639ReferentialLoader fromDefaultSource() {
      return new DebianIsoCodes639ReferentialLoader(concat(loadDataFromDefaultSource(),
                                                           iso6392Alpha3UserAssignedData()));
   }

   public static DebianIsoCodes639ReferentialLoader fromSource(InputStream source) {
      notNull(source, "source");

      return new DebianIsoCodes639ReferentialLoader(concat(loadDataFromSource(source),
                                                           iso6392Alpha3UserAssignedData()));
   }

   public EntryDataLoader<Iso6391Alpha2Data> iso6391Alpha2DataLoader() {
      return new EntryDataLoader<Iso6391Alpha2Data>() {

         @Override
         public List<Iso6391Alpha2Data> loadEntryDatas() {
            try {
               return DebianIsoCodes639ReferentialLoader.this.entryDatas
                     .stream()
                     .map(Iso6391Alpha2Data.class::cast)
                     .filter(entryData -> entryData
                           .entryCode(ReferentialCode.<Iso6391Alpha2, Iso6391Alpha2Referential>of(
                                 "ISO_6391_ALPHA2"))
                           .isPresent())
                     .collect(toList());
            } catch (EntryDataLoadingException e) {
               throw e;
            } catch (Exception e) {
               throw new EntryDataLoadingException(e.getMessage(), e);
            }
         }

         @Override
         public boolean hasChanged() {
            return DebianIsoCodes639ReferentialLoader.this.hasChanged();
         }
      };
   }

   public EntryDataLoader<Iso6392Alpha3Data> iso6392Alpha3DataLoader() {
      return new EntryDataLoader<Iso6392Alpha3Data>() {

         @Override
         public List<Iso6392Alpha3Data> loadEntryDatas() {
            return DebianIsoCodes639ReferentialLoader.this.entryDatas
                  .stream()
                  .map(Iso6392Alpha3Data.class::cast)
                  .filter(entryData -> entryData
                        .entryCode(ReferentialCode.<Iso6392Alpha3, Iso6392Alpha3Referential>of(
                              "ISO_6392_ALPHA3"))
                        .isPresent())
                  .collect(toList());
         }

         @Override
         public boolean hasChanged() {
            return DebianIsoCodes639ReferentialLoader.this.hasChanged();
         }
      };
   }

   private static List<Iso6392Data> loadDataFromDefaultSource() {
      try (InputStream jsonStream = jsonContentStream()) {
         return loadDataFromSource(jsonStream);
      } catch (IOException e) {
         throw new EntryDataLoadingException(e.getMessage(), e);
      }
   }

   private static List<Iso6392Data> loadDataFromSource(InputStream source) {
      return parseJsonContent(source)
            .get(JSON_CONTENT_KEY)
            .stream()
            .filter(c -> !c.iso6392Alpha3.equals("qaa-qtz"))
            .map(c -> {
               DefaultEntryCodeMappingBuilder mappingBuilder =
                     new DefaultEntryCodeMappingBuilder().mapping(ReferentialCode.<Iso6392Alpha3, Iso6392Alpha3Referential>of(
                           "ISO_6392_ALPHA3"), Iso6392Alpha3Code.of(c.iso6392Alpha3));
               if (c.iso6391Alpha2 != null) {
                  mappingBuilder =
                        mappingBuilder.mapping(ReferentialCode.<Iso6391Alpha2, Iso6391Alpha2Referential>of(
                              "ISO_6391_ALPHA2"), Iso6391Alpha2Code.of(c.iso6391Alpha2));
               }

               Iso6392DataBuilder data = new Iso6392DataBuilder()
                     .entryCodeMapping(mappingBuilder.build())
                     .name(new DefaultTranslatableNameBuilder().name(Iso6392Alpha3Code.ENG, c.name).build());

               return data.build();
            }).collect(toList());
   }

   public boolean hasChanged() {
      return false;
   }

   private static List<Iso6392Data> iso6392UserAssignedData() {
      return iso6392Alpha3UserAssignedData();
   }

   private static List<Iso6392Data> iso6392Alpha3UserAssignedData() {
      return iso6392Alpha3UserAssignedData(ISO_6392_ALPHA3_RESERVED_CODES);
   }

   private static List<Iso6392Data> iso6392Alpha3UserAssignedData(List<Pair<Iso6392Alpha3Code, Iso6392Alpha3Code>> iso6392Alpha3UserAssignedRanges) {
      List<Iso6392Data> userAssigned = new ArrayList<>();

      for (Pair<Iso6392Alpha3Code, Iso6392Alpha3Code> userAssignedRange : iso6392Alpha3UserAssignedRanges) {
         alpha3Range(userAssignedRange.getLeft().entryCode(),
                     userAssignedRange.getRight().entryCode(),
                     'a',
                     'z')
               .stream()
               .map(c -> new Iso6392DataBuilder()
                     .entryCodeMapping(new DefaultEntryCodeMappingBuilder()
                                             .mapping(ReferentialCode.<Iso6392Alpha3, Iso6392Alpha3Referential>of(
                                                   "ISO_6392_ALPHA3"), Iso6392Alpha3Code.of(c))
                                             .build())
                     .build())
               .forEach(userAssigned::add);
      }

      return userAssigned;
   }

   private static Map<String, List<JsonData>> parseJsonContent(InputStream source) {
      try {
         return OBJECT_MAPPER.readValue(source, new TypeReference<Map<String, List<JsonData>>>() {});
      } catch (IOException e) {
         throw new EntryDataLoadingException(e.getMessage(), e);
      }
   }

   private static InputStream jsonContentStream() {
      return DebianIsoCodes639ReferentialLoader.class.getResourceAsStream(DEFAULT_JSON_RESOURCE_PATH);
   }

   private static <T> List<T> concat(List<T> list1, List<T> list2) {
      return Stream.concat(list1.stream(), list2.stream()).collect(toList());
   }

   @JsonIgnoreProperties(ignoreUnknown = true)
   private static class JsonData {
      private String iso6391Alpha2;
      private String iso6392Alpha3;
      private String name;

      @JsonProperty("alpha_2")
      public String iso6391Alpha2() {
         return iso6391Alpha2;
      }

      @JsonProperty("alpha_3")
      public String iso6392Alpha3() {
         return iso6392Alpha3;
      }

      @JsonProperty("name")
      public String name() {
         return name;
      }
   }

   static class Iso6392Data implements Iso6391Alpha2Data, Iso6392Alpha3Data {

      private final TranslatableName name;
      private final EntryCodeMapping entryCodeMapping;

      private Iso6392Data(Iso6392DataBuilder builder) {
         this.name = notNull(builder.name, "name");
         this.entryCodeMapping = notNull(builder.entryCodeMapping, "entryCodeMapping");
      }

      public TranslatableName name() {
         return name;
      }

      @Override
      public EntryCodeMapping entryCodeMapping() {
         return entryCodeMapping;
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", Iso6392Data.class.getSimpleName() + "[", "]")
               .add("name='" + name + "'")
               .add("entryCodeMapping='" + entryCodeMapping + "'")
               .toString();
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         Iso6392Data that = (Iso6392Data) o;
         return Objects.equals(name, that.name) && Objects.equals(entryCodeMapping, that.entryCodeMapping);
      }

      @Override
      public int hashCode() {
         return Objects.hash(name, entryCodeMapping);
      }

      static class Iso6392DataBuilder {
         private TranslatableName name = new DefaultTranslatableNameBuilder().build();
         private EntryCodeMapping entryCodeMapping = new DefaultEntryCodeMappingBuilder().build();

         public Iso6392DataBuilder name(TranslatableName name) {
            this.name = name;
            return this;
         }

         public Iso6392DataBuilder entryCodeMapping(EntryCodeMapping entryCodeMapping) {
            this.entryCodeMapping = entryCodeMapping;
            return this;
         }

         public Iso6392Data build() {
            return new Iso6392Data(this);
         }
      }
   }
}
