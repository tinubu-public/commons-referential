/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.loader.debian;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.referential.referentials.IsoReferentials.IsoUserAssignedCodes.ISO_31661_ALPHA2_USER_ASSIGNED_RANGES;
import static com.tinubu.commons.referential.referentials.IsoReferentials.IsoUserAssignedCodes.ISO_31661_ALPHA3_USER_ASSIGNED_RANGES;
import static com.tinubu.commons.referential.referentials.IsoReferentials.IsoUserAssignedCodes.alpha2Range;
import static com.tinubu.commons.referential.referentials.IsoReferentials.IsoUserAssignedCodes.alpha3Range;
import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.referential.DefaultEntryCodeMapping.DefaultEntryCodeMappingBuilder;
import com.tinubu.commons.referential.DefaultTranslatableName.DefaultTranslatableNameBuilder;
import com.tinubu.commons.referential.EntryCodeMapping;
import com.tinubu.commons.referential.EntryDataLoader;
import com.tinubu.commons.referential.EntryDataLoadingException;
import com.tinubu.commons.referential.ReferentialCode;
import com.tinubu.commons.referential.TranslatableName;
import com.tinubu.commons.referential.loader.debian.DebianIsoCodes31661ReferentialLoader.Iso31661Data.Iso31661DataBuilder;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2.Iso31661Alpha2Data;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2Code;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2Referential;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha3;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha3.Iso31661Alpha3Data;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha3Code;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha3Referential;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Numeric;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Numeric.Iso31661NumericData;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661NumericCode;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661NumericReferential;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code;

/**
 * ISO 3166-1 {@link EntryDataLoader} from {@value DEFAULT_JSON_RESOURCE_PATH} data file from Debian
 * project.
 * This loader also programmatically generates alpha-2/alpha-3 user-assigned codes defined in ISO.
 * <p>
 * This is a common loader for {@link Iso31661Alpha2Data}, {@link Iso31661Alpha3Data} and {@link
 * Iso31661NumericData}, use respectively {@link #iso31661Alpha2DataLoader()}, {@link
 * #iso31661Alpha3DataLoader()} and {@link #iso31661NumericDataLoader()} to access each specialized loaders.
 */
public class DebianIsoCodes31661ReferentialLoader {
   private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

   private static final String DEFAULT_JSON_RESOURCE_PATH = "resources/iso_3166-1.json";

   /** JSON key used in Debian iso-codes definitions. */
   private static final String JSON_CONTENT_KEY = "3166-1";

   private final List<Iso31661Data> entryDatas;

   private DebianIsoCodes31661ReferentialLoader(List<Iso31661Data> entryDatas) {
      this.entryDatas = noNullElements(entryDatas, "entryDatas");
   }

   public static DebianIsoCodes31661ReferentialLoader fromDefaultSource() {
      return new DebianIsoCodes31661ReferentialLoader(concat(loadDataFromDefaultSource(),
                                                             iso31661UserAssignedData()));
   }

   public static DebianIsoCodes31661ReferentialLoader fromSource(InputStream source) {
      notNull(source, "source");

      return new DebianIsoCodes31661ReferentialLoader(concat(loadDataFromSource(source),
                                                             iso31661UserAssignedData()));
   }

   public EntryDataLoader<Iso31661Alpha2Data> iso31661Alpha2DataLoader() {
      return new EntryDataLoader<Iso31661Alpha2Data>() {

         @Override
         public List<Iso31661Alpha2Data> loadEntryDatas() {
            try {
               return DebianIsoCodes31661ReferentialLoader.this.entryDatas
                     .stream()
                     .map(Iso31661Alpha2Data.class::cast)
                     .filter(entryData -> entryData
                           .entryCode(ReferentialCode.<Iso31661Alpha2, Iso31661Alpha2Referential>of(
                                 "ISO_31661_ALPHA2"))
                           .isPresent())
                     .collect(toList());
            } catch (EntryDataLoadingException e) {
               throw e;
            } catch (Exception e) {
               throw new EntryDataLoadingException(e.getMessage(), e);
            }
         }

         @Override
         public boolean hasChanged() {
            return DebianIsoCodes31661ReferentialLoader.this.hasChanged();
         }
      };
   }

   public EntryDataLoader<Iso31661Alpha3Data> iso31661Alpha3DataLoader() {
      return new EntryDataLoader<Iso31661Alpha3Data>() {

         @Override
         public List<Iso31661Alpha3Data> loadEntryDatas() {
            try {
               return DebianIsoCodes31661ReferentialLoader.this.entryDatas
                     .stream()
                     .map(Iso31661Alpha3Data.class::cast)
                     .filter(entryData -> entryData
                           .entryCode(ReferentialCode.<Iso31661Alpha3, Iso31661Alpha3Referential>of(
                                 "ISO_31661_ALPHA3"))
                           .isPresent())
                     .collect(toList());
            } catch (EntryDataLoadingException e) {
               throw e;
            } catch (Exception e) {
               throw new EntryDataLoadingException(e.getMessage(), e);
            }
         }

         @Override
         public boolean hasChanged() {
            return DebianIsoCodes31661ReferentialLoader.this.hasChanged();
         }
      };
   }

   public EntryDataLoader<Iso31661NumericData> iso31661NumericDataLoader() {
      return new EntryDataLoader<Iso31661NumericData>() {

         @Override
         public List<Iso31661NumericData> loadEntryDatas() {
            try {
               return DebianIsoCodes31661ReferentialLoader.this.entryDatas
                     .stream()
                     .map(Iso31661NumericData.class::cast)
                     .filter(entryData -> entryData
                           .entryCode(ReferentialCode.<Iso31661Numeric, Iso31661NumericReferential>of(
                                 "ISO_31661_NUMERIC"))
                           .isPresent())
                     .collect(toList());
            } catch (EntryDataLoadingException e) {
               throw e;
            } catch (Exception e) {
               throw new EntryDataLoadingException(e.getMessage(), e);
            }
         }

         @Override
         public boolean hasChanged() {
            return DebianIsoCodes31661ReferentialLoader.this.hasChanged();
         }
      };
   }

   private static List<Iso31661Data> loadDataFromDefaultSource() {
      try (InputStream jsonStream = jsonContentStream()) {
         return loadDataFromSource(jsonStream);
      } catch (IOException e) {
         throw new EntryDataLoadingException(e.getMessage(), e);
      }
   }

   private static List<Iso31661Data> loadDataFromSource(InputStream source) {
      return parseJsonContent(source)
            .get(JSON_CONTENT_KEY)
            .stream()
            .map(c -> new Iso31661DataBuilder()
                  .iso31661Alpha2(Iso31661Alpha2Code.of(c.iso31661Alpha2))
                  .iso31661Alpha3(Iso31661Alpha3Code.of(c.iso31661Alpha3))
                  .iso31661Numeric(Iso31661NumericCode.of(c.iso31661Numeric))
                  .name(new DefaultTranslatableNameBuilder().name(Iso6392Alpha3Code.ENG, c.name).build())
                  .commonName(new DefaultTranslatableNameBuilder()
                                    .name(Iso6392Alpha3Code.ENG, c.commonName)
                                    .build())
                  .officialName(new DefaultTranslatableNameBuilder()
                                      .name(Iso6392Alpha3Code.ENG, c.officialName)
                                      .build()).build()).collect(toList());
   }

   public boolean hasChanged() {
      return false;
   }

   private static List<Iso31661Data> iso31661UserAssignedData() {
      return Stream
            .concat(iso31661Alpha2UserAssignedData().stream(), iso31661Alpha3UserAssignedData().stream())
            .collect(toList());
   }

   private static List<Iso31661Data> iso31661Alpha2UserAssignedData() {
      return iso31661Alpha2UserAssignedData(ISO_31661_ALPHA2_USER_ASSIGNED_RANGES);
   }

   private static List<Iso31661Data> iso31661Alpha2UserAssignedData(List<Pair<Iso31661Alpha2Code, Iso31661Alpha2Code>> iso31661Alpha2UserAssignedRanges) {
      List<Iso31661Data> userAssigned = new ArrayList<>();

      for (Pair<Iso31661Alpha2Code, Iso31661Alpha2Code> userAssignedRange : iso31661Alpha2UserAssignedRanges) {
         alpha2Range(userAssignedRange.getLeft().entryCode(),
                     userAssignedRange.getRight().entryCode(),
                     'A',
                     'Z')
               .stream()
               .map(c -> new Iso31661DataBuilder().iso31661Alpha2(Iso31661Alpha2Code.of(c)).build())
               .forEach(userAssigned::add);
      }

      return userAssigned;
   }

   private static List<Iso31661Data> iso31661Alpha3UserAssignedData() {
      return iso31661Alpha3UserAssignedData(ISO_31661_ALPHA3_USER_ASSIGNED_RANGES);
   }

   private static List<Iso31661Data> iso31661Alpha3UserAssignedData(List<Pair<Iso31661Alpha3Code, Iso31661Alpha3Code>> iso31661Alpha3UserAssignedRanges) {
      List<Iso31661Data> userAssigned = new ArrayList<>();

      for (Pair<Iso31661Alpha3Code, Iso31661Alpha3Code> userAssignedRange : iso31661Alpha3UserAssignedRanges) {
         alpha3Range(userAssignedRange.getLeft().entryCode(),
                     userAssignedRange.getRight().entryCode(),
                     'A',
                     'Z')
               .stream()
               .map(c -> new Iso31661DataBuilder().iso31661Alpha3(Iso31661Alpha3Code.of(c)).build())
               .forEach(userAssigned::add);
      }

      return userAssigned;
   }

   private static Map<String, List<JsonData>> parseJsonContent(InputStream source) {
      try {
         return OBJECT_MAPPER.readValue(source, new TypeReference<Map<String, List<JsonData>>>() {});
      } catch (IOException e) {
         throw new EntryDataLoadingException(e.getMessage(), e);
      }
   }

   private static InputStream jsonContentStream() {
      return DebianIsoCodes31661ReferentialLoader.class.getResourceAsStream(DEFAULT_JSON_RESOURCE_PATH);
   }

   private static <T> List<T> concat(List<T> list1, List<T> list2) {
      return Stream.concat(list1.stream(), list2.stream()).collect(toList());
   }

   @JsonIgnoreProperties(ignoreUnknown = true)
   private static class JsonData {
      private String iso31661Alpha2;
      private String iso31661Alpha3;
      private String iso31661Numeric;
      private String name;
      private String commonName;
      private String officialName;

      @JsonProperty("alpha_2")
      public String iso31661Alpha2() {
         return iso31661Alpha2;
      }

      @JsonProperty("alpha_3")
      public String iso31661Alpha3() {
         return iso31661Alpha3;
      }

      @JsonProperty("numeric")
      public String iso31661Numeric() {
         return iso31661Numeric;
      }

      @JsonProperty("name")
      public String name() {
         return name;
      }

      @JsonProperty("common_name")
      public String commonName() {
         return commonName;
      }

      @JsonProperty("official_name")
      public String officialName() {
         return officialName;
      }
   }

   static class Iso31661Data implements Iso31661Alpha2Data, Iso31661Alpha3Data, Iso31661NumericData {

      private final Iso31661Alpha2Code iso31661Alpha2Code;
      private final Iso31661Alpha3Code iso31661Alpha3Code;
      private final Iso31661NumericCode iso31661NumericCode;
      private final TranslatableName name;
      private final TranslatableName commonName;
      private final TranslatableName officialName;
      private final EntryCodeMapping entryCodeMapping;

      private Iso31661Data(Iso31661DataBuilder builder) {
         this.iso31661Alpha2Code = builder.iso31661Alpha2;
         this.iso31661Alpha3Code = builder.iso31661Alpha3;
         this.iso31661NumericCode = builder.iso31661Numeric;
         this.name = notNull(builder.name, "name");
         this.commonName = notNull(builder.commonName, "commonName").delegate(name);
         this.officialName = notNull(builder.officialName, "officialName").delegate(name);

         this.entryCodeMapping = new DefaultEntryCodeMappingBuilder() {{
            nullable(iso31661Alpha2Code).ifPresent(ec -> mapping(ReferentialCode.<Iso31661Alpha2, Iso31661Alpha2Referential>of(
                  "ISO_31661_ALPHA2"), ec));
            nullable(iso31661Alpha3Code).ifPresent(ec -> mapping(ReferentialCode.<Iso31661Alpha3, Iso31661Alpha3Referential>of(
                  "ISO_31661_ALPHA3"), ec));
            nullable(iso31661NumericCode).ifPresent(ec -> mapping(ReferentialCode.<Iso31661Numeric, Iso31661NumericReferential>of(
                  "ISO_31661_NUMERIC"), ec));
         }}.build();
      }

      @Override
      public Iso31661Alpha2Code iso31661Alpha2() {
         return nullable(iso31661Alpha2Code).orElseThrow(() -> new IllegalStateException(
               "iso31661Alpha2Code must not be null"));
      }

      @Override
      public Iso31661Alpha3Code iso31661Alpha3() {
         return nullable(iso31661Alpha3Code).orElseThrow(() -> new IllegalStateException(
               "iso31661Alpha3Code must not be null"));
      }

      @Override
      public Iso31661NumericCode iso31661Numeric() {
         return nullable(iso31661NumericCode).orElseThrow(() -> new IllegalStateException(
               "iso31661NumericCode must not be null"));
      }

      @Override
      public TranslatableName name() {
         return name;
      }

      @Override
      public TranslatableName commonName() {
         return commonName;
      }

      @Override
      public TranslatableName officialName() {
         return officialName;
      }

      @Override
      public EntryCodeMapping entryCodeMapping() {
         return entryCodeMapping;
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         Iso31661Data that = (Iso31661Data) o;
         return Objects.equals(iso31661Alpha2Code, that.iso31661Alpha2Code)
                && Objects.equals(iso31661Alpha3Code, that.iso31661Alpha3Code)
                && Objects.equals(iso31661NumericCode, that.iso31661NumericCode)
                && Objects.equals(name, that.name)
                && Objects.equals(commonName, that.commonName)
                && Objects.equals(officialName, that.officialName)
                && Objects.equals(entryCodeMapping, that.entryCodeMapping);
      }

      @Override
      public int hashCode() {
         return Objects.hash(iso31661Alpha2Code,
                             iso31661Alpha3Code,
                             iso31661NumericCode,
                             name,
                             commonName,
                             officialName,
                             entryCodeMapping);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", Iso31661Data.class.getSimpleName() + "[", "]")
               .add("iso31661Alpha2=" + iso31661Alpha2Code)
               .add("iso31661Alpha3=" + iso31661Alpha3Code)
               .add("iso31661Numeric=" + iso31661NumericCode)
               .add("name='" + name + "'")
               .add("commonName='" + commonName + "'")
               .add("officialName='" + officialName + "'")
               .add("entryCodeMapping=" + entryCodeMapping)
               .toString();
      }

      static class Iso31661DataBuilder {
         private Iso31661Alpha2Code iso31661Alpha2;
         private Iso31661Alpha3Code iso31661Alpha3;
         private Iso31661NumericCode iso31661Numeric;
         private TranslatableName name = new DefaultTranslatableNameBuilder().build();
         private TranslatableName commonName = new DefaultTranslatableNameBuilder().build();
         private TranslatableName officialName = new DefaultTranslatableNameBuilder().build();

         public Iso31661DataBuilder iso31661Alpha2(Iso31661Alpha2Code iso31661Alpha2) {
            this.iso31661Alpha2 = iso31661Alpha2;
            return this;
         }

         public Iso31661DataBuilder iso31661Alpha3(Iso31661Alpha3Code iso31661Alpha3) {
            this.iso31661Alpha3 = iso31661Alpha3;
            return this;
         }

         public Iso31661DataBuilder iso31661Numeric(Iso31661NumericCode iso31661Numeric) {
            this.iso31661Numeric = iso31661Numeric;
            return this;
         }

         public Iso31661DataBuilder name(TranslatableName name) {
            this.name = name;
            return this;
         }

         public Iso31661DataBuilder commonName(TranslatableName commonName) {
            this.commonName = commonName;
            return this;
         }

         public Iso31661DataBuilder officialName(TranslatableName officialName) {
            this.officialName = officialName;
            return this;
         }

         public Iso31661Data build() {
            return new Iso31661Data(this);
         }
      }
   }
}
