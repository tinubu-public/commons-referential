/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.loader.debian;

import static com.tinubu.commons.referential.Referentials.clearRegisteredReferentials;
import static com.tinubu.commons.referential.referentials.IsoReferentials.iso31663Alpha4Referential;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.referential.EntryCode;
import com.tinubu.commons.referential.ReferentialProvider;
import com.tinubu.commons.referential.Referentials;
import com.tinubu.commons.referential.UnknownEntryException;
import com.tinubu.commons.referential.loader.debian.providers.DebianIso31663Alpha4ReferentialProvider;
import com.tinubu.commons.referential.referentials.IsoReferentials;
import com.tinubu.commons.referential.referentials.iso31663.Iso31663Alpha4Code;
import com.tinubu.commons.referential.referentials.iso31663.Iso31663Alpha4Referential;

public class Iso31663Alpha4ReferentialTest {

   @BeforeAll
   public static void registerTestReferentials() {
      clearRegisteredReferentials();
      Referentials.registerReferential(new DebianIso31663Alpha4ReferentialProvider());
   }

   @Test
   @Disabled("Manual test to generate Java code for Iso31663Alpha4Code constants")
   public void generateConstantsCode() {
      ReferentialProvider referentialProvider = new DebianIso31663Alpha4ReferentialProvider();

      System.out.println(String.format("/* Generated codes at %s using '%s' provider. */",
                                       OffsetDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                                       referentialProvider.getClass().getName()));

      referentialProvider
            .build()
            .entries()
            .stream()
            .map(com.tinubu.commons.referential.Entry::entryCode)
            .sorted()
            .forEach(entryCode -> System.out.println(String.format(
                  "public static final Iso31663Alpha4Code %1$s = Iso31663Alpha4Code.of(\"%2$s\");",
                  normalizeFieldName(entryCode),
                  entryCode.entryCode())));

   }

   @Test
   public void testGeneratedConstantsCodeUpToDate() {
      List<String> entryCodes = IsoReferentials
            .iso31663Alpha4Referential()
            .entries()
            .stream()
            .map(com.tinubu.commons.referential.Entry::entryCode)
            .map(this::normalizeFieldName)
            .collect(toList());

      List<String> constantCodes = constantFields().map(Field::getName).collect(toList());

      assertThat(constantCodes).containsExactlyInAnyOrderElementsOf(entryCodes);
   }

   @Test
   public void testInstantiateReferentialWhenNominal() {
      Iso31663Alpha4Referential referential = iso31663Alpha4Referential();

      assertThat(referential.displayName()).isEqualTo("ISO 3166-3 alpha-4");
   }

   @Test
   public void testEntries() {
      Iso31663Alpha4Referential referential = iso31663Alpha4Referential();

      assertThat(referential.entries()).hasSizeGreaterThan(0);
   }

   @Nested
   public class Entry {

      @Test
      public void testEntryWhenNominal() {
         Iso31663Alpha4Referential referential = iso31663Alpha4Referential();

         assertThat(referential.entry("AIDJ")).isNotNull();
      }

      @Test
      public void testEntryWhenNotFound() {
         Iso31663Alpha4Referential referential = iso31663Alpha4Referential();

         assertThatExceptionOfType(UnknownEntryException.class).isThrownBy(() -> referential.entry("ZZZZ")).withMessage("Unknown 'ZZZZ' entry code in 'ISO_31663_ALPHA4' referential");
      }

      @Test
      public void testEntryWhenBadParameters() {
         Iso31663Alpha4Referential referential = iso31663Alpha4Referential();

         assertThatNullPointerException().isThrownBy(() -> referential.entry((String) null)).withMessage("'entryCode' must not be null");
         assertThatIllegalArgumentException().isThrownBy(() -> referential.entry("")).withMessage("'entryCode' must not be blank");
      }

      @Test
      public void testOptionalEntryWhenNominal() {
         Iso31663Alpha4Referential referential = iso31663Alpha4Referential();

         assertThat(referential.optionalEntry("AIDJ")).isPresent();
      }

      @Test
      public void testOptionalEntryWhenNotFound() {
         Iso31663Alpha4Referential referential = iso31663Alpha4Referential();

         assertThat(referential.optionalEntry("ZZZZ")).isEmpty();
      }

      @Test
      public void testOptionalEntryWhenBadParameters() {
         Iso31663Alpha4Referential referential = iso31663Alpha4Referential();

         assertThatNullPointerException().isThrownBy(() -> referential.optionalEntry((String) null)).withMessage("'entryCode' must not be null");
         assertThatIllegalArgumentException().isThrownBy(() -> referential.optionalEntry("")).withMessage("'entryCode' must not be blank");
      }

   }

   private String normalizeFieldName(EntryCode entryCode) {
      return entryCode.entryCode().toUpperCase().replaceAll("[^A-Z0-9]", "_").replaceAll("^([0-9]{1})", "_$1");
   }

   private Stream<Field> constantFields() {
      Predicate<Field> constantField = f -> {
         int modifiers = f.getModifiers();
         return Modifier.isPublic(modifiers) && Modifier.isStatic(modifiers) && Modifier.isFinal(modifiers);
      };

      return Stream.of(Iso31663Alpha4Code.class.getDeclaredFields()).filter(constantField);
   }

}
