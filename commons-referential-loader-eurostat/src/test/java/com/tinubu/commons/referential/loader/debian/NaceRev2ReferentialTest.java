/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.loader.debian;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.referential.EntryCode;
import com.tinubu.commons.referential.ReferentialProvider;
import com.tinubu.commons.referential.UnknownEntryException;
import com.tinubu.commons.referential.loader.eurostat.providers.EurostatNaceRev2ReferentialProvider;
import com.tinubu.commons.referential.referentials.naceRev2.NaceRev2;
import com.tinubu.commons.referential.referentials.naceRev2.NaceRev2Code;
import com.tinubu.commons.referential.referentials.naceRev2.NaceRev2Referential;

public class NaceRev2ReferentialTest {

   @Test
   @Disabled("Manual test to generate Java code for NaceRev2Code constants")
   public void generateConstantsCode() {
      ReferentialProvider referentialProvider = new EurostatNaceRev2ReferentialProvider();

      System.out.println(String.format("/* Generated codes at %s using '%s' provider. */",
                                       OffsetDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                                       referentialProvider.getClass().getName()));
      referentialProvider
            .build()
            .entries()
            .stream()
            .map(com.tinubu.commons.referential.Entry::entryCode)
            .sorted()
            .forEach(entryCode -> System.out.println(String.format(
                  "public static final NaceRev2Code %1$s = NaceRev2Code.of(\"%2$s\");",
                  normalizeFieldName(entryCode),
                  entryCode.entryCode())));

   }

   @Test
   public void testGeneratedConstantsCodeUpToDate() {
      List<String> entryCodes = new EurostatNaceRev2ReferentialProvider()
            .build()
            .entries()
            .stream()
            .map(NaceRev2::entryCode)
            .map(this::normalizeFieldName)
            .collect(toList());

      List<String> constantCodes = constantFields().map(Field::getName).collect(toList());

      assertThat(constantCodes).containsExactlyInAnyOrderElementsOf(entryCodes);
   }

   @Test
   public void testInstantiateReferentialWhenNominal() {
      NaceRev2Referential referential = new EurostatNaceRev2ReferentialProvider().build();

      assertThat(referential.displayName()).isEqualTo("NACE Rev.2");
   }

   @Nested
   public class Entry {

      @Test
      public void testEntryWhenNominal() {
         NaceRev2Referential referential = new EurostatNaceRev2ReferentialProvider().build();

         assertThat(referential.entry("01")).isNotNull();
      }

      @Test
      public void testEntryWhenNotFound() {
         NaceRev2Referential referential = new EurostatNaceRev2ReferentialProvider().build();

         assertThatExceptionOfType(UnknownEntryException.class)
               .isThrownBy(() -> referential.entry("ql"))
               .withMessage("Unknown 'ql' entry code in 'NACE_REV2' referential");
      }

      @Test
      public void testEntryWhenBadParameters() {
         NaceRev2Referential referential = new EurostatNaceRev2ReferentialProvider().build();

         assertThatNullPointerException()
               .isThrownBy(() -> referential.entry((String) null))
               .withMessage("'entryCode' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> referential.entry(""))
               .withMessage("'entryCode' must not be blank");
      }

      @Test
      public void testOptionalEntryWhenNominal() {
         NaceRev2Referential referential = new EurostatNaceRev2ReferentialProvider().build();

         assertThat(referential.optionalEntry("01")).isPresent();
      }

      @Test
      public void testOptionalEntryWhenNotFound() {
         NaceRev2Referential referential = new EurostatNaceRev2ReferentialProvider().build();

         assertThat(referential.optionalEntry("ql")).isEmpty();
      }

      @Test
      public void testOptionalEntryWhenBadParameters() {
         NaceRev2Referential referential = new EurostatNaceRev2ReferentialProvider().build();

         assertThatNullPointerException()
               .isThrownBy(() -> referential.optionalEntry((String) null))
               .withMessage("'entryCode' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> referential.optionalEntry(""))
               .withMessage("'entryCode' must not be blank");
      }

   }

   private String normalizeFieldName(EntryCode entryCode) {
      return entryCode
            .entryCode()
            .toUpperCase()
            .replaceAll("[^A-Z0-9]", "_")
            .replaceAll("^([0-9]{1})", "_$1");
   }

   private Stream<Field> constantFields() {
      Predicate<Field> constantField = f -> {
         int modifiers = f.getModifiers();
         return Modifier.isPublic(modifiers) && Modifier.isStatic(modifiers) && Modifier.isFinal(modifiers);
      };

      return Stream.of(NaceRev2Code.class.getDeclaredFields()).filter(constantField);
   }

}
