/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.loader.eurostat.providers;

import static com.tinubu.commons.referential.referentials.naceRev2.NaceRev2Referential.DEFAULT_REFERENTIAL_CODE;

import com.tinubu.commons.referential.ReferentialCode;
import com.tinubu.commons.referential.ReferentialProvider;
import com.tinubu.commons.referential.loader.eurostat.EurostatNaceRev2ReferentialLoader;
import com.tinubu.commons.referential.referentials.naceRev2.NaceRev2;
import com.tinubu.commons.referential.referentials.naceRev2.NaceRev2Referential;
import com.tinubu.commons.referential.referentials.naceRev2.NaceRev2Referential.NaceRev2ReferentialBuilder;

public class EurostatNaceRev2ReferentialProvider implements ReferentialProvider {

   @Override
   public ReferentialCode<NaceRev2, NaceRev2Referential> referentialCode() {
      return DEFAULT_REFERENTIAL_CODE;
   }

   @Override
   public Class<NaceRev2Referential> referentialClass() {
      return NaceRev2Referential.class;
   }

   @Override
   public NaceRev2Referential build() {
      return new NaceRev2ReferentialBuilder()
            .referentialCode(referentialCode())
            .referentialLoader(new EurostatNaceRev2ReferentialLoader())
            .build();
   }
}
