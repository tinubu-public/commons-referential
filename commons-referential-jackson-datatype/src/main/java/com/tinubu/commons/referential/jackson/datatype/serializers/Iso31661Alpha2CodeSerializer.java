/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.jackson.datatype.serializers;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonFormatVisitorWrapper;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2Code;

public final class Iso31661Alpha2CodeSerializer extends StdSerializer<Iso31661Alpha2Code> {

   public Iso31661Alpha2CodeSerializer() {
      super(Iso31661Alpha2Code.class);
   }

   @Override
   public void serialize(final Iso31661Alpha2Code value,
                         final JsonGenerator generator,
                         final SerializerProvider serializers) throws IOException {
      if (value == null) {
         generator.writeNull();
      } else {
         generator.writeString(value.entryCode());
      }
   }

   @Override
   public void acceptJsonFormatVisitor(final JsonFormatVisitorWrapper visitor, final JavaType hint)
         throws JsonMappingException {
      visitor.expectStringFormat(hint);
   }

}
