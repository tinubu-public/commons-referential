/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.jackson.datatype.deserializers;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.tinubu.commons.referential.referentials.IsoReferentials;
import com.tinubu.commons.referential.referentials.iso6391.Iso6391Alpha2;

public class Iso6391Alpha2Deserializer extends StdDeserializer<Iso6391Alpha2> {
   public Iso6391Alpha2Deserializer() {
      super(Iso6391Alpha2.class);
   }

   @Override
   public Iso6391Alpha2 deserialize(JsonParser parser, DeserializationContext context) throws IOException {
      final String countryCode = parser.getValueAsString();

      if (countryCode == null) return null;
      return IsoReferentials.iso6391Alpha2(countryCode);
   }
}
