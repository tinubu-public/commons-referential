/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.jackson.datatype;

import com.fasterxml.jackson.databind.module.SimpleModule;
import com.tinubu.commons.referential.Entry;
import com.tinubu.commons.referential.EntryCode;
import com.tinubu.commons.referential.Referential;
import com.tinubu.commons.referential.jackson.datatype.deserializers.Iso31661Alpha2CodeDeserializer;
import com.tinubu.commons.referential.jackson.datatype.deserializers.Iso31661Alpha2Deserializer;
import com.tinubu.commons.referential.jackson.datatype.deserializers.Iso31661Alpha3CodeDeserializer;
import com.tinubu.commons.referential.jackson.datatype.deserializers.Iso31661Alpha3Deserializer;
import com.tinubu.commons.referential.jackson.datatype.deserializers.Iso31661NumericCodeDeserializer;
import com.tinubu.commons.referential.jackson.datatype.deserializers.Iso31661NumericDeserializer;
import com.tinubu.commons.referential.jackson.datatype.deserializers.Iso31662CodeDeserializer;
import com.tinubu.commons.referential.jackson.datatype.deserializers.Iso31662Deserializer;
import com.tinubu.commons.referential.jackson.datatype.deserializers.Iso31663Alpha4CodeDeserializer;
import com.tinubu.commons.referential.jackson.datatype.deserializers.Iso31663Alpha4Deserializer;
import com.tinubu.commons.referential.jackson.datatype.deserializers.Iso6391Alpha2CodeDeserializer;
import com.tinubu.commons.referential.jackson.datatype.deserializers.Iso6391Alpha2Deserializer;
import com.tinubu.commons.referential.jackson.datatype.deserializers.Iso6392Alpha3CodeDeserializer;
import com.tinubu.commons.referential.jackson.datatype.deserializers.Iso6392Alpha3Deserializer;
import com.tinubu.commons.referential.jackson.datatype.serializers.Iso31661Alpha2CodeSerializer;
import com.tinubu.commons.referential.jackson.datatype.serializers.Iso31661Alpha2Serializer;
import com.tinubu.commons.referential.jackson.datatype.serializers.Iso31661Alpha3CodeSerializer;
import com.tinubu.commons.referential.jackson.datatype.serializers.Iso31661Alpha3Serializer;
import com.tinubu.commons.referential.jackson.datatype.serializers.Iso31661NumericCodeSerializer;
import com.tinubu.commons.referential.jackson.datatype.serializers.Iso31661NumericSerializer;
import com.tinubu.commons.referential.jackson.datatype.serializers.Iso31662CodeSerializer;
import com.tinubu.commons.referential.jackson.datatype.serializers.Iso31662Serializer;
import com.tinubu.commons.referential.jackson.datatype.serializers.Iso31663Alpha4CodeSerializer;
import com.tinubu.commons.referential.jackson.datatype.serializers.Iso31663Alpha4Serializer;
import com.tinubu.commons.referential.jackson.datatype.serializers.Iso6391Alpha2CodeSerializer;
import com.tinubu.commons.referential.jackson.datatype.serializers.Iso6391Alpha2Serializer;
import com.tinubu.commons.referential.jackson.datatype.serializers.Iso6392Alpha3CodeSerializer;
import com.tinubu.commons.referential.jackson.datatype.serializers.Iso6392Alpha3Serializer;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2Code;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha3;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha3Code;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Numeric;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661NumericCode;
import com.tinubu.commons.referential.referentials.iso31662.Iso31662;
import com.tinubu.commons.referential.referentials.iso31662.Iso31662Code;
import com.tinubu.commons.referential.referentials.iso31663.Iso31663Alpha4;
import com.tinubu.commons.referential.referentials.iso31663.Iso31663Alpha4Code;
import com.tinubu.commons.referential.referentials.iso6391.Iso6391Alpha2;
import com.tinubu.commons.referential.referentials.iso6391.Iso6391Alpha2Code;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code;

/**
 * Class that registers capability of serializing {@link Referential}, {@link Entry} and {@link EntryCode}
 * objects with the Jackson core.
 *
 * <pre>
 * ObjectMapper mapper = new ObjectMapper();
 * mapper.registerModule(new CommonsReferentialModule());
 * </pre>
 */
public final class CommonsReferentialModule extends SimpleModule {
   public CommonsReferentialModule() {
      super("CommonsReferential");

      addDeserializer(Iso6391Alpha2Code.class, new Iso6391Alpha2CodeDeserializer());
      addSerializer(Iso6391Alpha2Code.class, new Iso6391Alpha2CodeSerializer());

      addDeserializer(Iso6391Alpha2.class, new Iso6391Alpha2Deserializer());
      addSerializer(Iso6391Alpha2.class, new Iso6391Alpha2Serializer());

      addDeserializer(Iso6392Alpha3Code.class, new Iso6392Alpha3CodeDeserializer());
      addSerializer(Iso6392Alpha3Code.class, new Iso6392Alpha3CodeSerializer());

      addDeserializer(Iso6392Alpha3.class, new Iso6392Alpha3Deserializer());
      addSerializer(Iso6392Alpha3.class, new Iso6392Alpha3Serializer());

      addDeserializer(Iso31661Alpha2Code.class, new Iso31661Alpha2CodeDeserializer());
      addSerializer(Iso31661Alpha2Code.class, new Iso31661Alpha2CodeSerializer());

      addDeserializer(Iso31661Alpha2.class, new Iso31661Alpha2Deserializer());
      addSerializer(Iso31661Alpha2.class, new Iso31661Alpha2Serializer());

      addDeserializer(Iso31661Alpha3Code.class, new Iso31661Alpha3CodeDeserializer());
      addSerializer(Iso31661Alpha3Code.class, new Iso31661Alpha3CodeSerializer());

      addDeserializer(Iso31661Alpha3.class, new Iso31661Alpha3Deserializer());
      addSerializer(Iso31661Alpha3.class, new Iso31661Alpha3Serializer());

      addDeserializer(Iso31661NumericCode.class, new Iso31661NumericCodeDeserializer());
      addSerializer(Iso31661NumericCode.class, new Iso31661NumericCodeSerializer());

      addDeserializer(Iso31661Numeric.class, new Iso31661NumericDeserializer());
      addSerializer(Iso31661Numeric.class, new Iso31661NumericSerializer());

      addDeserializer(Iso31662Code.class, new Iso31662CodeDeserializer());
      addSerializer(Iso31662Code.class, new Iso31662CodeSerializer());

      addDeserializer(Iso31662.class, new Iso31662Deserializer());
      addSerializer(Iso31662.class, new Iso31662Serializer());

      addDeserializer(Iso31663Alpha4Code.class, new Iso31663Alpha4CodeDeserializer());
      addSerializer(Iso31663Alpha4Code.class, new Iso31663Alpha4CodeSerializer());

      addDeserializer(Iso31663Alpha4.class, new Iso31663Alpha4Deserializer());
      addSerializer(Iso31663Alpha4.class, new Iso31663Alpha4Serializer());
   }

}
