/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.fixture;

import com.tinubu.commons.referential.loaders.ConfigurableReferentialLoader.ConfigurableReferentialLoaderBuilder;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3.Iso6392Alpha3Data;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Referential;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Referential.Iso6392Alpha3ReferentialBuilder;
import com.tinubu.commons.test.fixture.DefaultFixtureFactory;
import com.tinubu.commons.test.fixture.junit.FixtureFactory;

@FixtureFactory(Iso6392Alpha3Referential.class)
public class Iso6392Alpha3ReferentialFixtureFactory extends DefaultFixtureFactory<Iso6392Alpha3Referential> {

   @Override
   public Iso6392Alpha3Referential create(int index) throws IndexOutOfBoundsException {
      return new Iso6392Alpha3ReferentialBuilder()
            .referentialLoader(new ConfigurableReferentialLoaderBuilder<Iso6392Alpha3Data>()
                                     .addEntryDatas(new Iso6392Alpha3DataFixtureFactory().createList(3))
                                     .build())
            .build();
   }

}
