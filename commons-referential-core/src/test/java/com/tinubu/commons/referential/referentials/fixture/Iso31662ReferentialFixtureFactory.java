/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.fixture;

import com.tinubu.commons.referential.loaders.ConfigurableReferentialLoader.ConfigurableReferentialLoaderBuilder;
import com.tinubu.commons.referential.referentials.iso31662.Iso31662.Iso31662Data;
import com.tinubu.commons.referential.referentials.iso31662.Iso31662Referential;
import com.tinubu.commons.referential.referentials.iso31662.Iso31662Referential.Iso31662ReferentialBuilder;
import com.tinubu.commons.test.fixture.DefaultFixtureFactory;
import com.tinubu.commons.test.fixture.junit.FixtureFactory;

@FixtureFactory(Iso31662Referential.class)
public class Iso31662ReferentialFixtureFactory extends DefaultFixtureFactory<Iso31662Referential> {

   @Override
   public Iso31662Referential create(int index) throws IndexOutOfBoundsException {
      return new Iso31662ReferentialBuilder()
            .referentialLoader(new ConfigurableReferentialLoaderBuilder<Iso31662Data>()
                                     .addEntryDatas(new Iso31662DataFixtureFactory().createList(6))
                                     .build())
            .build();
   }

}
