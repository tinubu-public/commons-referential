/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.iso31661;

import static com.tinubu.commons.referential.Referentials.clearRegisteredReferentials;
import static com.tinubu.commons.referential.referentials.IsoReferentials.iso31661Alpha2;
import static com.tinubu.commons.referential.referentials.IsoReferentials.iso31661Alpha2Referential;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;
import static org.mockito.Mockito.doReturn;

import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.referential.Entry;
import com.tinubu.commons.referential.EntryCode;
import com.tinubu.commons.referential.ReferentialCode;
import com.tinubu.commons.referential.Referentials;
import com.tinubu.commons.referential.UnknownEntryException;
import com.tinubu.commons.referential.referentials.AbstractReferentialTest;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2.Iso31661Alpha2Data;
import com.tinubu.commons.test.fixture.junit.Fixture;

public class Iso31661Alpha2Test extends AbstractReferentialTest {

   @BeforeAll
   public static void registerTestReferentials(@Fixture Iso31661Alpha2Referential iso31661Alpha2Referential,
                                               @Fixture Iso31661Alpha3Referential iso31661Alpha3Referential,
                                               @Fixture Iso31661NumericReferential iso31661NumericReferential) {
      clearRegisteredReferentials();
      Referentials.registerReferential(referentialProvider(iso31661Alpha2Referential));
      Referentials.registerReferential(referentialProvider(iso31661Alpha3Referential));
      Referentials.registerReferential(referentialProvider(iso31661NumericReferential));
   }

   @Test
   public void testEntryWhenNominal(@Fixture Iso31661Alpha2Data isoEntryData) {
      Iso31661Alpha2Referential referential = iso31661Alpha2Referential();
      Iso31661Alpha2 fr = new Iso31661Alpha2(referential, isoEntryData);

      assertThat(fr.entryCode()).isEqualTo(EntryCode.of("FR"));
      assertThat(fr.entryData()).isEqualTo(isoEntryData);
      assertThat(fr.referential()).isEqualTo(referential);
      assertThat(fr).isEqualTo(new Iso31661Alpha2(referential, isoEntryData));
      assertThat(fr).hasSameHashCodeAs(new Iso31661Alpha2(referential, isoEntryData));
   }

   @Test
   public void testInstantiateEntryWhenBadParameters(@Fixture Iso31661Alpha2Data isoEntryData) {
      assertThatNullPointerException().isThrownBy(() -> new Iso31661Alpha2(null, isoEntryData)).withMessage("'referential' must not be null");

      Iso31661Alpha2Referential referential = iso31661Alpha2Referential();

      assertThatNullPointerException().isThrownBy(() -> new Iso31661Alpha2(referential, null)).withMessage("'entryData' must not be null");
   }

   @Test
   public void testEntryMappingWhenNominal(@Fixture Iso31661Alpha2Data isoEntryData) {
      Iso31661Alpha2Referential referential = iso31661Alpha2Referential();
      Iso31661Alpha2 fr = new Iso31661Alpha2(referential, isoEntryData);

      assertThat(fr.entryMapping(ReferentialCode.<Iso31661Alpha3, Iso31661Alpha3Referential>of(
            "ISO_31661_ALPHA3"))).map(Entry::entryCode).hasValue(Iso31661Alpha3Code.FRA);
      assertThat(fr.entryMapping(ReferentialCode.<Iso31661Numeric, Iso31661NumericReferential>of(
            "ISO_31661_NUMERIC"))).map(Entry::entryCode).hasValue(Iso31661NumericCode._250);
   }

   @Test
   public void testEntryMappingWhenUnknownMappedEntry(@Fixture Iso31661Alpha2Data isoEntryData) {
      Iso31661Alpha2Referential referential = iso31661Alpha2Referential();
      doReturn(Optional.of(Iso31661Alpha3Code.of("QLZ"))).when(isoEntryData).entryCode(ReferentialCode.<Iso31661Alpha3, Iso31661Alpha3Referential>of("ISO_31661_ALPHA3"));
      doReturn(Optional.of(Iso31661NumericCode.of("999"))).when(isoEntryData).entryCode(ReferentialCode.<Iso31661Numeric, Iso31661NumericReferential>of("ISO_31661_NUMERIC"));

      Iso31661Alpha2 fr = new Iso31661Alpha2(referential, isoEntryData);

      assertThatExceptionOfType(UnknownEntryException.class).isThrownBy(() -> fr.entryMapping(ReferentialCode.<Iso31661Alpha3, Iso31661Alpha3Referential>of(
            "ISO_31661_ALPHA3")));
      assertThatExceptionOfType(UnknownEntryException.class).isThrownBy(() -> fr.entryMapping(ReferentialCode.<Iso31661Numeric, Iso31661NumericReferential>of(
            "ISO_31661_NUMERIC")));
   }

   @Test
   public void testEntryComparableWhenNominal() {
      assertThat(iso31661Alpha2("FR").compareTo(iso31661Alpha2("FR"))).isEqualTo(0);
      assertThat(iso31661Alpha2("FR").compareTo(iso31661Alpha2("ZW"))).isLessThan(0);
      assertThat(iso31661Alpha2("FR").compareTo(iso31661Alpha2("AW"))).isGreaterThan(0);
   }

   @Test
   public void testEntryComparableWhenNullEntry() {
      assertThatNullPointerException().isThrownBy(() -> iso31661Alpha2("FR").compareTo(null)).withMessage("'entry' must not be null");
   }

}
