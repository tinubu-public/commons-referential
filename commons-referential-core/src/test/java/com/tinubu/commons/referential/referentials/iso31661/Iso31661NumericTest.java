/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.iso31661;

import static com.tinubu.commons.referential.Referentials.clearRegisteredReferentials;
import static com.tinubu.commons.referential.referentials.IsoReferentials.iso31661Numeric;
import static com.tinubu.commons.referential.referentials.IsoReferentials.iso31661NumericReferential;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.referential.DefaultEntryCodeMapping.DefaultEntryCodeMappingBuilder;
import com.tinubu.commons.referential.DefaultTranslatableName.DefaultTranslatableNameBuilder;
import com.tinubu.commons.referential.Entry;
import com.tinubu.commons.referential.EntryCode;
import com.tinubu.commons.referential.ReferentialCode;
import com.tinubu.commons.referential.Referentials;
import com.tinubu.commons.referential.UnknownEntryException;
import com.tinubu.commons.referential.referentials.AbstractReferentialTest;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Numeric.Iso31661NumericData;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code;
import com.tinubu.commons.test.fixture.junit.Fixture;

public class Iso31661NumericTest extends AbstractReferentialTest {

   @BeforeAll
   public static void registerTestReferentials(@Fixture Iso31661Alpha2Referential iso31661Alpha2Referential,
                                               @Fixture Iso31661Alpha3Referential iso31661Alpha3Referential,
                                               @Fixture Iso31661NumericReferential iso31661NumericReferential) {
      clearRegisteredReferentials();
      Referentials.registerReferential(referentialProvider(iso31661Alpha2Referential));
      Referentials.registerReferential(referentialProvider(iso31661Alpha3Referential));
      Referentials.registerReferential(referentialProvider(iso31661NumericReferential));
   }

   @Test
   public void testEntryWhenNominal() {
      Iso31661NumericReferential referential = iso31661NumericReferential();
      Iso31661NumericData isoEntryData = mockIso31661NumericData();
      Iso31661Numeric fr = new Iso31661Numeric(referential, isoEntryData);

      assertThat(fr.entryCode()).isEqualTo(EntryCode.of("250"));
      assertThat(fr.entryData()).isEqualTo(isoEntryData);
      assertThat(fr.referential()).isEqualTo(referential);
      assertThat(fr).isEqualTo(new Iso31661Numeric(referential, isoEntryData));
      assertThat(fr).hasSameHashCodeAs(new Iso31661Numeric(referential, isoEntryData));
   }

   @Test
   public void testInstantiateEntryWhenBadParameters() {
      assertThatNullPointerException().isThrownBy(() -> new Iso31661Numeric(null, mockIso31661NumericData())).withMessage("'referential' must not be null");

      Iso31661NumericReferential referential = iso31661NumericReferential();

      assertThatNullPointerException().isThrownBy(() -> new Iso31661Numeric(referential, null)).withMessage("'entryData' must not be null");
   }

   @Test
   public void testEntryMappingWhenNominal() {
      Iso31661NumericReferential referential = iso31661NumericReferential();
      Iso31661NumericData isoEntryData = mockIso31661NumericData();

      Iso31661Numeric fr = new Iso31661Numeric(referential, isoEntryData);

      assertThat(fr.entryMapping(ReferentialCode.<Iso31661Alpha2, Iso31661Alpha2Referential>of(
            "ISO_31661_ALPHA2"))).map(Entry::entryCode).hasValue(Iso31661Alpha2Code.FR);
      assertThat(fr.entryMapping(ReferentialCode.<Iso31661Alpha3, Iso31661Alpha3Referential>of(
            "ISO_31661_ALPHA3"))).map(Entry::entryCode).hasValue(Iso31661Alpha3Code.FRA);
   }

   @Test
   public void testEntryMappingWhenUnknownMappedEntry() {
      Iso31661NumericReferential referential = iso31661NumericReferential();
      Iso31661NumericData isoEntryData = mockIso31661NumericData();
      doReturn(Optional.of(Iso31661Alpha2Code.of("QL"))).when(isoEntryData).entryCode(ReferentialCode.<Iso31661Alpha2, Iso31661Alpha2Referential>of("ISO_31661_ALPHA2"));
      doReturn(Optional.of(Iso31661Alpha3Code.of("QLZ"))).when(isoEntryData).entryCode(ReferentialCode.<Iso31661Alpha3, Iso31661Alpha3Referential>of("ISO_31661_ALPHA3"));

      Iso31661Numeric fr = new Iso31661Numeric(referential, isoEntryData);

      assertThatExceptionOfType(UnknownEntryException.class).isThrownBy(() -> fr.entryMapping(ReferentialCode.<Iso31661Alpha2, Iso31661Alpha2Referential>of(
            "ISO_31661_ALPHA2")));
      assertThatExceptionOfType(UnknownEntryException.class).isThrownBy(() -> fr.entryMapping(ReferentialCode.<Iso31661Alpha3, Iso31661Alpha3Referential>of(
            "ISO_31661_ALPHA3")));
   }

   @Test
   public void testEntryComparableWhenNominal() {
      assertThat(iso31661Numeric("250").compareTo(iso31661Numeric("250"))).isEqualTo(0);
      assertThat(iso31661Numeric("250").compareTo(iso31661Numeric("716"))).isLessThan(0);
      assertThat(iso31661Numeric("250").compareTo(iso31661Numeric("533"))).isLessThan(0);
   }

   @Test
   public void testEntryComparableWhenNullEntry() {
      assertThatNullPointerException().isThrownBy(() -> iso31661Numeric("250").compareTo(null)).withMessage("'entry' must not be null");
   }

   private Iso31661NumericData mockIso31661NumericData() {
      Iso31661NumericData data = spy(Iso31661NumericData.class);

      when(data.iso31661Numeric()).thenReturn(Iso31661NumericCode._250);
      when(data.name()).thenReturn(new DefaultTranslatableNameBuilder()
                                         .name(Iso6392Alpha3Code.ENG, "France")
                                         .build());
      when(data.commonName()).thenReturn(new DefaultTranslatableNameBuilder()
                                               .name(Iso6392Alpha3Code.ENG, "France")
                                               .build());
      when(data.officialName()).thenReturn(new DefaultTranslatableNameBuilder()
                                                 .name(Iso6392Alpha3Code.ENG, "French Republic")
                                                 .build());
      when(data.entryCodeMapping()).thenReturn(new DefaultEntryCodeMappingBuilder()
                                                     .mapping(ReferentialCode.<Iso31661Alpha2, Iso31661Alpha2Referential>of(
                                                           "ISO_31661_ALPHA2"), Iso31661Alpha2Code.FR)
                                                     .mapping(ReferentialCode.<Iso31661Alpha3, Iso31661Alpha3Referential>of(
                                                           "ISO_31661_ALPHA3"), Iso31661Alpha3Code.FRA)
                                                     .mapping(ReferentialCode.<Iso31661Numeric, Iso31661NumericReferential>of(
                                                           "ISO_31661_NUMERIC"), Iso31661NumericCode._250)
                                                     .build());

      return data;
   }

}
