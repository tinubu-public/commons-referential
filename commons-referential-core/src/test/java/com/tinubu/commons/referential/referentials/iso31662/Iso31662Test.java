/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.iso31662;

import static com.tinubu.commons.referential.Referentials.clearRegisteredReferentials;
import static com.tinubu.commons.referential.referentials.IsoReferentials.iso31662;
import static com.tinubu.commons.referential.referentials.IsoReferentials.iso31662Referential;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.referential.DefaultTranslatableName.DefaultTranslatableNameBuilder;
import com.tinubu.commons.referential.Referentials;
import com.tinubu.commons.referential.UnknownEntryException;
import com.tinubu.commons.referential.referentials.AbstractReferentialTest;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2Code;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2Referential;
import com.tinubu.commons.referential.referentials.iso31662.Iso31662.Iso31662Data;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code;
import com.tinubu.commons.test.fixture.junit.Fixture;

public class Iso31662Test extends AbstractReferentialTest {

   @BeforeAll
   public static void registerTestReferentials(@Fixture Iso31662Referential iso31662Referential,
                                               @Fixture Iso31661Alpha2Referential iso31661Alpha2Referential) {
      clearRegisteredReferentials();
      Referentials.registerReferential(referentialProvider(iso31662Referential));
      Referentials.registerReferential(referentialProvider(iso31661Alpha2Referential));
   }

   @Test
   public void testEntryWhenNominal() {
      Iso31662Referential referential = iso31662Referential();
      Iso31662Data isoEntryData = mockIso31662Data();
      Iso31662 fr01 = new Iso31662(referential, isoEntryData);

      assertThat(fr01.entryCode()).isEqualTo(Iso31662Code.of("FR-01"));
      Assertions.assertThat(fr01.entryData()).isEqualTo(isoEntryData);
      Assertions.assertThat(fr01.referential()).isEqualTo(referential);
      Assertions.assertThat(fr01).isEqualTo(new Iso31662(referential, isoEntryData));
      Assertions.assertThat(fr01).hasSameHashCodeAs(new Iso31662(referential, isoEntryData));
   }

   @Test
   public void testInstantiateEntryWhenBadParameters() {
      assertThatNullPointerException().isThrownBy(() -> new Iso31662(null, mockIso31662Data())).withMessage("'referential' must not be null");

      Iso31662Referential referential = iso31662Referential();

      assertThatNullPointerException().isThrownBy(() -> new Iso31662(referential, null)).withMessage("'entryData' must not be null");
   }

   @Test
   public void testIsoAlpha2EntryWhenNominal() {
      Iso31662Referential referential = iso31662Referential();
      Iso31662Data isoEntryData = mockIso31662Data();
      Iso31662 fr01 = new Iso31662(referential, isoEntryData);

      assertThat(fr01.country()).isExactlyInstanceOf(Iso31661Alpha2.class);
      assertThat(fr01.country().entryCode()).isEqualTo(Iso31661Alpha2Code.of("FR"));
   }

   // FIXME test when unknown

   @Test
   public void testParentWhenNominal() {
      Iso31662Referential referential = iso31662Referential();
      Iso31662Data isoEntryData = mockIso31662Data();
      Iso31662 fr01 = new Iso31662(referential, isoEntryData);

      assertThat(fr01.parent()).get().satisfies(parent -> {
         Assertions.assertThat(parent).isExactlyInstanceOf(Iso31662.class);
         assertThat(parent.entryCode()).isEqualTo(Iso31662Code.of("FR-ARA"));
      });
   }

   @Test
   public void testParentWhenEmpty() {
      Iso31662Referential referential = iso31662Referential();
      Iso31662Data isoEntryData = mockIso31662Data();
      doReturn(Optional.empty()).when(isoEntryData).parent();

      Iso31662 fr01 = new Iso31662(referential, isoEntryData);

      assertThat(fr01.parent()).isEmpty();
   }

   @Test
   public void testParentWhenUnknown() {
      Iso31662Referential referential = iso31662Referential();
      Iso31662Data isoEntryData = mockIso31662Data();
      doReturn(Optional.of(Iso31662Code.of("FR-DOESNOTEXIST"))).when(isoEntryData).parent();

      Iso31662 fr01 = new Iso31662(referential, isoEntryData);

      assertThatExceptionOfType(UnknownEntryException.class).isThrownBy(() -> fr01.parent()).withMessage("Unknown 'FR-DOESNOTEXIST' entry code in 'ISO_31662' referential");
   }

   @Test
   public void testEntryComparableWhenNominal() {
      Assertions.assertThat(iso31662("FR-01").compareTo(iso31662("FR-01"))).isEqualTo(0);
      Assertions.assertThat(iso31662("FR-01").compareTo(iso31662("FR-02"))).isLessThan(0);
      Assertions.assertThat(iso31662("FR-01").compareTo(iso31662("ZW-MW"))).isLessThan(0);
      Assertions.assertThat(iso31662("FR-01").compareTo(iso31662("AD-02"))).isGreaterThan(0);
   }

   @Test
   public void testEntryComparableWhenNullCountry() {
      assertThatNullPointerException().isThrownBy(() -> iso31662("FR-01").compareTo(null)).withMessage("'entry' must not be null");
   }

   private Iso31662Data mockIso31662Data() {
      Iso31662Data data = spy(Iso31662Data.class);

      when(data.iso31662()).thenReturn(Iso31662Code.FR_01);
      when(data.country()).thenReturn(Iso31661Alpha2Code.FR);
      when(data.name()).thenReturn(new DefaultTranslatableNameBuilder()
                                         .name(Iso6392Alpha3Code.ENG, "Ain")
                                         .build());
      when(data.type()).thenReturn("Metropolitan department");
      when(data.parent()).thenReturn(Optional.of(Iso31662Code.FR_ARA));

      return data;
   }

}
