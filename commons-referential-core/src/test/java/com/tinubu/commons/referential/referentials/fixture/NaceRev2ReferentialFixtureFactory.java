/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.fixture;

import com.tinubu.commons.referential.loaders.ConfigurableReferentialLoader.ConfigurableReferentialLoaderBuilder;
import com.tinubu.commons.referential.referentials.naceRev2.NaceRev2.NaceRev2Data;
import com.tinubu.commons.referential.referentials.naceRev2.NaceRev2Referential;
import com.tinubu.commons.referential.referentials.naceRev2.NaceRev2Referential.NaceRev2ReferentialBuilder;
import com.tinubu.commons.test.fixture.DefaultFixtureFactory;
import com.tinubu.commons.test.fixture.junit.FixtureFactory;

@FixtureFactory(NaceRev2Referential.class)
public class NaceRev2ReferentialFixtureFactory extends DefaultFixtureFactory<NaceRev2Referential> {

   @Override
   public NaceRev2Referential create(int index) throws IndexOutOfBoundsException {
      return new NaceRev2ReferentialBuilder()
            .referentialLoader(new ConfigurableReferentialLoaderBuilder<NaceRev2Data>()
                                     .addEntryDatas(new NaceRev2DataFixtureFactory().createList(2))
                                     .build())
            .build();
   }

}
