/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.naceRev2;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import org.junit.jupiter.api.Test;

class NaceRev2CodeTest {

   @Test
   public void testOfWhenCaseInsensitive() {
      assertThat(NaceRev2Code.of("a")).isEqualTo(NaceRev2Code.A);
      assertThat(NaceRev2Code.of("A")).isEqualTo(NaceRev2Code.A);
   }

   @Test
   public void testOfWhenNominal() {
      assertThat(NaceRev2Code.of("A").entryCode()).isEqualTo("A");

      assertThat(NaceRev2Code.of("01").entryCode()).isEqualTo("01");

      assertThat(NaceRev2Code.of("01.1").entryCode()).isEqualTo("01.1");

      assertThat(NaceRev2Code.of("01.12").entryCode()).isEqualTo("01.12");
   }

   @Test
   public void testOfWhenInvalid() {
      assertOfInvalidEntryCode("AA");
      assertOfInvalidEntryCode("0");
      assertOfInvalidEntryCode("A01");
      assertOfInvalidEntryCode("000");
      assertOfInvalidEntryCode("0.0");
      assertOfInvalidEntryCode("0.00");
      assertOfInvalidEntryCode("0.000");
      assertOfInvalidEntryCode("00.000");
      assertOfInvalidEntryCode("000.0");
      assertOfInvalidEntryCode("000.00");
      assertOfInvalidEntryCode("A01.1");
      assertOfInvalidEntryCode("0110");
      assertOfInvalidEntryCode("A01.12");
      assertOfInvalidEntryCode("01101");
   }

   @Test
   public void testOfWhenBadParameters() {
      assertThatNullPointerException().isThrownBy(() -> NaceRev2Code.of(null)).withMessage("'entryCode' must not be null");
      assertThatIllegalArgumentException().isThrownBy(() -> NaceRev2Code.of("")).withMessage("'entryCode' must not be blank");
   }

   @Test
   public void testOfLenientWhenNominal() {
      assertThat(NaceRev2Code.ofLenient("A").entryCode()).isEqualTo("A");

      assertThat(NaceRev2Code.ofLenient("01").entryCode()).isEqualTo("01");

      assertThat(NaceRev2Code.ofLenient("01.1").entryCode()).isEqualTo("01.1");
      assertThat(NaceRev2Code.ofLenient("011").entryCode()).isEqualTo("01.1");
      assertThat(NaceRev2Code.ofLenient("01.12").entryCode()).isEqualTo("01.12");
      assertThat(NaceRev2Code.ofLenient("0112").entryCode()).isEqualTo("01.12");
      assertThat(NaceRev2Code.ofLenient("01.1.2").entryCode()).isEqualTo("01.12");
      assertThat(NaceRev2Code.ofLenient("011.2").entryCode()).isEqualTo("01.12");
   }

   @Test
   public void testOfLenientWhenInvalid() {
      assertOfLenientInvalidEntryCode("A.");
      assertOfLenientInvalidEntryCode("AA");
      assertOfLenientInvalidEntryCode("0");
      assertOfLenientInvalidEntryCode("0.0");
      assertOfLenientInvalidEntryCode("0.00");
      assertOfLenientInvalidEntryCode("0.000");
      assertOfLenientInvalidEntryCode("00.000");
      assertOfLenientInvalidEntryCode("000.00");
      assertOfLenientInvalidEntryCode("01101");
      assertOfLenientInvalidEntryCode("A01");
      assertOfLenientInvalidEntryCode("A.01");
      assertOfLenientInvalidEntryCode("A01.1");
      assertOfLenientInvalidEntryCode("A.01.1");
      assertOfLenientInvalidEntryCode("A011");
      assertOfLenientInvalidEntryCode("A.011");
      assertOfLenientInvalidEntryCode("A01.12");
      assertOfLenientInvalidEntryCode("A.01.12");
      assertOfLenientInvalidEntryCode("A0112");
      assertOfLenientInvalidEntryCode("A.0112");
      assertOfLenientInvalidEntryCode("A01.1.2");
      assertOfLenientInvalidEntryCode("A.01.1.2");
      assertOfLenientInvalidEntryCode("A011.2");
      assertOfLenientInvalidEntryCode("A.011.2");
   }

   @Test
   public void testOfLenientWhenBadParameters() {
      assertThatNullPointerException().isThrownBy(() -> NaceRev2Code.ofLenient(null)).withMessage("'entryCode' must not be null");
      assertThatIllegalArgumentException().isThrownBy(() -> NaceRev2Code.ofLenient("")).withMessage("'entryCode' must not be blank");
   }

   @Test
   public void testUndottedEntryCodeWhenNominal() {
      assertThat(NaceRev2Code.A.undottedEntryCode()).isEqualTo("A");
      assertThat(NaceRev2Code._01.undottedEntryCode()).isEqualTo("01");
      assertThat(NaceRev2Code._01_1.undottedEntryCode()).isEqualTo("011");
      assertThat(NaceRev2Code._01_12.undottedEntryCode()).isEqualTo("0112");
   }

   private void assertOfInvalidEntryCode(String entryCode) {
      assertThatIllegalArgumentException().isThrownBy(() -> NaceRev2Code.of(entryCode)).withMessage("Invalid 'entryCode' code format : " + entryCode);
   }

   private void assertOfLenientInvalidEntryCode(String entryCode) {
      assertThatIllegalArgumentException().isThrownBy(() -> NaceRev2Code.ofLenient(entryCode)).withMessage("Invalid 'entryCode' code format : " + entryCode);
   }


}