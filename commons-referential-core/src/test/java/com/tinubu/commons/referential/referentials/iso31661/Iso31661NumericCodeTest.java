/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.iso31661;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import org.junit.jupiter.api.Test;

class Iso31661NumericCodeTest {

   @Test
   public void testOfWhenNominal() {
      assertThat(Iso31661NumericCode.of("001").entryCode()).isEqualTo("001");
   }

   @Test
   public void testOfWhenInvalid() {
      assertThatEntryCodeIsInvalid("1");
      assertThatEntryCodeIsInvalid("01");
      assertThatEntryCodeIsInvalid("0001");
   }

   @Test
   public void testOfWhenBadParameters() {
      assertThatNullPointerException().isThrownBy(() -> Iso31661NumericCode.of(null)).withMessage("'entryCode' must not be null");
      assertThatIllegalArgumentException().isThrownBy(() -> Iso31661NumericCode.of("")).withMessage("'entryCode' must not be blank");
   }

   @Test
   public void testOfInteger() {
      assertThat(Iso31661NumericCode.of(0)).isEqualTo(Iso31661NumericCode.of("000"));
      assertThat(Iso31661NumericCode.of(1)).isEqualTo(Iso31661NumericCode.of("001"));
      assertThat(Iso31661NumericCode.of(10)).isEqualTo(Iso31661NumericCode.of("010"));
      assertThat(Iso31661NumericCode.of(10)).isEqualTo(Iso31661NumericCode.of("010"));
      assertThat(Iso31661NumericCode.of(250)).isEqualTo(Iso31661NumericCode.of("250"));
      assertThat(Iso31661NumericCode.of(999)).isEqualTo(Iso31661NumericCode.of("999"));
      assertThatIllegalArgumentException()
            .isThrownBy(() -> Iso31661NumericCode.of(1000))
            .isInstanceOf(IllegalArgumentException.class)
            .withMessage("Invalid 'entryCode' code format : 1000");
   }

   private void assertThatEntryCodeIsInvalid(String entryCode) {
      assertThatIllegalArgumentException().isThrownBy(() -> Iso31661NumericCode.of(entryCode)).withMessage("Invalid 'entryCode' code format : " + entryCode);
   }

}