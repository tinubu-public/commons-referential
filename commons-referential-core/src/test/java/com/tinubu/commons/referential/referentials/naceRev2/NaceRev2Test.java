/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.naceRev2;

import static com.tinubu.commons.referential.Referentials.clearRegisteredReferentials;
import static com.tinubu.commons.referential.referentials.NaceReferentials.naceRev2Referential;
import static com.tinubu.commons.referential.referentials.naceRev2.NaceRev2.NaceRev2EntryType.CLASS;
import static com.tinubu.commons.referential.referentials.naceRev2.NaceRev2.NaceRev2EntryType.SECTION;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.referential.Referentials;
import com.tinubu.commons.referential.referentials.AbstractReferentialTest;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code;
import com.tinubu.commons.test.fixture.junit.Fixture;

public class NaceRev2Test extends AbstractReferentialTest {

   @BeforeAll
   public static void registerTestReferentials(@Fixture NaceRev2Referential naceRev2Referential) {
      clearRegisteredReferentials();
      Referentials.registerReferential(referentialProvider(naceRev2Referential));
   }

   @Test
   public void testEntryWhenNominal() {
      NaceRev2Referential referential = naceRev2Referential();
      NaceRev2 value = referential.entry("01.63");

      assertThat(value
                       .entryData()
                       .name()
                       .name(Iso6392Alpha3Code.ENG)).hasValue("Post-harvest crop activities");
      assertThat(value.entryData().name().name(Iso6392Alpha3Code.FRA)).hasValue(
            "Traitement primaire des récoltes");
      assertThat(value.entryData().type()).isEqualTo(CLASS);
      assertThat(value.entryData().naceRev2()).isEqualTo(NaceRev2Code.of("01.63"));
      assertThat(value.entryData().section()).isEqualTo(NaceRev2Code.of("A"));
      assertThat(value.entryData().division()).hasValue(NaceRev2Code.of("01"));
      assertThat(value.entryData().group()).hasValue(NaceRev2Code.of("01.6"));
      assertThat(value.entryData().naceClass()).hasValue(NaceRev2Code.of("01.63"));
   }

   @Test
   public void testEntryWhenSection() {
      NaceRev2Referential referential = naceRev2Referential();
      NaceRev2 value = referential.entry("A");

      assertThat(value.entryData().name().name(Iso6392Alpha3Code.ENG)).hasValue(
            "AGRICULTURE, FORESTRY AND FISHING");
      assertThat(value.entryData().name().name(Iso6392Alpha3Code.FRA)).hasValue(
            "AGRICULTURE, SYLVICULTURE ET PÊCHE");
      assertThat(value.entryData().type()).isEqualTo(SECTION);
      assertThat(value.entryData().naceRev2()).isEqualTo(NaceRev2Code.of("A"));
      assertThat(value.entryData().section()).isEqualTo(NaceRev2Code.of("A"));
      assertThat(value.entryData().division()).isEmpty();
      assertThat(value.entryData().group()).isEmpty();
      assertThat(value.entryData().naceClass()).isEmpty();
   }
}
