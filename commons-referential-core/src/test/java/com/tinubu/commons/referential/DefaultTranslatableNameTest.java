/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.referential.DefaultTranslatableName.DefaultTranslatableNameBuilder;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code;

class DefaultTranslatableNameTest {

   @Test
   public void testWhenNominal() {
      DefaultTranslatableName name = new DefaultTranslatableNameBuilder()
            .name(Iso6392Alpha3Code.FRA, "Angleterre")
            .name(Iso6392Alpha3Code.ENG, "England")
            .build();

      assertThat(name.name(Iso6392Alpha3Code.FRA)).hasValue("Angleterre");
      assertThat(name.name(Iso6392Alpha3Code.ENG)).hasValue("England");
   }

   @Test
   public void testWhenUnknownLanguage() {
      DefaultTranslatableName name =
            new DefaultTranslatableNameBuilder().name(Iso6392Alpha3Code.FRA, "Angleterre").build();

      assertThat(name.name(Iso6392Alpha3Code.ENG)).isEmpty();
   }

   @Test
   public void testWhenDelegate() {
      DefaultTranslatableName delegate =
            new DefaultTranslatableNameBuilder().name(Iso6392Alpha3Code.ENG, "England").build();

      TranslatableName name = new DefaultTranslatableNameBuilder()
            .name(Iso6392Alpha3Code.FRA, "Angleterre")
            .build()
            .delegate(delegate);

      assertThat(name.name(Iso6392Alpha3Code.FRA)).hasValue("Angleterre");
      assertThat(name.name(Iso6392Alpha3Code.ENG)).hasValue("England");
      assertThat(name.name(Iso6392Alpha3Code.AAR)).isEmpty();
   }

}