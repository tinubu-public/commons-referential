/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential;

import static org.assertj.core.api.Assertions.assertThat;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class EntryCodeTest {

   @Test
   public void testEquality() {
      assertThat(EntryCode.of("FR")).isEqualTo(EntryCode.of("FR"));
      assertThat(EntryCode.of("FR")).isEqualTo(new Test1Code("FR"));
      Assertions.assertThat(new Test1Code("FR")).isEqualTo(EntryCode.of("FR"));
      Assertions.assertThat(new Test1Code("FR")).isEqualTo(new Test1Code("FR"));
      Assertions.assertThat(new Test1Code("FR")).isNotEqualTo(new Test2Code("FR"));
   }

   public static class Test1Code extends EntryCode {

      public Test1Code(String entryCode) {
         super(entryCode);
      }
   }

   public static class Test2Code extends EntryCode {

      public Test2Code(String entryCode) {
         super(entryCode);
      }
   }

}