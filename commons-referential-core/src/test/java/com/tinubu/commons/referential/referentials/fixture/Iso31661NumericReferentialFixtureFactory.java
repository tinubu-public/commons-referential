/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.fixture;

import com.tinubu.commons.referential.loaders.ConfigurableReferentialLoader.ConfigurableReferentialLoaderBuilder;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Numeric.Iso31661NumericData;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661NumericReferential;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661NumericReferential.Iso31661NumericReferentialBuilder;
import com.tinubu.commons.test.fixture.DefaultFixtureFactory;
import com.tinubu.commons.test.fixture.junit.FixtureFactory;

@FixtureFactory(Iso31661NumericReferential.class)
public class Iso31661NumericReferentialFixtureFactory
      extends DefaultFixtureFactory<Iso31661NumericReferential> {

   @Override
   public Iso31661NumericReferential create(int index) throws IndexOutOfBoundsException {
      return new Iso31661NumericReferentialBuilder()
            .referentialLoader(new ConfigurableReferentialLoaderBuilder<Iso31661NumericData>()
                                     .addEntryDatas(new Iso31661NumericDataFixtureFactory().createList(4))
                                     .build())
            .build();
   }

}
