/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.iso31663;

import static com.tinubu.commons.referential.Referentials.clearRegisteredReferentials;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.referential.Referentials;
import com.tinubu.commons.referential.referentials.AbstractReferentialTest;
import com.tinubu.commons.referential.referentials.IsoReferentials;
import com.tinubu.commons.referential.referentials.iso31663.Iso31663Alpha4.Iso31663Alpha4Data;
import com.tinubu.commons.test.fixture.junit.Fixture;

public class Iso31663Alpha4Test extends AbstractReferentialTest {

   @BeforeAll
   public static void registerTestReferentials(@Fixture Iso31663Alpha4Referential iso31663Alpha4Referential) {
      clearRegisteredReferentials();
      Referentials.registerReferential(referentialProvider(iso31663Alpha4Referential));
   }

   @Test
   public void testEntryWhenNominal(@Fixture Iso31663Alpha4Data isoEntryData) {
      Iso31663Alpha4Referential referential = IsoReferentials.iso31663Alpha4Referential();
      Iso31663Alpha4 fr = new Iso31663Alpha4(referential, isoEntryData);

      assertThat(fr.entryCode()).isEqualTo(Iso31663Alpha4Code.of("AIDJ"));
      Assertions.assertThat(fr.entryData()).isEqualTo(isoEntryData);
      Assertions.assertThat(fr.referential()).isEqualTo(referential);
      Assertions.assertThat(fr).isEqualTo(new Iso31663Alpha4(referential, isoEntryData));
      Assertions.assertThat(fr).hasSameHashCodeAs(new Iso31663Alpha4(referential, isoEntryData));
   }

   @Test
   public void testInstantiateEntryWhenBadParameters(@Fixture Iso31663Alpha4Data isoEntryData) {
      assertThatNullPointerException().isThrownBy(() -> new Iso31663Alpha4(null, isoEntryData)).withMessage("'referential' must not be null");

      Iso31663Alpha4Referential referential = IsoReferentials.iso31663Alpha4Referential();

      assertThatNullPointerException().isThrownBy(() -> new Iso31663Alpha4(referential, null)).withMessage("'entryData' must not be null");
   }

   @Test
   public void testEntryComparableWhenNominal() {
      Assertions
            .assertThat(IsoReferentials
                              .iso31663Alpha4("ANHH")
                              .compareTo(IsoReferentials.iso31663Alpha4("ANHH")))
            .isEqualTo(0);
      Assertions
            .assertThat(IsoReferentials
                              .iso31663Alpha4("ANHH")
                              .compareTo(IsoReferentials.iso31663Alpha4("ZRCD")))
            .isLessThan(0);
      Assertions
            .assertThat(IsoReferentials
                              .iso31663Alpha4("ANHH")
                              .compareTo(IsoReferentials.iso31663Alpha4("AIDJ")))
            .isGreaterThan(0);
   }

   @Test
   public void testEntryComparableWhenNullEntry() {
      assertThatNullPointerException()
            .isThrownBy(() -> IsoReferentials.iso31663Alpha4("ANHH").compareTo(null))
            .withMessage("'entry' must not be null");
   }

}
