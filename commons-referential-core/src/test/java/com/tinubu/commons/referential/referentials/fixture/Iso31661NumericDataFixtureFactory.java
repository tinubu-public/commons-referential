/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.fixture;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import com.tinubu.commons.referential.DefaultEntryCodeMapping.DefaultEntryCodeMappingBuilder;
import com.tinubu.commons.referential.DefaultTranslatableName.DefaultTranslatableNameBuilder;
import com.tinubu.commons.referential.ReferentialCode;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2Code;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2Referential;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha3Code;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Numeric;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Numeric.Iso31661NumericData;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661NumericCode;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661NumericReferential;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code;
import com.tinubu.commons.test.fixture.DefaultFixtureFactory;
import com.tinubu.commons.test.fixture.junit.FixtureFactory;

@FixtureFactory(Iso31661NumericData.class)
public class Iso31661NumericDataFixtureFactory extends DefaultFixtureFactory<Iso31661NumericData> {

   public static final List<Iso31661NumericData> DATA = Arrays.asList(mockIso31661NumericFranceData(),
                                                                      mockIso31661NumericZimbabweData(),
                                                                      mockIso31661NumericArubaData(),
                                                                      mockIso31661NumericAndorraData());

   @Override
   public Iso31661NumericData create(int index) throws IndexOutOfBoundsException {
      if (index >= DATA.size()) {
         throw new IndexOutOfBoundsException();
      }
      return DATA.get(index);
   }

   private static Iso31661NumericData mockIso31661NumericFranceData() {
      return mockIso31661NumericData(Iso31661Alpha2Code.FR,
                                     Iso31661Alpha3Code.FRA,
                                     Iso31661NumericCode._250,
                                     "France",
                                     "French republic");
   }

   private static Iso31661NumericData mockIso31661NumericZimbabweData() {
      return mockIso31661NumericData(Iso31661Alpha2Code.ZW,
                                     Iso31661Alpha3Code.ZWE,
                                     Iso31661NumericCode._716,
                                     "Zimbabwe",
                                     "Republic of Zimbabwe");
   }

   private static Iso31661NumericData mockIso31661NumericArubaData() {
      return mockIso31661NumericData(Iso31661Alpha2Code.AW,
                                     Iso31661Alpha3Code.ABW,
                                     Iso31661NumericCode._533,
                                     "Aruba",
                                     null);
   }

   private static Iso31661NumericData mockIso31661NumericAndorraData() {
      return mockIso31661NumericData(Iso31661Alpha2Code.AD,
                                     Iso31661Alpha3Code.AND,
                                     Iso31661NumericCode._020,
                                     "Andorra",
                                     "Principality of Andorra");
   }

   private static Iso31661NumericData mockIso31661NumericData(Iso31661Alpha2Code alpha2Code,
                                                              Iso31661Alpha3Code alpha3Code,
                                                              Iso31661NumericCode numericCode,
                                                              String name,
                                                              String officialName) {
      Iso31661NumericData data = spy(Iso31661NumericData.class);

      when(data.iso31661Numeric()).thenReturn(numericCode);
      when(data.name()).thenReturn(new DefaultTranslatableNameBuilder()
                                         .name(Iso6392Alpha3Code.ENG, name)
                                         .build());
      when(data.commonName()).thenReturn(new DefaultTranslatableNameBuilder()
                                               .name(Iso6392Alpha3Code.ENG, name)
                                               .build());
      if (officialName != null) {
         when(data.officialName()).thenReturn(new DefaultTranslatableNameBuilder()
                                                    .name(Iso6392Alpha3Code.ENG, officialName)
                                                    .build());
      }
      when(data.entryCodeMapping()).thenReturn(new DefaultEntryCodeMappingBuilder()
                                                     .mapping(ReferentialCode.<Iso31661Alpha2, Iso31661Alpha2Referential>of(
                                                           "ISO_31661_ALPHA2"), alpha2Code)
                                                     .mapping(ReferentialCode.<Iso31661Numeric, Iso31661NumericReferential>of(
                                                           "ISO_31661_ALPHA3"), alpha3Code)
                                                     .mapping(ReferentialCode.<Iso31661Numeric, Iso31661NumericReferential>of(
                                                           "ISO_31661_NUMERIC"), numericCode)
                                                     .build());

      return data;
   }

}
