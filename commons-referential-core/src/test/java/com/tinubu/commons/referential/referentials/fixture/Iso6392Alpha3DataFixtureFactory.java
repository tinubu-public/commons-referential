/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.fixture;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import com.tinubu.commons.referential.DefaultEntryCodeMapping.DefaultEntryCodeMappingBuilder;
import com.tinubu.commons.referential.DefaultTranslatableName;
import com.tinubu.commons.referential.ReferentialCode;
import com.tinubu.commons.referential.referentials.iso6391.Iso6391Alpha2;
import com.tinubu.commons.referential.referentials.iso6391.Iso6391Alpha2Code;
import com.tinubu.commons.referential.referentials.iso6391.Iso6391Alpha2Referential;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3.Iso6392Alpha3Data;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Referential;
import com.tinubu.commons.test.fixture.DefaultFixtureFactory;
import com.tinubu.commons.test.fixture.junit.FixtureFactory;

@FixtureFactory(Iso6392Alpha3Data.class)
public class Iso6392Alpha3DataFixtureFactory extends DefaultFixtureFactory<Iso6392Alpha3Data> {

   public static final List<Iso6392Alpha3Data> DATA = Arrays.asList(mockIso6392Alpha3FrenchData(),
                                                                    mockIso6392Alpha3ZuluData(),
                                                                    mockIso6392Alpha3AfarData());

   @Override
   public Iso6392Alpha3Data create(int index) throws IndexOutOfBoundsException {
      if (index >= DATA.size()) {
         throw new IndexOutOfBoundsException();
      }
      return DATA.get(index);
   }

   private static Iso6392Alpha3Data mockIso6392Alpha3FrenchData() {
      Iso6392Alpha3Data data = spy(Iso6392Alpha3Data.class);

      when(data.name()).thenReturn(new DefaultTranslatableName.DefaultTranslatableNameBuilder()
                                         .name(Iso6392Alpha3Code.ENG, "French")
                                         .build());
      when(data.entryCodeMapping()).thenReturn(new DefaultEntryCodeMappingBuilder()
                                                     .mapping(ReferentialCode.<Iso6391Alpha2, Iso6391Alpha2Referential>of(
                                                           "ISO_6391_ALPHA2"), Iso6391Alpha2Code.FR)
                                                     .mapping(ReferentialCode.<Iso6392Alpha3, Iso6392Alpha3Referential>of(
                                                           "ISO_6392_ALPHA3"), Iso6392Alpha3Code.FRA)
                                                     .build());

      return data;
   }

   private static Iso6392Alpha3Data mockIso6392Alpha3ZuluData() {
      Iso6392Alpha3Data data = spy(Iso6392Alpha3Data.class);

      when(data.name()).thenReturn(new DefaultTranslatableName.DefaultTranslatableNameBuilder()
                                         .name(Iso6392Alpha3Code.ENG, "Zulu")
                                         .build());
      when(data.entryCodeMapping()).thenReturn(new DefaultEntryCodeMappingBuilder()
                                                     .mapping(ReferentialCode.<Iso6391Alpha2, Iso6391Alpha2Referential>of(
                                                           "ISO_6391_ALPHA2"), Iso6391Alpha2Code.ZU)
                                                     .mapping(ReferentialCode.<Iso6392Alpha3, Iso6392Alpha3Referential>of(
                                                           "ISO_6392_ALPHA3"), Iso6392Alpha3Code.ZUL)
                                                     .build());

      return data;
   }

   private static Iso6392Alpha3Data mockIso6392Alpha3AfarData() {
      Iso6392Alpha3Data data = spy(Iso6392Alpha3Data.class);

      when(data.name()).thenReturn(new DefaultTranslatableName.DefaultTranslatableNameBuilder()
                                         .name(Iso6392Alpha3Code.ENG, "Afar")
                                         .build());
      when(data.entryCodeMapping()).thenReturn(new DefaultEntryCodeMappingBuilder()
                                                     .mapping(ReferentialCode.<Iso6391Alpha2, Iso6391Alpha2Referential>of(
                                                           "ISO_6391_ALPHA2"), Iso6391Alpha2Code.AA)
                                                     .mapping(ReferentialCode.<Iso6392Alpha3, Iso6392Alpha3Referential>of(
                                                           "ISO_6392_ALPHA3"), Iso6392Alpha3Code.AAR)
                                                     .build());

      return data;
   }

}
