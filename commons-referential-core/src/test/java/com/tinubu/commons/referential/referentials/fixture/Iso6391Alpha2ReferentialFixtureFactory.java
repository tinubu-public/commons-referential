/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.fixture;

import com.tinubu.commons.referential.loaders.ConfigurableReferentialLoader.ConfigurableReferentialLoaderBuilder;
import com.tinubu.commons.referential.referentials.iso6391.Iso6391Alpha2.Iso6391Alpha2Data;
import com.tinubu.commons.referential.referentials.iso6391.Iso6391Alpha2Referential;
import com.tinubu.commons.referential.referentials.iso6391.Iso6391Alpha2Referential.Iso6391Alpha2ReferentialBuilder;
import com.tinubu.commons.test.fixture.DefaultFixtureFactory;
import com.tinubu.commons.test.fixture.junit.FixtureFactory;

@FixtureFactory(Iso6391Alpha2Referential.class)
public class Iso6391Alpha2ReferentialFixtureFactory extends DefaultFixtureFactory<Iso6391Alpha2Referential> {

   @Override
   public Iso6391Alpha2Referential create(int index) throws IndexOutOfBoundsException {
      return new Iso6391Alpha2ReferentialBuilder()
            .referentialLoader(new ConfigurableReferentialLoaderBuilder<Iso6391Alpha2Data>()
                                     .addEntryDatas(new Iso6391Alpha2DataFixtureFactory().createList(3))
                                     .build())
            .build();
   }

}
