/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.fixture;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import com.tinubu.commons.referential.DefaultTranslatableName.DefaultTranslatableNameBuilder;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2Code;
import com.tinubu.commons.referential.referentials.iso31662.Iso31662.Iso31662Data;
import com.tinubu.commons.referential.referentials.iso31662.Iso31662Code;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code;
import com.tinubu.commons.test.fixture.DefaultFixtureFactory;
import com.tinubu.commons.test.fixture.junit.FixtureFactory;

@FixtureFactory(Iso31662Data.class)
public class Iso31662DataFixtureFactory extends DefaultFixtureFactory<Iso31662Data> {

   public static final List<Iso31662Data> DATA = Arrays.asList(mockIso31662FranceAinData(),
                                                               mockIso31662FranceAisneData(),
                                                               mockIso31662ZimbabweMashonalandWestData(),
                                                               mockIso31662AndorraCanilloData(),
                                                               mockIso31662FranceAraData(),
                                                               mockIso31662FranceHdfData());

   @Override
   public Iso31662Data create(int index) throws IndexOutOfBoundsException {
      if (index >= DATA.size()) {
         throw new IndexOutOfBoundsException();
      }
      return DATA.get(index);
   }

   private static Iso31662Data mockIso31662FranceAraData() {
      return mockIso31662Data(Iso31662Code.FR_ARA,
                              null,
                              Iso31661Alpha2Code.FR,
                              "Auvergne-Rhône-Alpes",
                              "Metropolitan region");
   }

   private static Iso31662Data mockIso31662FranceHdfData() {
      return mockIso31662Data(Iso31662Code.FR_HDF,
                              null,
                              Iso31661Alpha2Code.FR,
                              "Hauts-de-France",
                              "Metropolitan region");
   }

   private static Iso31662Data mockIso31662FranceAinData() {
      return mockIso31662Data(Iso31662Code.FR_01,
                              Iso31662Code.FR_ARA,
                              Iso31661Alpha2Code.FR,
                              "Ain",
                              "Metropolitan department");
   }

   private static Iso31662Data mockIso31662FranceAisneData() {
      return mockIso31662Data(Iso31662Code.FR_02,
                              Iso31662Code.FR_HDF,
                              Iso31661Alpha2Code.FR,
                              "Aisne",
                              "Metropolitan department");
   }

   private static Iso31662Data mockIso31662ZimbabweMashonalandWestData() {
      return mockIso31662Data(Iso31662Code.ZW_MW,
                              null,
                              Iso31661Alpha2Code.ZW,
                              "Mashonaland West",
                              "Province");
   }

   private static Iso31662Data mockIso31662AndorraCanilloData() {
      return mockIso31662Data(Iso31662Code.AD_02, null, Iso31661Alpha2Code.AD, "Canillo", "Parish");
   }

   private static Iso31662Data mockIso31662Data(Iso31662Code iso31662Code,
                                                Iso31662Code parent,
                                                Iso31661Alpha2Code country,
                                                String name,
                                                String type) {
      Iso31662Data data = spy(Iso31662Data.class);

      when(data.iso31662()).thenReturn(iso31662Code);
      when(data.country()).thenReturn(country);
      when(data.name()).thenReturn(new DefaultTranslatableNameBuilder()
                                         .name(Iso6392Alpha3Code.ENG, name)
                                         .build());
      when(data.type()).thenReturn(type);
      when(data.parent()).thenReturn(nullable(parent));

      return data;
   }

}
