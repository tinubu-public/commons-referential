/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.fixture;

import java.util.List;

import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2;
import com.tinubu.commons.test.fixture.DefaultFixtureFactory;
import com.tinubu.commons.test.fixture.junit.FixtureFactory;

@FixtureFactory(Iso31661Alpha2.class)
public class Iso31661Alpha2FixtureFactory extends DefaultFixtureFactory<Iso31661Alpha2> {

   @Override
   public Iso31661Alpha2 create(int index) throws IndexOutOfBoundsException {
      List<Iso31661Alpha2> entries = new Iso31661Alpha2ReferentialFixtureFactory().create().entries();

      return entries.get(index % entries.size());
   }

}
