/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.fixture;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.referential.referentials.naceRev2.NaceRev2.NaceRev2EntryType.CLASS;
import static com.tinubu.commons.referential.referentials.naceRev2.NaceRev2.NaceRev2EntryType.SECTION;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import com.tinubu.commons.referential.DefaultTranslatableName.DefaultTranslatableNameBuilder;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code;
import com.tinubu.commons.referential.referentials.naceRev2.NaceRev2.NaceRev2Data;
import com.tinubu.commons.referential.referentials.naceRev2.NaceRev2Code;
import com.tinubu.commons.test.fixture.DefaultFixtureFactory;
import com.tinubu.commons.test.fixture.junit.FixtureFactory;

@FixtureFactory(NaceRev2Data.class)
public class NaceRev2DataFixtureFactory extends DefaultFixtureFactory<NaceRev2Data> {

   public static final List<NaceRev2Data> DATA =
         Arrays.asList(mockNaceRev2AgricultureData(), mockNaceRev2CropActivitiesData());

   @Override
   public NaceRev2Data create(int index) throws IndexOutOfBoundsException {
      if (index >= DATA.size()) {
         throw new IndexOutOfBoundsException();
      }
      return DATA.get(index);
   }

   private static NaceRev2Data mockNaceRev2CropActivitiesData() {
      NaceRev2Data data = spy(NaceRev2Data.class);

      when(data.name()).thenReturn(new DefaultTranslatableNameBuilder()
                                         .name(Iso6392Alpha3Code.ENG, "Post-harvest crop activities")
                                         .name(Iso6392Alpha3Code.FRA, "Traitement primaire des récoltes")
                                         .build());
      when(data.naceRev2()).thenReturn(NaceRev2Code.of("01.63"));
      when(data.type()).thenReturn(CLASS);
      when(data.section()).thenReturn(NaceRev2Code.of("A"));
      when(data.division()).thenReturn(optional(NaceRev2Code.of("01")));
      when(data.group()).thenReturn(optional(NaceRev2Code.of("01.6")));
      when(data.naceClass()).thenReturn(optional(NaceRev2Code.of("01.63")));

      return data;
   }

   private static NaceRev2Data mockNaceRev2AgricultureData() {
      NaceRev2Data data = spy(NaceRev2Data.class);

      when(data.name()).thenReturn(new DefaultTranslatableNameBuilder()
                                         .name(Iso6392Alpha3Code.ENG, "AGRICULTURE, FORESTRY AND FISHING")
                                         .name(Iso6392Alpha3Code.FRA, "AGRICULTURE, SYLVICULTURE ET PÊCHE")
                                         .build());
      when(data.naceRev2()).thenReturn(NaceRev2Code.of("A"));
      when(data.type()).thenReturn(SECTION);
      when(data.section()).thenReturn(NaceRev2Code.of("A"));
      when(data.division()).thenReturn(optional());
      when(data.group()).thenReturn(optional());
      when(data.naceClass()).thenReturn(optional());

      return data;
   }

}