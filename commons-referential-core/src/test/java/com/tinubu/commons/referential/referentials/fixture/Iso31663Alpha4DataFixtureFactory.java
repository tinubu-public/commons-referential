/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.fixture;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import com.tinubu.commons.referential.DefaultTranslatableName.DefaultTranslatableNameBuilder;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2Code;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha3Code;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661NumericCode;
import com.tinubu.commons.referential.referentials.iso31663.Iso31663Alpha4.Iso31663Alpha4Data;
import com.tinubu.commons.referential.referentials.iso31663.Iso31663Alpha4Code;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code;
import com.tinubu.commons.test.fixture.DefaultFixtureFactory;
import com.tinubu.commons.test.fixture.junit.FixtureFactory;

@FixtureFactory(Iso31663Alpha4Data.class)
public class Iso31663Alpha4DataFixtureFactory extends DefaultFixtureFactory<Iso31663Alpha4Data> {

   public static final List<Iso31663Alpha4Data> DATA = Arrays.asList(mockIso31663Alpha4AfarsData(),
                                                                     mockIso31663Alpha4AntillesData(),
                                                                     mockIso31663Alpha4ZaireData());

   @Override
   public Iso31663Alpha4Data create(int index) throws IndexOutOfBoundsException {
      if (index >= DATA.size()) {
         throw new IndexOutOfBoundsException();
      }
      return DATA.get(index);
   }

   private static Iso31663Alpha4Data mockIso31663Alpha4AfarsData() {
      return mockIso31663Alpha4AData(Iso31663Alpha4Code.AIDJ,
                                     Iso31661Alpha2Code.of("AI"),
                                     Iso31661Alpha3Code.of("AFI"),
                                     Iso31661NumericCode.of("262"),
                                     "French Afars and Issas",
                                     LocalDate.of(1977, 1, 1));
   }

   private static Iso31663Alpha4Data mockIso31663Alpha4AntillesData() {
      return mockIso31663Alpha4AData(Iso31663Alpha4Code.ANHH,
                                     Iso31661Alpha2Code.of("AN"),
                                     Iso31661Alpha3Code.of("ANT"),
                                     Iso31661NumericCode.of("530"),
                                     "Netherlands Antilles",
                                     LocalDate.of(1993, 7, 12));
   }

   private static Iso31663Alpha4Data mockIso31663Alpha4ZaireData() {
      return mockIso31663Alpha4AData(Iso31663Alpha4Code.ZRCD,
                                     Iso31661Alpha2Code.of("ZR"),
                                     Iso31661Alpha3Code.of("ZAR"),
                                     Iso31661NumericCode.of("180"),
                                     "Zaire, Republic of",
                                     LocalDate.of(1997, 7, 14));
   }

   private static Iso31663Alpha4Data mockIso31663Alpha4AData(Iso31663Alpha4Code alpha4Code,
                                                             Iso31661Alpha2Code alpha2Code,
                                                             Iso31661Alpha3Code alpha3Code,
                                                             Iso31661NumericCode numericCode,
                                                             String name,
                                                             LocalDate withdrawalDate) {
      Iso31663Alpha4Data data = spy(Iso31663Alpha4Data.class);

      when(data.iso31663Alpha4()).thenReturn(alpha4Code);
      when(data.iso31661Alpha2()).thenReturn(alpha2Code);
      when(data.iso31661Alpha3()).thenReturn(alpha3Code);
      when(data.iso31661Numeric()).thenReturn(optional(numericCode));
      when(data.name()).thenReturn(new DefaultTranslatableNameBuilder()
                                         .name(Iso6392Alpha3Code.ENG, name)
                                         .build());
      when(data.withdrawalDate()).thenReturn(withdrawalDate);

      return data;
   }

}
