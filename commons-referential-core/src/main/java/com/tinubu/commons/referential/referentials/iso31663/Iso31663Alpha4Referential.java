/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.iso31663;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.referential.AbstractReferential;
import com.tinubu.commons.referential.EntryDataLoader;
import com.tinubu.commons.referential.Referential;
import com.tinubu.commons.referential.ReferentialCode;
import com.tinubu.commons.referential.referentials.iso31663.Iso31663Alpha4.Iso31663Alpha4Data;

/**
 * ISO 3166-3 alpha-4 country referential.
 */
public class Iso31663Alpha4Referential extends AbstractReferential<Iso31663Alpha4, Iso31663Alpha4Data>
      implements Referential<Iso31663Alpha4> {

   public static final ReferentialCode<Iso31663Alpha4, Iso31663Alpha4Referential> DEFAULT_REFERENTIAL_CODE =
         ReferentialCode.of("ISO_31663_ALPHA4");

   private static final String DISPLAY_NAME = "ISO 3166-3 alpha-4";

   private Iso31663Alpha4Referential(ReferentialCode<Iso31663Alpha4, Iso31663Alpha4Referential> referentialCode,
                                     EntryDataLoader<Iso31663Alpha4Data> entryDataLoader) {
      super(referentialCode, entryDataLoader);
   }

   @Override
   public String displayName() {
      return DISPLAY_NAME;
   }

   @Override
   protected Iso31663Alpha4 loadEntry(Iso31663Alpha4Data entryData) {
      notNull(entryData, "entryData");

      return new Iso31663Alpha4(this, entryData);
   }

   public static class Iso31663Alpha4ReferentialBuilder {

      private ReferentialCode<Iso31663Alpha4, Iso31663Alpha4Referential> referentialCode =
            DEFAULT_REFERENTIAL_CODE;
      private EntryDataLoader<Iso31663Alpha4Data> entryDataLoader;

      public Iso31663Alpha4ReferentialBuilder referentialCode(ReferentialCode<Iso31663Alpha4, Iso31663Alpha4Referential> referentialCode) {
         this.referentialCode = referentialCode;
         return this;
      }

      public Iso31663Alpha4ReferentialBuilder referentialLoader(EntryDataLoader<Iso31663Alpha4Data> entryDataLoader) {
         this.entryDataLoader = entryDataLoader;
         return this;
      }

      public ReferentialCode<Iso31663Alpha4, Iso31663Alpha4Referential> referentialCode() {
         return referentialCode;
      }

      public Class<Iso31663Alpha4Referential> referentialClass() {
         return Iso31663Alpha4Referential.class;
      }

      public Iso31663Alpha4Referential build() {
         return new Iso31663Alpha4Referential(referentialCode, entryDataLoader);
      }
   }

}
