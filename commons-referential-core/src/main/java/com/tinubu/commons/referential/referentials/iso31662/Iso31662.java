/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.iso31662;

import java.util.Optional;

import com.tinubu.commons.referential.AbstractEntry;
import com.tinubu.commons.referential.Entry;
import com.tinubu.commons.referential.EntryData;
import com.tinubu.commons.referential.TranslatableName;
import com.tinubu.commons.referential.UnknownEntryException;
import com.tinubu.commons.referential.referentials.IsoReferentials;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2Code;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2Referential;
import com.tinubu.commons.referential.referentials.iso31662.Iso31662.Iso31662Data;

/**
 * Represents a ISO 3166-2 country subdivision.
 * Each subdivision is associated with {@link Iso31662Data}.
 */
public class Iso31662 extends AbstractEntry<Iso31662Referential, Iso31662, Iso31662Data>
      implements Entry<Iso31662> {

   Iso31662(Iso31662Referential referential, Iso31662Data entryData) {
      super(referential, entryData);
   }

   @Override
   public Iso31662Code entryCode() {
      return entryData.iso31662();
   }

   @Override
   public TranslatableName entryName() {
      return entryData.name();
   }

   /**
    * Returns {@link Iso31661Alpha2} country associated with this subdivision.
    *
    * @return returns ISO 3166-1 alpha-2 country associated with this subdivision
    *
    * @throws UnknownEntryException if {@link Iso31662Data#country() country code}
    *       is not available in {@link Iso31661Alpha2Referential}
    */
   public Iso31661Alpha2 country() {
      return IsoReferentials.iso31661Alpha2(entryData.country());
   }

   /**
    * Returns parent subdivision for this subdivision.
    *
    * @return parent subdivision for this subdivision or {@link Optional#empty} if there's no parent
    *       subdivision
    */
   public Optional<Iso31662> parent() {
      return entryData.parent().map(IsoReferentials::iso31662);
   }

   /**
    * Defines ISO 3166-2 data.
    */
   public interface Iso31662Data extends EntryData {

      /** Entry code. */
      Iso31662Code iso31662();

      /** Country code in {@link Iso31661Alpha2} referential for this subdivision code. */
      Iso31661Alpha2Code country();

      /** Name translations for this entry. */
      TranslatableName name();

      /** Subdivision type. */
      String type();

      /** Optional parent subdivision code for this subdivision code. */
      Optional<Iso31662Code> parent();
   }

}
