/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.iso31661;

import com.tinubu.commons.referential.AbstractReferential;
import com.tinubu.commons.referential.EntryDataLoader;
import com.tinubu.commons.referential.ReferentialCode;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Numeric.Iso31661NumericData;

/**
 * ISO 3166-1 numeric country referential.
 * Numeric values are referenced in UN M49 referential.
 */
public class Iso31661NumericReferential extends AbstractReferential<Iso31661Numeric, Iso31661NumericData> {

   public static final ReferentialCode<Iso31661Numeric, Iso31661NumericReferential> DEFAULT_REFERENTIAL_CODE =
         ReferentialCode.of("ISO_31661_NUMERIC");

   private static final String DISPLAY_NAME = "ISO 3166-1 numeric";

   private Iso31661NumericReferential(ReferentialCode<Iso31661Numeric, Iso31661NumericReferential> referentialCode,
                                      EntryDataLoader<Iso31661NumericData> entryDataLoader) {
      super(referentialCode, entryDataLoader);
   }

   @Override
   public String displayName() {
      return DISPLAY_NAME;
   }

   @Override
   public Iso31661Numeric loadEntry(Iso31661NumericData entryData) {
      return new Iso31661Numeric(this, entryData);
   }

   public static class Iso31661NumericReferentialBuilder {

      private ReferentialCode<Iso31661Numeric, Iso31661NumericReferential> referentialCode =
            DEFAULT_REFERENTIAL_CODE;
      private EntryDataLoader<Iso31661NumericData> entryDataLoader;

      public Iso31661NumericReferentialBuilder referentialCode(ReferentialCode<Iso31661Numeric, Iso31661NumericReferential> referentialCode) {
         this.referentialCode = referentialCode;
         return this;
      }

      public Iso31661NumericReferentialBuilder referentialLoader(EntryDataLoader<Iso31661NumericData> entryDataLoader) {
         this.entryDataLoader = entryDataLoader;
         return this;
      }

      public ReferentialCode<Iso31661Numeric, Iso31661NumericReferential> referentialCode() {
         return referentialCode;
      }

      public Class<Iso31661NumericReferential> referentialClass() {
         return Iso31661NumericReferential.class;
      }

      public Iso31661NumericReferential build() {
         return new Iso31661NumericReferential(referentialCode, entryDataLoader);
      }
   }

}
