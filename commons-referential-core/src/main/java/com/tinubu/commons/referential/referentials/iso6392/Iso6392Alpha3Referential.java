/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.iso6392;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.referential.AbstractReferential;
import com.tinubu.commons.referential.EntryDataLoader;
import com.tinubu.commons.referential.Referential;
import com.tinubu.commons.referential.ReferentialCode;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3.Iso6392Alpha3Data;

/**
 * ISO 639-2 alpha-3 country referential.
 */
public class Iso6392Alpha3Referential extends AbstractReferential<Iso6392Alpha3, Iso6392Alpha3Data>
      implements Referential<Iso6392Alpha3> {

   public static final ReferentialCode<Iso6392Alpha3, Iso6392Alpha3Referential> DEFAULT_REFERENTIAL_CODE =
         ReferentialCode.of("ISO_6392_ALPHA3");

   private static final String DISPLAY_NAME = "ISO 639-2 alpha-3";

   private Iso6392Alpha3Referential(ReferentialCode<Iso6392Alpha3, Iso6392Alpha3Referential> referentialCode,
                                    EntryDataLoader<Iso6392Alpha3Data> entryDataLoader) {
      super(referentialCode, entryDataLoader);
   }

   @Override
   public String displayName() {
      return DISPLAY_NAME;
   }

   @Override
   protected Iso6392Alpha3 loadEntry(Iso6392Alpha3Data entryData) {
      notNull(entryData, "entryData");

      return new Iso6392Alpha3(this, entryData);
   }

   public static class Iso6392Alpha3ReferentialBuilder {

      private ReferentialCode<Iso6392Alpha3, Iso6392Alpha3Referential> referentialCode =
            DEFAULT_REFERENTIAL_CODE;
      private EntryDataLoader<Iso6392Alpha3Data> entryDataLoader;

      public Iso6392Alpha3ReferentialBuilder referentialCode(ReferentialCode<Iso6392Alpha3, Iso6392Alpha3Referential> referentialCode) {
         this.referentialCode = referentialCode;
         return this;
      }

      public Iso6392Alpha3ReferentialBuilder referentialLoader(EntryDataLoader<Iso6392Alpha3Data> entryDataLoader) {
         this.entryDataLoader = entryDataLoader;
         return this;
      }

      public ReferentialCode<Iso6392Alpha3, Iso6392Alpha3Referential> referentialCode() {
         return referentialCode;
      }

      public Class<Iso6392Alpha3Referential> referentialClass() {
         return Iso6392Alpha3Referential.class;
      }

      public Iso6392Alpha3Referential build() {
         return new Iso6392Alpha3Referential(referentialCode, entryDataLoader);
      }
   }

}
