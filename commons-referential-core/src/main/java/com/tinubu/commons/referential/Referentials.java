/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notEmpty;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ServiceLoader;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Main factory for {@link Referential} and {@link Entry}. Factory supports manual registration using
 * {@link #registerReferential(ReferentialProvider)}. Referentials declared using {@link
 * ReferentialProvider} plugin will be auto-registered.
 */
public class Referentials {

   private static final Map<ReferentialCode<?, ?>, ReferentialProvider> registeredReferentialsByCode =
         new ConcurrentHashMap<>();

   private static final Map<ReferentialCode<?, ?>, Referential<?>> loadedReferentials =
         new ConcurrentHashMap<>();

   static {
      preRegisteredReferentials().forEach(Referentials::registerReferential);
   }

   /**
    * Registers manually a referential. Referential will be lazily loaded at first use. If a referential with
    * the same code is already registered, it will be replaced.
    *
    * @param referentialProvider referential provider definition
    */
   public static void registerReferential(ReferentialProvider referentialProvider) {
      notNull(referentialProvider, "referentialProvider");

      registeredReferentialsByCode.put(referentialProvider.referentialCode(), referentialProvider);
   }

   /**
    * Resets all currently registered referentials.
    */
   public static void clearRegisteredReferentials() {
      registeredReferentialsByCode.clear();
   }

   /**
    * Returns currently registered referentials.
    *
    * @return currently registered referentials
    */
   public static List<ReferentialProvider> registeredReferentials() {
      return new ArrayList<>(registeredReferentialsByCode.values());
   }

   /**
    * Returns registered referential matching specified code.
    * <p>
    * Referential is lazily initialized and cached at first call.
    *
    * @param referentialCode referential code
    *
    * @return referential matching specified code
    */
   @SuppressWarnings("unchecked")
   public static <E extends Entry<E>, R extends Referential<E>> R referential(ReferentialCode<E, R> referentialCode) {
      notNull(referentialCode, "referentialCode");

      return (R) cachedReferential(referentialCode);
   }

   /**
    * Searches an entry in a specified list of referentials. Caution, multiple referentials can match for the
    * same code, the first matching referential will be used.
    * <p>
    * The entry is returned from the first referential providing the specified code.
    * Referentials are lazily initialized and cached until a referential match the specified code.
    *
    * @param entryCode entry code to search
    * @param referentialCodes non-empty list of referential codes to search in
    *
    * @return entry or {@link Optional#empty()}
    */
   @SuppressWarnings("unchecked")
   public static Optional<Entry<?>> entry(EntryCode entryCode, ReferentialCode<?, ?>... referentialCodes) {
      notNull(entryCode, "entryCode");
      notEmpty(noNullElements(referentialCodes, "referentialCodes"), "referentialCodes");

      Optional<Entry<?>> entry = Optional.empty();

      for (ReferentialCode<?, ?> referentialCode : referentialCodes) {
         Referential<?> referential = referential(referentialCode);
         if ((entry = (Optional<Entry<?>>) referential.optionalEntry(entryCode)).isPresent()) {
            break;
         }
      }

      return entry;
   }

   private static List<ReferentialProvider> preRegisteredReferentials() {
      return pluginReferentials();
   }

   private static List<ReferentialProvider> pluginReferentials() {
      ServiceLoader<ReferentialProvider> referentialProviders = ServiceLoader.load(ReferentialProvider.class);

      return list(stream(referentialProviders));
   }

   private static Referential<?> cachedReferential(ReferentialCode<?, ?> referentialCode) {
      ReferentialProvider referentialProvider = registeredReferential(referentialCode);

      return loadedReferentials.computeIfAbsent(referentialCode, __ -> loadReferential(referentialProvider));
   }

   private static Referential<?> loadReferential(ReferentialProvider referentialProvider) {
      try {
         return referentialProvider.build();
      } catch (Exception e) {
         throw new ReferentialInitializationException(String.format("Can't load '%s' referential of type '%s'",
                                                                    referentialProvider
                                                                          .referentialCode()
                                                                          .referentialCode(),
                                                                    referentialProvider
                                                                          .referentialClass()
                                                                          .getSimpleName()), e);
      }
   }

   private static ReferentialProvider registeredReferential(ReferentialCode<?, ?> referentialCode) {
      return nullable(registeredReferentialsByCode.get(referentialCode)).orElseThrow(() -> new UnknownReferentialException(
            referentialCode));
   }

}
