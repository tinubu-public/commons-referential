/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.naceRev2;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.referential.AbstractReferential;
import com.tinubu.commons.referential.EntryDataLoader;
import com.tinubu.commons.referential.Referential;
import com.tinubu.commons.referential.ReferentialCode;
import com.tinubu.commons.referential.referentials.naceRev2.NaceRev2.NaceRev2Data;

/**
 * NACE Rev.2 activity referential.
 * NACE is for "statistical classification of economic activities in the European Community".
 */
public class NaceRev2Referential extends AbstractReferential<NaceRev2, NaceRev2Data>
      implements Referential<NaceRev2> {

   public static final ReferentialCode<NaceRev2, NaceRev2Referential> DEFAULT_REFERENTIAL_CODE =
         ReferentialCode.of("NACE_REV2");

   private static final String DISPLAY_NAME = "NACE Rev.2";

   private NaceRev2Referential(ReferentialCode<NaceRev2, NaceRev2Referential> referentialCode,
                               EntryDataLoader<NaceRev2Data> entryDataLoader) {
      super(referentialCode, entryDataLoader);
   }

   @Override
   public String displayName() {
      return DISPLAY_NAME;
   }

   @Override
   protected NaceRev2 loadEntry(NaceRev2Data entryData) {
      notNull(entryData, "entryData");

      return new NaceRev2(this, entryData);
   }

   public static class NaceRev2ReferentialBuilder {

      private ReferentialCode<NaceRev2, NaceRev2Referential> referentialCode = DEFAULT_REFERENTIAL_CODE;
      private EntryDataLoader<NaceRev2Data> entryDataLoader;

      public NaceRev2ReferentialBuilder referentialCode(ReferentialCode<NaceRev2, NaceRev2Referential> referentialCode) {
         this.referentialCode = referentialCode;
         return this;
      }

      public NaceRev2ReferentialBuilder referentialLoader(EntryDataLoader<NaceRev2Data> entryDataLoader) {
         this.entryDataLoader = entryDataLoader;
         return this;
      }

      public ReferentialCode<NaceRev2, NaceRev2Referential> referentialCode() {
         return referentialCode;
      }

      public Class<NaceRev2Referential> referentialClass() {
         return NaceRev2Referential.class;
      }

      public NaceRev2Referential build() {
         return new NaceRev2Referential(referentialCode, entryDataLoader);
      }

   }

}
