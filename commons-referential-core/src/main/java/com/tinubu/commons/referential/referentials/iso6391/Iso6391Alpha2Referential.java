/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.iso6391;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.referential.AbstractReferential;
import com.tinubu.commons.referential.EntryDataLoader;
import com.tinubu.commons.referential.Referential;
import com.tinubu.commons.referential.ReferentialCode;
import com.tinubu.commons.referential.referentials.iso6391.Iso6391Alpha2.Iso6391Alpha2Data;

/**
 * ISO 639-1 alpha-2 country referential.
 */
public class Iso6391Alpha2Referential extends AbstractReferential<Iso6391Alpha2, Iso6391Alpha2Data>
      implements Referential<Iso6391Alpha2> {

   public static final ReferentialCode<Iso6391Alpha2, Iso6391Alpha2Referential> DEFAULT_REFERENTIAL_CODE =
         ReferentialCode.of("ISO_6391_ALPHA2");

   private static final String DISPLAY_NAME = "ISO 639-1 alpha-2";

   private Iso6391Alpha2Referential(ReferentialCode<Iso6391Alpha2, Iso6391Alpha2Referential> referentialCode,
                                    EntryDataLoader<Iso6391Alpha2Data> entryDataLoader) {
      super(referentialCode, entryDataLoader);
   }

   @Override
   public String displayName() {
      return DISPLAY_NAME;
   }

   @Override
   protected Iso6391Alpha2 loadEntry(Iso6391Alpha2Data entryData) {
      notNull(entryData, "entryData");

      return new Iso6391Alpha2(this, entryData);
   }

   public static class Iso6391Alpha2ReferentialBuilder {

      private ReferentialCode<Iso6391Alpha2, Iso6391Alpha2Referential> referentialCode =
            DEFAULT_REFERENTIAL_CODE;
      private EntryDataLoader<Iso6391Alpha2Data> entryDataLoader;

      public Iso6391Alpha2ReferentialBuilder referentialCode(ReferentialCode<Iso6391Alpha2, Iso6391Alpha2Referential> referentialCode) {
         this.referentialCode = referentialCode;
         return this;
      }

      public Iso6391Alpha2ReferentialBuilder referentialLoader(EntryDataLoader<Iso6391Alpha2Data> entryDataLoader) {
         this.entryDataLoader = entryDataLoader;
         return this;
      }

      public ReferentialCode<Iso6391Alpha2, Iso6391Alpha2Referential> referentialCode() {
         return referentialCode;
      }

      public Class<Iso6391Alpha2Referential> referentialClass() {
         return Iso6391Alpha2Referential.class;
      }

      public Iso6391Alpha2Referential build() {
         return new Iso6391Alpha2Referential(referentialCode, entryDataLoader);
      }
   }

}
