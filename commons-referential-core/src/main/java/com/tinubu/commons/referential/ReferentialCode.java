/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential;

import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * {@link com.tinubu.commons.referential.Referential} identification code.
 */
public class ReferentialCode<E extends Entry<E>, R extends Referential<E>> implements Comparable<ReferentialCode<E, R>> {

   private final String referentialCode;

   protected ReferentialCode(String referentialCode) {
      this.referentialCode = notBlank(referentialCode, "referentialCode");
   }

   public static <E extends Entry<E>, R extends Referential<E>> ReferentialCode<E, R> of(String referentialCode) {
      return new ReferentialCode<>(referentialCode);
   }

   public static ReferentialCode<? extends Entry<?>, ? extends Referential<?>> untypedOf(String referentialCode) {
      return new ReferentialCode<>(referentialCode);
   }

   public String referentialCode() {
      return referentialCode;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      ReferentialCode<?, ?> that = (ReferentialCode<?, ?>) o;
      return Objects.equals(referentialCode, that.referentialCode);
   }

   @Override
   public int hashCode() {
      return Objects.hash(referentialCode);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", getClass().getSimpleName() + "[", "]")
            .add("referentialCode='" + referentialCode + "'")
            .toString();
   }

   @Override
   public int compareTo(ReferentialCode referentialCode) {
      notNull(this.referentialCode, "referentialCode");

      return this.referentialCode.compareTo(referentialCode.referentialCode);
   }
}
