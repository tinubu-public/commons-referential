/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential;

import static com.tinubu.commons.lang.model.ImmutableUtils.immutableMap;
import static com.tinubu.commons.lang.model.MutableUtils.mutableMap;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

/**
 * Default entry code mapping implementation.
 */
public class DefaultEntryCodeMapping implements EntryCodeMapping {

   private final Map<ReferentialCode<?, ?>, EntryCode> mapping;

   private DefaultEntryCodeMapping(DefaultEntryCodeMappingBuilder builder) {
      this.mapping = immutableMap(notNull(builder.mapping, "mapping"));
   }

   public Optional<EntryCode> mapping(ReferentialCode<?, ?> referential) {
      return nullable(mapping.get(referential));
   }

   @Override
   public Map<ReferentialCode<?, ?>, EntryCode> mapping() {
      return mapping;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      DefaultEntryCodeMapping that = (DefaultEntryCodeMapping) o;
      return Objects.equals(mapping, that.mapping);
   }

   @Override
   public int hashCode() {
      return Objects.hash(mapping);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", DefaultEntryCodeMapping.class.getSimpleName() + "[", "]")
            .add("mapping=" + mapping)
            .toString();
   }

   public static class DefaultEntryCodeMappingBuilder {
      private Map<ReferentialCode<?, ?>, EntryCode> mapping = mutableMap();

      public static DefaultEntryCodeMappingBuilder from(EntryCodeMapping entryCodeMapping) {
         return new DefaultEntryCodeMappingBuilder().mapping(entryCodeMapping.mapping());
      }

      public DefaultEntryCodeMappingBuilder mapping(Map<ReferentialCode<?, ?>, EntryCode> mapping) {
         notNull(mapping, "mapping");

         this.mapping = mutableMap(mapping);

         return this;
      }

      public DefaultEntryCodeMappingBuilder mapping(ReferentialCode<?, ?> referential, EntryCode entryCode) {
         notNull(referential, "referential");
         notNull(entryCode, "entryCode");

         mapping.put(referential, entryCode);

         return this;
      }

      public DefaultEntryCodeMapping build() {
         return new DefaultEntryCodeMapping(this);
      }
   }
}
