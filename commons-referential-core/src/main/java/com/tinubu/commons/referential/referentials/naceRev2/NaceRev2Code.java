/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.naceRev2;

import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.tinubu.commons.referential.Entry;
import com.tinubu.commons.referential.EntryCode;
import com.tinubu.commons.referential.ReferentialCode;

/**
 * NACE Rev.2 activity code.
 * Activity code format is either :
 * <ul>
 * <li>Section code : {@code <SECTION>} in uppercase</li>
 * <li>Division code : {@code <2-digits>}</li>
 * <li>Group code : {@code <2-digits>.<1-digit>}</li>
 * <li>Class code : {@code <2-digits>.<2-digit>}</li>
 * </ul>
 */
public class NaceRev2Code extends EntryCode {

   private static final Pattern NACE_LENIENT_FORMAT = Pattern.compile("(?:"
                                                                      + "(?<section>[A-Z])|"
                                                                      + "(?<division>[0-9]{2})|"
                                                                      + "(?<group>(?<group1>[0-9]{2})\\.?(?<group2>[0-9]))|"
                                                                      + "(?<class>(?<class1>[0-9]{2})\\.?(?<class2>[0-9])\\.?(?<class3>[0-9]))"
                                                                      + ")");

   private NaceRev2Code(String entryCode) {
      super(entryCode);
   }

   /**
    * Creates NACE Rev.2 code from entry code. Format supported are :
    * <ul>
    * <li>Section code : {@code <SECTION>} (case insensitive)</li>
    * <li>Division code : {@code <2-digits>}</li>
    * <li>Group code : {@code <2-digits>.<1-digit>}</li>
    * <li>Class code : {@code <2-digits>.<2-digit>}</li>
    * </ul>
    */
   public static NaceRev2Code of(String entryCode) {
      return new NaceRev2Code(notNull(entryCode, "entryCode").toUpperCase());
   }

   /**
    * Creates NACE Rev.2 code from entry code with lenient parsing. Format supported are :
    * <ul>
    * <li>Section code : {@code <SECTION>}</li>
    * <li>Division code : {@code <2-digits>}</li>
    * <li>Group code : {@code <2-digits>[.]<1-digit>}</li>
    * <li>Class code : {@code <2-digits>[.]<1-digit>[.]<1-digit>}</li>
    * </ul>
    *
    * @apiNote Lenient parser does not support optional section in front of division/group/class
    *       codes, because parser only checks code format, not code semantic, and it would be then possible
    *       to parse the erroneous code {@code Z01.1.1} and retrieve it successfully, because the section is
    *       ignored at parsing time and {@code 01.1.1} is valid.
    */
   public static NaceRev2Code ofLenient(String entryCode) {
      String normalizedEntryCode = notBlank(entryCode, "entryCode").toUpperCase();

      Matcher parser = NACE_LENIENT_FORMAT.matcher(normalizedEntryCode);

      if (parser.matches()) {
         if (parser.group("section") != null) {
            return new NaceRev2Code(parser.group("section"));
         } else if (parser.group("division") != null) {
            return new NaceRev2Code(parser.group("division"));
         } else if (parser.group("group") != null) {
            return new NaceRev2Code(parser.group("group1") + "." + parser.group("group2"));
         } else {
            return new NaceRev2Code(parser.group("class1") + "." + parser.group("class2") + parser.group(
                  "class3"));
         }
      }
      return new NaceRev2Code(normalizedEntryCode);
   }

   /**
    * Returns NACE Rev.2 entry code without dots :
    * <ul>
    * <li>Section code : {@code <SECTION>} in uppercase</li>
    * <li>Division code : {@code <2-digits>}</li>
    * <li>Group code : {@code <3-digits>}</li>
    * <li>Class code : {@code <4-digits>}</li>
    * </ul>
    * <p>
    * This method should *not* be used to map NACE to ISIC4 codes, instead, you should use {@link Entry#entryMapping(ReferentialCode) entry mapping}.
    *
    * @return NACE Rev.2 entry code without dots
    */
   public String undottedEntryCode() {
      return super.entryCode().replace(".", "");
   }

   @Override
   protected boolean validEntryCodeFormat(String entryCode) {
      switch (entryCode.length()) {
         case 1:
            return entryCode.chars().allMatch(c -> c >= 'A' && c <= 'Z');
         case 2:
            return entryCode.chars().allMatch(c -> c >= '0' && c <= '9');
         case 4:
         case 5:
            return entryCode.chars().limit(2).allMatch(c -> c >= '0' && c <= '9')
                   && entryCode.charAt(2) == '.'
                   && entryCode.chars().skip(3).allMatch(c -> c >= '0' && c <= '9');
         default:
            return false;
      }

   }

   /* Generated codes at 2022-01-12T18:29:57+01:00 using default loader. */
   public static final NaceRev2Code _01 = NaceRev2Code.of("01");
   public static final NaceRev2Code _01_1 = NaceRev2Code.of("01.1");
   public static final NaceRev2Code _01_11 = NaceRev2Code.of("01.11");
   public static final NaceRev2Code _01_12 = NaceRev2Code.of("01.12");
   public static final NaceRev2Code _01_13 = NaceRev2Code.of("01.13");
   public static final NaceRev2Code _01_14 = NaceRev2Code.of("01.14");
   public static final NaceRev2Code _01_15 = NaceRev2Code.of("01.15");
   public static final NaceRev2Code _01_16 = NaceRev2Code.of("01.16");
   public static final NaceRev2Code _01_19 = NaceRev2Code.of("01.19");
   public static final NaceRev2Code _01_2 = NaceRev2Code.of("01.2");
   public static final NaceRev2Code _01_21 = NaceRev2Code.of("01.21");
   public static final NaceRev2Code _01_22 = NaceRev2Code.of("01.22");
   public static final NaceRev2Code _01_23 = NaceRev2Code.of("01.23");
   public static final NaceRev2Code _01_24 = NaceRev2Code.of("01.24");
   public static final NaceRev2Code _01_25 = NaceRev2Code.of("01.25");
   public static final NaceRev2Code _01_26 = NaceRev2Code.of("01.26");
   public static final NaceRev2Code _01_27 = NaceRev2Code.of("01.27");
   public static final NaceRev2Code _01_28 = NaceRev2Code.of("01.28");
   public static final NaceRev2Code _01_29 = NaceRev2Code.of("01.29");
   public static final NaceRev2Code _01_3 = NaceRev2Code.of("01.3");
   public static final NaceRev2Code _01_30 = NaceRev2Code.of("01.30");
   public static final NaceRev2Code _01_4 = NaceRev2Code.of("01.4");
   public static final NaceRev2Code _01_41 = NaceRev2Code.of("01.41");
   public static final NaceRev2Code _01_42 = NaceRev2Code.of("01.42");
   public static final NaceRev2Code _01_43 = NaceRev2Code.of("01.43");
   public static final NaceRev2Code _01_44 = NaceRev2Code.of("01.44");
   public static final NaceRev2Code _01_45 = NaceRev2Code.of("01.45");
   public static final NaceRev2Code _01_46 = NaceRev2Code.of("01.46");
   public static final NaceRev2Code _01_47 = NaceRev2Code.of("01.47");
   public static final NaceRev2Code _01_49 = NaceRev2Code.of("01.49");
   public static final NaceRev2Code _01_5 = NaceRev2Code.of("01.5");
   public static final NaceRev2Code _01_50 = NaceRev2Code.of("01.50");
   public static final NaceRev2Code _01_6 = NaceRev2Code.of("01.6");
   public static final NaceRev2Code _01_61 = NaceRev2Code.of("01.61");
   public static final NaceRev2Code _01_62 = NaceRev2Code.of("01.62");
   public static final NaceRev2Code _01_63 = NaceRev2Code.of("01.63");
   public static final NaceRev2Code _01_64 = NaceRev2Code.of("01.64");
   public static final NaceRev2Code _01_7 = NaceRev2Code.of("01.7");
   public static final NaceRev2Code _01_70 = NaceRev2Code.of("01.70");
   public static final NaceRev2Code _02 = NaceRev2Code.of("02");
   public static final NaceRev2Code _02_1 = NaceRev2Code.of("02.1");
   public static final NaceRev2Code _02_10 = NaceRev2Code.of("02.10");
   public static final NaceRev2Code _02_2 = NaceRev2Code.of("02.2");
   public static final NaceRev2Code _02_20 = NaceRev2Code.of("02.20");
   public static final NaceRev2Code _02_3 = NaceRev2Code.of("02.3");
   public static final NaceRev2Code _02_30 = NaceRev2Code.of("02.30");
   public static final NaceRev2Code _02_4 = NaceRev2Code.of("02.4");
   public static final NaceRev2Code _02_40 = NaceRev2Code.of("02.40");
   public static final NaceRev2Code _03 = NaceRev2Code.of("03");
   public static final NaceRev2Code _03_1 = NaceRev2Code.of("03.1");
   public static final NaceRev2Code _03_11 = NaceRev2Code.of("03.11");
   public static final NaceRev2Code _03_12 = NaceRev2Code.of("03.12");
   public static final NaceRev2Code _03_2 = NaceRev2Code.of("03.2");
   public static final NaceRev2Code _03_21 = NaceRev2Code.of("03.21");
   public static final NaceRev2Code _03_22 = NaceRev2Code.of("03.22");
   public static final NaceRev2Code _05 = NaceRev2Code.of("05");
   public static final NaceRev2Code _05_1 = NaceRev2Code.of("05.1");
   public static final NaceRev2Code _05_10 = NaceRev2Code.of("05.10");
   public static final NaceRev2Code _05_2 = NaceRev2Code.of("05.2");
   public static final NaceRev2Code _05_20 = NaceRev2Code.of("05.20");
   public static final NaceRev2Code _06 = NaceRev2Code.of("06");
   public static final NaceRev2Code _06_1 = NaceRev2Code.of("06.1");
   public static final NaceRev2Code _06_10 = NaceRev2Code.of("06.10");
   public static final NaceRev2Code _06_2 = NaceRev2Code.of("06.2");
   public static final NaceRev2Code _06_20 = NaceRev2Code.of("06.20");
   public static final NaceRev2Code _07 = NaceRev2Code.of("07");
   public static final NaceRev2Code _07_1 = NaceRev2Code.of("07.1");
   public static final NaceRev2Code _07_10 = NaceRev2Code.of("07.10");
   public static final NaceRev2Code _07_2 = NaceRev2Code.of("07.2");
   public static final NaceRev2Code _07_21 = NaceRev2Code.of("07.21");
   public static final NaceRev2Code _07_29 = NaceRev2Code.of("07.29");
   public static final NaceRev2Code _08 = NaceRev2Code.of("08");
   public static final NaceRev2Code _08_1 = NaceRev2Code.of("08.1");
   public static final NaceRev2Code _08_11 = NaceRev2Code.of("08.11");
   public static final NaceRev2Code _08_12 = NaceRev2Code.of("08.12");
   public static final NaceRev2Code _08_9 = NaceRev2Code.of("08.9");
   public static final NaceRev2Code _08_91 = NaceRev2Code.of("08.91");
   public static final NaceRev2Code _08_92 = NaceRev2Code.of("08.92");
   public static final NaceRev2Code _08_93 = NaceRev2Code.of("08.93");
   public static final NaceRev2Code _08_99 = NaceRev2Code.of("08.99");
   public static final NaceRev2Code _09 = NaceRev2Code.of("09");
   public static final NaceRev2Code _09_1 = NaceRev2Code.of("09.1");
   public static final NaceRev2Code _09_10 = NaceRev2Code.of("09.10");
   public static final NaceRev2Code _09_9 = NaceRev2Code.of("09.9");
   public static final NaceRev2Code _09_90 = NaceRev2Code.of("09.90");
   public static final NaceRev2Code _10 = NaceRev2Code.of("10");
   public static final NaceRev2Code _10_1 = NaceRev2Code.of("10.1");
   public static final NaceRev2Code _10_11 = NaceRev2Code.of("10.11");
   public static final NaceRev2Code _10_12 = NaceRev2Code.of("10.12");
   public static final NaceRev2Code _10_13 = NaceRev2Code.of("10.13");
   public static final NaceRev2Code _10_2 = NaceRev2Code.of("10.2");
   public static final NaceRev2Code _10_20 = NaceRev2Code.of("10.20");
   public static final NaceRev2Code _10_3 = NaceRev2Code.of("10.3");
   public static final NaceRev2Code _10_31 = NaceRev2Code.of("10.31");
   public static final NaceRev2Code _10_32 = NaceRev2Code.of("10.32");
   public static final NaceRev2Code _10_39 = NaceRev2Code.of("10.39");
   public static final NaceRev2Code _10_4 = NaceRev2Code.of("10.4");
   public static final NaceRev2Code _10_41 = NaceRev2Code.of("10.41");
   public static final NaceRev2Code _10_42 = NaceRev2Code.of("10.42");
   public static final NaceRev2Code _10_5 = NaceRev2Code.of("10.5");
   public static final NaceRev2Code _10_51 = NaceRev2Code.of("10.51");
   public static final NaceRev2Code _10_52 = NaceRev2Code.of("10.52");
   public static final NaceRev2Code _10_6 = NaceRev2Code.of("10.6");
   public static final NaceRev2Code _10_61 = NaceRev2Code.of("10.61");
   public static final NaceRev2Code _10_62 = NaceRev2Code.of("10.62");
   public static final NaceRev2Code _10_7 = NaceRev2Code.of("10.7");
   public static final NaceRev2Code _10_71 = NaceRev2Code.of("10.71");
   public static final NaceRev2Code _10_72 = NaceRev2Code.of("10.72");
   public static final NaceRev2Code _10_73 = NaceRev2Code.of("10.73");
   public static final NaceRev2Code _10_8 = NaceRev2Code.of("10.8");
   public static final NaceRev2Code _10_81 = NaceRev2Code.of("10.81");
   public static final NaceRev2Code _10_82 = NaceRev2Code.of("10.82");
   public static final NaceRev2Code _10_83 = NaceRev2Code.of("10.83");
   public static final NaceRev2Code _10_84 = NaceRev2Code.of("10.84");
   public static final NaceRev2Code _10_85 = NaceRev2Code.of("10.85");
   public static final NaceRev2Code _10_86 = NaceRev2Code.of("10.86");
   public static final NaceRev2Code _10_89 = NaceRev2Code.of("10.89");
   public static final NaceRev2Code _10_9 = NaceRev2Code.of("10.9");
   public static final NaceRev2Code _10_91 = NaceRev2Code.of("10.91");
   public static final NaceRev2Code _10_92 = NaceRev2Code.of("10.92");
   public static final NaceRev2Code _11 = NaceRev2Code.of("11");
   public static final NaceRev2Code _11_0 = NaceRev2Code.of("11.0");
   public static final NaceRev2Code _11_01 = NaceRev2Code.of("11.01");
   public static final NaceRev2Code _11_02 = NaceRev2Code.of("11.02");
   public static final NaceRev2Code _11_03 = NaceRev2Code.of("11.03");
   public static final NaceRev2Code _11_04 = NaceRev2Code.of("11.04");
   public static final NaceRev2Code _11_05 = NaceRev2Code.of("11.05");
   public static final NaceRev2Code _11_06 = NaceRev2Code.of("11.06");
   public static final NaceRev2Code _11_07 = NaceRev2Code.of("11.07");
   public static final NaceRev2Code _12 = NaceRev2Code.of("12");
   public static final NaceRev2Code _12_0 = NaceRev2Code.of("12.0");
   public static final NaceRev2Code _12_00 = NaceRev2Code.of("12.00");
   public static final NaceRev2Code _13 = NaceRev2Code.of("13");
   public static final NaceRev2Code _13_1 = NaceRev2Code.of("13.1");
   public static final NaceRev2Code _13_10 = NaceRev2Code.of("13.10");
   public static final NaceRev2Code _13_2 = NaceRev2Code.of("13.2");
   public static final NaceRev2Code _13_20 = NaceRev2Code.of("13.20");
   public static final NaceRev2Code _13_3 = NaceRev2Code.of("13.3");
   public static final NaceRev2Code _13_30 = NaceRev2Code.of("13.30");
   public static final NaceRev2Code _13_9 = NaceRev2Code.of("13.9");
   public static final NaceRev2Code _13_91 = NaceRev2Code.of("13.91");
   public static final NaceRev2Code _13_92 = NaceRev2Code.of("13.92");
   public static final NaceRev2Code _13_93 = NaceRev2Code.of("13.93");
   public static final NaceRev2Code _13_94 = NaceRev2Code.of("13.94");
   public static final NaceRev2Code _13_95 = NaceRev2Code.of("13.95");
   public static final NaceRev2Code _13_96 = NaceRev2Code.of("13.96");
   public static final NaceRev2Code _13_99 = NaceRev2Code.of("13.99");
   public static final NaceRev2Code _14 = NaceRev2Code.of("14");
   public static final NaceRev2Code _14_1 = NaceRev2Code.of("14.1");
   public static final NaceRev2Code _14_11 = NaceRev2Code.of("14.11");
   public static final NaceRev2Code _14_12 = NaceRev2Code.of("14.12");
   public static final NaceRev2Code _14_13 = NaceRev2Code.of("14.13");
   public static final NaceRev2Code _14_14 = NaceRev2Code.of("14.14");
   public static final NaceRev2Code _14_19 = NaceRev2Code.of("14.19");
   public static final NaceRev2Code _14_2 = NaceRev2Code.of("14.2");
   public static final NaceRev2Code _14_20 = NaceRev2Code.of("14.20");
   public static final NaceRev2Code _14_3 = NaceRev2Code.of("14.3");
   public static final NaceRev2Code _14_31 = NaceRev2Code.of("14.31");
   public static final NaceRev2Code _14_39 = NaceRev2Code.of("14.39");
   public static final NaceRev2Code _15 = NaceRev2Code.of("15");
   public static final NaceRev2Code _15_1 = NaceRev2Code.of("15.1");
   public static final NaceRev2Code _15_11 = NaceRev2Code.of("15.11");
   public static final NaceRev2Code _15_12 = NaceRev2Code.of("15.12");
   public static final NaceRev2Code _15_2 = NaceRev2Code.of("15.2");
   public static final NaceRev2Code _15_20 = NaceRev2Code.of("15.20");
   public static final NaceRev2Code _16 = NaceRev2Code.of("16");
   public static final NaceRev2Code _16_1 = NaceRev2Code.of("16.1");
   public static final NaceRev2Code _16_10 = NaceRev2Code.of("16.10");
   public static final NaceRev2Code _16_2 = NaceRev2Code.of("16.2");
   public static final NaceRev2Code _16_21 = NaceRev2Code.of("16.21");
   public static final NaceRev2Code _16_22 = NaceRev2Code.of("16.22");
   public static final NaceRev2Code _16_23 = NaceRev2Code.of("16.23");
   public static final NaceRev2Code _16_24 = NaceRev2Code.of("16.24");
   public static final NaceRev2Code _16_29 = NaceRev2Code.of("16.29");
   public static final NaceRev2Code _17 = NaceRev2Code.of("17");
   public static final NaceRev2Code _17_1 = NaceRev2Code.of("17.1");
   public static final NaceRev2Code _17_11 = NaceRev2Code.of("17.11");
   public static final NaceRev2Code _17_12 = NaceRev2Code.of("17.12");
   public static final NaceRev2Code _17_2 = NaceRev2Code.of("17.2");
   public static final NaceRev2Code _17_21 = NaceRev2Code.of("17.21");
   public static final NaceRev2Code _17_22 = NaceRev2Code.of("17.22");
   public static final NaceRev2Code _17_23 = NaceRev2Code.of("17.23");
   public static final NaceRev2Code _17_24 = NaceRev2Code.of("17.24");
   public static final NaceRev2Code _17_29 = NaceRev2Code.of("17.29");
   public static final NaceRev2Code _18 = NaceRev2Code.of("18");
   public static final NaceRev2Code _18_1 = NaceRev2Code.of("18.1");
   public static final NaceRev2Code _18_11 = NaceRev2Code.of("18.11");
   public static final NaceRev2Code _18_12 = NaceRev2Code.of("18.12");
   public static final NaceRev2Code _18_13 = NaceRev2Code.of("18.13");
   public static final NaceRev2Code _18_14 = NaceRev2Code.of("18.14");
   public static final NaceRev2Code _18_2 = NaceRev2Code.of("18.2");
   public static final NaceRev2Code _18_20 = NaceRev2Code.of("18.20");
   public static final NaceRev2Code _19 = NaceRev2Code.of("19");
   public static final NaceRev2Code _19_1 = NaceRev2Code.of("19.1");
   public static final NaceRev2Code _19_10 = NaceRev2Code.of("19.10");
   public static final NaceRev2Code _19_2 = NaceRev2Code.of("19.2");
   public static final NaceRev2Code _19_20 = NaceRev2Code.of("19.20");
   public static final NaceRev2Code _20 = NaceRev2Code.of("20");
   public static final NaceRev2Code _20_1 = NaceRev2Code.of("20.1");
   public static final NaceRev2Code _20_11 = NaceRev2Code.of("20.11");
   public static final NaceRev2Code _20_12 = NaceRev2Code.of("20.12");
   public static final NaceRev2Code _20_13 = NaceRev2Code.of("20.13");
   public static final NaceRev2Code _20_14 = NaceRev2Code.of("20.14");
   public static final NaceRev2Code _20_15 = NaceRev2Code.of("20.15");
   public static final NaceRev2Code _20_16 = NaceRev2Code.of("20.16");
   public static final NaceRev2Code _20_17 = NaceRev2Code.of("20.17");
   public static final NaceRev2Code _20_2 = NaceRev2Code.of("20.2");
   public static final NaceRev2Code _20_20 = NaceRev2Code.of("20.20");
   public static final NaceRev2Code _20_3 = NaceRev2Code.of("20.3");
   public static final NaceRev2Code _20_30 = NaceRev2Code.of("20.30");
   public static final NaceRev2Code _20_4 = NaceRev2Code.of("20.4");
   public static final NaceRev2Code _20_41 = NaceRev2Code.of("20.41");
   public static final NaceRev2Code _20_42 = NaceRev2Code.of("20.42");
   public static final NaceRev2Code _20_5 = NaceRev2Code.of("20.5");
   public static final NaceRev2Code _20_51 = NaceRev2Code.of("20.51");
   public static final NaceRev2Code _20_52 = NaceRev2Code.of("20.52");
   public static final NaceRev2Code _20_53 = NaceRev2Code.of("20.53");
   public static final NaceRev2Code _20_59 = NaceRev2Code.of("20.59");
   public static final NaceRev2Code _20_6 = NaceRev2Code.of("20.6");
   public static final NaceRev2Code _20_60 = NaceRev2Code.of("20.60");
   public static final NaceRev2Code _21 = NaceRev2Code.of("21");
   public static final NaceRev2Code _21_1 = NaceRev2Code.of("21.1");
   public static final NaceRev2Code _21_10 = NaceRev2Code.of("21.10");
   public static final NaceRev2Code _21_2 = NaceRev2Code.of("21.2");
   public static final NaceRev2Code _21_20 = NaceRev2Code.of("21.20");
   public static final NaceRev2Code _22 = NaceRev2Code.of("22");
   public static final NaceRev2Code _22_1 = NaceRev2Code.of("22.1");
   public static final NaceRev2Code _22_11 = NaceRev2Code.of("22.11");
   public static final NaceRev2Code _22_19 = NaceRev2Code.of("22.19");
   public static final NaceRev2Code _22_2 = NaceRev2Code.of("22.2");
   public static final NaceRev2Code _22_21 = NaceRev2Code.of("22.21");
   public static final NaceRev2Code _22_22 = NaceRev2Code.of("22.22");
   public static final NaceRev2Code _22_23 = NaceRev2Code.of("22.23");
   public static final NaceRev2Code _22_29 = NaceRev2Code.of("22.29");
   public static final NaceRev2Code _23 = NaceRev2Code.of("23");
   public static final NaceRev2Code _23_1 = NaceRev2Code.of("23.1");
   public static final NaceRev2Code _23_11 = NaceRev2Code.of("23.11");
   public static final NaceRev2Code _23_12 = NaceRev2Code.of("23.12");
   public static final NaceRev2Code _23_13 = NaceRev2Code.of("23.13");
   public static final NaceRev2Code _23_14 = NaceRev2Code.of("23.14");
   public static final NaceRev2Code _23_19 = NaceRev2Code.of("23.19");
   public static final NaceRev2Code _23_2 = NaceRev2Code.of("23.2");
   public static final NaceRev2Code _23_20 = NaceRev2Code.of("23.20");
   public static final NaceRev2Code _23_3 = NaceRev2Code.of("23.3");
   public static final NaceRev2Code _23_31 = NaceRev2Code.of("23.31");
   public static final NaceRev2Code _23_32 = NaceRev2Code.of("23.32");
   public static final NaceRev2Code _23_4 = NaceRev2Code.of("23.4");
   public static final NaceRev2Code _23_41 = NaceRev2Code.of("23.41");
   public static final NaceRev2Code _23_42 = NaceRev2Code.of("23.42");
   public static final NaceRev2Code _23_43 = NaceRev2Code.of("23.43");
   public static final NaceRev2Code _23_44 = NaceRev2Code.of("23.44");
   public static final NaceRev2Code _23_49 = NaceRev2Code.of("23.49");
   public static final NaceRev2Code _23_5 = NaceRev2Code.of("23.5");
   public static final NaceRev2Code _23_51 = NaceRev2Code.of("23.51");
   public static final NaceRev2Code _23_52 = NaceRev2Code.of("23.52");
   public static final NaceRev2Code _23_6 = NaceRev2Code.of("23.6");
   public static final NaceRev2Code _23_61 = NaceRev2Code.of("23.61");
   public static final NaceRev2Code _23_62 = NaceRev2Code.of("23.62");
   public static final NaceRev2Code _23_63 = NaceRev2Code.of("23.63");
   public static final NaceRev2Code _23_64 = NaceRev2Code.of("23.64");
   public static final NaceRev2Code _23_65 = NaceRev2Code.of("23.65");
   public static final NaceRev2Code _23_69 = NaceRev2Code.of("23.69");
   public static final NaceRev2Code _23_7 = NaceRev2Code.of("23.7");
   public static final NaceRev2Code _23_70 = NaceRev2Code.of("23.70");
   public static final NaceRev2Code _23_9 = NaceRev2Code.of("23.9");
   public static final NaceRev2Code _23_91 = NaceRev2Code.of("23.91");
   public static final NaceRev2Code _23_99 = NaceRev2Code.of("23.99");
   public static final NaceRev2Code _24 = NaceRev2Code.of("24");
   public static final NaceRev2Code _24_1 = NaceRev2Code.of("24.1");
   public static final NaceRev2Code _24_10 = NaceRev2Code.of("24.10");
   public static final NaceRev2Code _24_2 = NaceRev2Code.of("24.2");
   public static final NaceRev2Code _24_20 = NaceRev2Code.of("24.20");
   public static final NaceRev2Code _24_3 = NaceRev2Code.of("24.3");
   public static final NaceRev2Code _24_31 = NaceRev2Code.of("24.31");
   public static final NaceRev2Code _24_32 = NaceRev2Code.of("24.32");
   public static final NaceRev2Code _24_33 = NaceRev2Code.of("24.33");
   public static final NaceRev2Code _24_34 = NaceRev2Code.of("24.34");
   public static final NaceRev2Code _24_4 = NaceRev2Code.of("24.4");
   public static final NaceRev2Code _24_41 = NaceRev2Code.of("24.41");
   public static final NaceRev2Code _24_42 = NaceRev2Code.of("24.42");
   public static final NaceRev2Code _24_43 = NaceRev2Code.of("24.43");
   public static final NaceRev2Code _24_44 = NaceRev2Code.of("24.44");
   public static final NaceRev2Code _24_45 = NaceRev2Code.of("24.45");
   public static final NaceRev2Code _24_46 = NaceRev2Code.of("24.46");
   public static final NaceRev2Code _24_5 = NaceRev2Code.of("24.5");
   public static final NaceRev2Code _24_51 = NaceRev2Code.of("24.51");
   public static final NaceRev2Code _24_52 = NaceRev2Code.of("24.52");
   public static final NaceRev2Code _24_53 = NaceRev2Code.of("24.53");
   public static final NaceRev2Code _24_54 = NaceRev2Code.of("24.54");
   public static final NaceRev2Code _25 = NaceRev2Code.of("25");
   public static final NaceRev2Code _25_1 = NaceRev2Code.of("25.1");
   public static final NaceRev2Code _25_11 = NaceRev2Code.of("25.11");
   public static final NaceRev2Code _25_12 = NaceRev2Code.of("25.12");
   public static final NaceRev2Code _25_2 = NaceRev2Code.of("25.2");
   public static final NaceRev2Code _25_21 = NaceRev2Code.of("25.21");
   public static final NaceRev2Code _25_29 = NaceRev2Code.of("25.29");
   public static final NaceRev2Code _25_3 = NaceRev2Code.of("25.3");
   public static final NaceRev2Code _25_30 = NaceRev2Code.of("25.30");
   public static final NaceRev2Code _25_4 = NaceRev2Code.of("25.4");
   public static final NaceRev2Code _25_40 = NaceRev2Code.of("25.40");
   public static final NaceRev2Code _25_5 = NaceRev2Code.of("25.5");
   public static final NaceRev2Code _25_50 = NaceRev2Code.of("25.50");
   public static final NaceRev2Code _25_6 = NaceRev2Code.of("25.6");
   public static final NaceRev2Code _25_61 = NaceRev2Code.of("25.61");
   public static final NaceRev2Code _25_62 = NaceRev2Code.of("25.62");
   public static final NaceRev2Code _25_7 = NaceRev2Code.of("25.7");
   public static final NaceRev2Code _25_71 = NaceRev2Code.of("25.71");
   public static final NaceRev2Code _25_72 = NaceRev2Code.of("25.72");
   public static final NaceRev2Code _25_73 = NaceRev2Code.of("25.73");
   public static final NaceRev2Code _25_9 = NaceRev2Code.of("25.9");
   public static final NaceRev2Code _25_91 = NaceRev2Code.of("25.91");
   public static final NaceRev2Code _25_92 = NaceRev2Code.of("25.92");
   public static final NaceRev2Code _25_93 = NaceRev2Code.of("25.93");
   public static final NaceRev2Code _25_94 = NaceRev2Code.of("25.94");
   public static final NaceRev2Code _25_99 = NaceRev2Code.of("25.99");
   public static final NaceRev2Code _26 = NaceRev2Code.of("26");
   public static final NaceRev2Code _26_1 = NaceRev2Code.of("26.1");
   public static final NaceRev2Code _26_11 = NaceRev2Code.of("26.11");
   public static final NaceRev2Code _26_12 = NaceRev2Code.of("26.12");
   public static final NaceRev2Code _26_2 = NaceRev2Code.of("26.2");
   public static final NaceRev2Code _26_20 = NaceRev2Code.of("26.20");
   public static final NaceRev2Code _26_3 = NaceRev2Code.of("26.3");
   public static final NaceRev2Code _26_30 = NaceRev2Code.of("26.30");
   public static final NaceRev2Code _26_4 = NaceRev2Code.of("26.4");
   public static final NaceRev2Code _26_40 = NaceRev2Code.of("26.40");
   public static final NaceRev2Code _26_5 = NaceRev2Code.of("26.5");
   public static final NaceRev2Code _26_51 = NaceRev2Code.of("26.51");
   public static final NaceRev2Code _26_52 = NaceRev2Code.of("26.52");
   public static final NaceRev2Code _26_6 = NaceRev2Code.of("26.6");
   public static final NaceRev2Code _26_60 = NaceRev2Code.of("26.60");
   public static final NaceRev2Code _26_7 = NaceRev2Code.of("26.7");
   public static final NaceRev2Code _26_70 = NaceRev2Code.of("26.70");
   public static final NaceRev2Code _26_8 = NaceRev2Code.of("26.8");
   public static final NaceRev2Code _26_80 = NaceRev2Code.of("26.80");
   public static final NaceRev2Code _27 = NaceRev2Code.of("27");
   public static final NaceRev2Code _27_1 = NaceRev2Code.of("27.1");
   public static final NaceRev2Code _27_11 = NaceRev2Code.of("27.11");
   public static final NaceRev2Code _27_12 = NaceRev2Code.of("27.12");
   public static final NaceRev2Code _27_2 = NaceRev2Code.of("27.2");
   public static final NaceRev2Code _27_20 = NaceRev2Code.of("27.20");
   public static final NaceRev2Code _27_3 = NaceRev2Code.of("27.3");
   public static final NaceRev2Code _27_31 = NaceRev2Code.of("27.31");
   public static final NaceRev2Code _27_32 = NaceRev2Code.of("27.32");
   public static final NaceRev2Code _27_33 = NaceRev2Code.of("27.33");
   public static final NaceRev2Code _27_4 = NaceRev2Code.of("27.4");
   public static final NaceRev2Code _27_40 = NaceRev2Code.of("27.40");
   public static final NaceRev2Code _27_5 = NaceRev2Code.of("27.5");
   public static final NaceRev2Code _27_51 = NaceRev2Code.of("27.51");
   public static final NaceRev2Code _27_52 = NaceRev2Code.of("27.52");
   public static final NaceRev2Code _27_9 = NaceRev2Code.of("27.9");
   public static final NaceRev2Code _27_90 = NaceRev2Code.of("27.90");
   public static final NaceRev2Code _28 = NaceRev2Code.of("28");
   public static final NaceRev2Code _28_1 = NaceRev2Code.of("28.1");
   public static final NaceRev2Code _28_11 = NaceRev2Code.of("28.11");
   public static final NaceRev2Code _28_12 = NaceRev2Code.of("28.12");
   public static final NaceRev2Code _28_13 = NaceRev2Code.of("28.13");
   public static final NaceRev2Code _28_14 = NaceRev2Code.of("28.14");
   public static final NaceRev2Code _28_15 = NaceRev2Code.of("28.15");
   public static final NaceRev2Code _28_2 = NaceRev2Code.of("28.2");
   public static final NaceRev2Code _28_21 = NaceRev2Code.of("28.21");
   public static final NaceRev2Code _28_22 = NaceRev2Code.of("28.22");
   public static final NaceRev2Code _28_23 = NaceRev2Code.of("28.23");
   public static final NaceRev2Code _28_24 = NaceRev2Code.of("28.24");
   public static final NaceRev2Code _28_25 = NaceRev2Code.of("28.25");
   public static final NaceRev2Code _28_29 = NaceRev2Code.of("28.29");
   public static final NaceRev2Code _28_3 = NaceRev2Code.of("28.3");
   public static final NaceRev2Code _28_30 = NaceRev2Code.of("28.30");
   public static final NaceRev2Code _28_4 = NaceRev2Code.of("28.4");
   public static final NaceRev2Code _28_41 = NaceRev2Code.of("28.41");
   public static final NaceRev2Code _28_49 = NaceRev2Code.of("28.49");
   public static final NaceRev2Code _28_9 = NaceRev2Code.of("28.9");
   public static final NaceRev2Code _28_91 = NaceRev2Code.of("28.91");
   public static final NaceRev2Code _28_92 = NaceRev2Code.of("28.92");
   public static final NaceRev2Code _28_93 = NaceRev2Code.of("28.93");
   public static final NaceRev2Code _28_94 = NaceRev2Code.of("28.94");
   public static final NaceRev2Code _28_95 = NaceRev2Code.of("28.95");
   public static final NaceRev2Code _28_96 = NaceRev2Code.of("28.96");
   public static final NaceRev2Code _28_99 = NaceRev2Code.of("28.99");
   public static final NaceRev2Code _29 = NaceRev2Code.of("29");
   public static final NaceRev2Code _29_1 = NaceRev2Code.of("29.1");
   public static final NaceRev2Code _29_10 = NaceRev2Code.of("29.10");
   public static final NaceRev2Code _29_2 = NaceRev2Code.of("29.2");
   public static final NaceRev2Code _29_20 = NaceRev2Code.of("29.20");
   public static final NaceRev2Code _29_3 = NaceRev2Code.of("29.3");
   public static final NaceRev2Code _29_31 = NaceRev2Code.of("29.31");
   public static final NaceRev2Code _29_32 = NaceRev2Code.of("29.32");
   public static final NaceRev2Code _30 = NaceRev2Code.of("30");
   public static final NaceRev2Code _30_1 = NaceRev2Code.of("30.1");
   public static final NaceRev2Code _30_11 = NaceRev2Code.of("30.11");
   public static final NaceRev2Code _30_12 = NaceRev2Code.of("30.12");
   public static final NaceRev2Code _30_2 = NaceRev2Code.of("30.2");
   public static final NaceRev2Code _30_20 = NaceRev2Code.of("30.20");
   public static final NaceRev2Code _30_3 = NaceRev2Code.of("30.3");
   public static final NaceRev2Code _30_30 = NaceRev2Code.of("30.30");
   public static final NaceRev2Code _30_4 = NaceRev2Code.of("30.4");
   public static final NaceRev2Code _30_40 = NaceRev2Code.of("30.40");
   public static final NaceRev2Code _30_9 = NaceRev2Code.of("30.9");
   public static final NaceRev2Code _30_91 = NaceRev2Code.of("30.91");
   public static final NaceRev2Code _30_92 = NaceRev2Code.of("30.92");
   public static final NaceRev2Code _30_99 = NaceRev2Code.of("30.99");
   public static final NaceRev2Code _31 = NaceRev2Code.of("31");
   public static final NaceRev2Code _31_0 = NaceRev2Code.of("31.0");
   public static final NaceRev2Code _31_01 = NaceRev2Code.of("31.01");
   public static final NaceRev2Code _31_02 = NaceRev2Code.of("31.02");
   public static final NaceRev2Code _31_03 = NaceRev2Code.of("31.03");
   public static final NaceRev2Code _31_09 = NaceRev2Code.of("31.09");
   public static final NaceRev2Code _32 = NaceRev2Code.of("32");
   public static final NaceRev2Code _32_1 = NaceRev2Code.of("32.1");
   public static final NaceRev2Code _32_11 = NaceRev2Code.of("32.11");
   public static final NaceRev2Code _32_12 = NaceRev2Code.of("32.12");
   public static final NaceRev2Code _32_13 = NaceRev2Code.of("32.13");
   public static final NaceRev2Code _32_2 = NaceRev2Code.of("32.2");
   public static final NaceRev2Code _32_20 = NaceRev2Code.of("32.20");
   public static final NaceRev2Code _32_3 = NaceRev2Code.of("32.3");
   public static final NaceRev2Code _32_30 = NaceRev2Code.of("32.30");
   public static final NaceRev2Code _32_4 = NaceRev2Code.of("32.4");
   public static final NaceRev2Code _32_40 = NaceRev2Code.of("32.40");
   public static final NaceRev2Code _32_5 = NaceRev2Code.of("32.5");
   public static final NaceRev2Code _32_50 = NaceRev2Code.of("32.50");
   public static final NaceRev2Code _32_9 = NaceRev2Code.of("32.9");
   public static final NaceRev2Code _32_91 = NaceRev2Code.of("32.91");
   public static final NaceRev2Code _32_99 = NaceRev2Code.of("32.99");
   public static final NaceRev2Code _33 = NaceRev2Code.of("33");
   public static final NaceRev2Code _33_1 = NaceRev2Code.of("33.1");
   public static final NaceRev2Code _33_11 = NaceRev2Code.of("33.11");
   public static final NaceRev2Code _33_12 = NaceRev2Code.of("33.12");
   public static final NaceRev2Code _33_13 = NaceRev2Code.of("33.13");
   public static final NaceRev2Code _33_14 = NaceRev2Code.of("33.14");
   public static final NaceRev2Code _33_15 = NaceRev2Code.of("33.15");
   public static final NaceRev2Code _33_16 = NaceRev2Code.of("33.16");
   public static final NaceRev2Code _33_17 = NaceRev2Code.of("33.17");
   public static final NaceRev2Code _33_19 = NaceRev2Code.of("33.19");
   public static final NaceRev2Code _33_2 = NaceRev2Code.of("33.2");
   public static final NaceRev2Code _33_20 = NaceRev2Code.of("33.20");
   public static final NaceRev2Code _35 = NaceRev2Code.of("35");
   public static final NaceRev2Code _35_1 = NaceRev2Code.of("35.1");
   public static final NaceRev2Code _35_11 = NaceRev2Code.of("35.11");
   public static final NaceRev2Code _35_12 = NaceRev2Code.of("35.12");
   public static final NaceRev2Code _35_13 = NaceRev2Code.of("35.13");
   public static final NaceRev2Code _35_14 = NaceRev2Code.of("35.14");
   public static final NaceRev2Code _35_2 = NaceRev2Code.of("35.2");
   public static final NaceRev2Code _35_21 = NaceRev2Code.of("35.21");
   public static final NaceRev2Code _35_22 = NaceRev2Code.of("35.22");
   public static final NaceRev2Code _35_23 = NaceRev2Code.of("35.23");
   public static final NaceRev2Code _35_3 = NaceRev2Code.of("35.3");
   public static final NaceRev2Code _35_30 = NaceRev2Code.of("35.30");
   public static final NaceRev2Code _36 = NaceRev2Code.of("36");
   public static final NaceRev2Code _36_0 = NaceRev2Code.of("36.0");
   public static final NaceRev2Code _36_00 = NaceRev2Code.of("36.00");
   public static final NaceRev2Code _37 = NaceRev2Code.of("37");
   public static final NaceRev2Code _37_0 = NaceRev2Code.of("37.0");
   public static final NaceRev2Code _37_00 = NaceRev2Code.of("37.00");
   public static final NaceRev2Code _38 = NaceRev2Code.of("38");
   public static final NaceRev2Code _38_1 = NaceRev2Code.of("38.1");
   public static final NaceRev2Code _38_11 = NaceRev2Code.of("38.11");
   public static final NaceRev2Code _38_12 = NaceRev2Code.of("38.12");
   public static final NaceRev2Code _38_2 = NaceRev2Code.of("38.2");
   public static final NaceRev2Code _38_21 = NaceRev2Code.of("38.21");
   public static final NaceRev2Code _38_22 = NaceRev2Code.of("38.22");
   public static final NaceRev2Code _38_3 = NaceRev2Code.of("38.3");
   public static final NaceRev2Code _38_31 = NaceRev2Code.of("38.31");
   public static final NaceRev2Code _38_32 = NaceRev2Code.of("38.32");
   public static final NaceRev2Code _39 = NaceRev2Code.of("39");
   public static final NaceRev2Code _39_0 = NaceRev2Code.of("39.0");
   public static final NaceRev2Code _39_00 = NaceRev2Code.of("39.00");
   public static final NaceRev2Code _41 = NaceRev2Code.of("41");
   public static final NaceRev2Code _41_1 = NaceRev2Code.of("41.1");
   public static final NaceRev2Code _41_10 = NaceRev2Code.of("41.10");
   public static final NaceRev2Code _41_2 = NaceRev2Code.of("41.2");
   public static final NaceRev2Code _41_20 = NaceRev2Code.of("41.20");
   public static final NaceRev2Code _42 = NaceRev2Code.of("42");
   public static final NaceRev2Code _42_1 = NaceRev2Code.of("42.1");
   public static final NaceRev2Code _42_11 = NaceRev2Code.of("42.11");
   public static final NaceRev2Code _42_12 = NaceRev2Code.of("42.12");
   public static final NaceRev2Code _42_13 = NaceRev2Code.of("42.13");
   public static final NaceRev2Code _42_2 = NaceRev2Code.of("42.2");
   public static final NaceRev2Code _42_21 = NaceRev2Code.of("42.21");
   public static final NaceRev2Code _42_22 = NaceRev2Code.of("42.22");
   public static final NaceRev2Code _42_9 = NaceRev2Code.of("42.9");
   public static final NaceRev2Code _42_91 = NaceRev2Code.of("42.91");
   public static final NaceRev2Code _42_99 = NaceRev2Code.of("42.99");
   public static final NaceRev2Code _43 = NaceRev2Code.of("43");
   public static final NaceRev2Code _43_1 = NaceRev2Code.of("43.1");
   public static final NaceRev2Code _43_11 = NaceRev2Code.of("43.11");
   public static final NaceRev2Code _43_12 = NaceRev2Code.of("43.12");
   public static final NaceRev2Code _43_13 = NaceRev2Code.of("43.13");
   public static final NaceRev2Code _43_2 = NaceRev2Code.of("43.2");
   public static final NaceRev2Code _43_21 = NaceRev2Code.of("43.21");
   public static final NaceRev2Code _43_22 = NaceRev2Code.of("43.22");
   public static final NaceRev2Code _43_29 = NaceRev2Code.of("43.29");
   public static final NaceRev2Code _43_3 = NaceRev2Code.of("43.3");
   public static final NaceRev2Code _43_31 = NaceRev2Code.of("43.31");
   public static final NaceRev2Code _43_32 = NaceRev2Code.of("43.32");
   public static final NaceRev2Code _43_33 = NaceRev2Code.of("43.33");
   public static final NaceRev2Code _43_34 = NaceRev2Code.of("43.34");
   public static final NaceRev2Code _43_39 = NaceRev2Code.of("43.39");
   public static final NaceRev2Code _43_9 = NaceRev2Code.of("43.9");
   public static final NaceRev2Code _43_91 = NaceRev2Code.of("43.91");
   public static final NaceRev2Code _43_99 = NaceRev2Code.of("43.99");
   public static final NaceRev2Code _45 = NaceRev2Code.of("45");
   public static final NaceRev2Code _45_1 = NaceRev2Code.of("45.1");
   public static final NaceRev2Code _45_11 = NaceRev2Code.of("45.11");
   public static final NaceRev2Code _45_19 = NaceRev2Code.of("45.19");
   public static final NaceRev2Code _45_2 = NaceRev2Code.of("45.2");
   public static final NaceRev2Code _45_20 = NaceRev2Code.of("45.20");
   public static final NaceRev2Code _45_3 = NaceRev2Code.of("45.3");
   public static final NaceRev2Code _45_31 = NaceRev2Code.of("45.31");
   public static final NaceRev2Code _45_32 = NaceRev2Code.of("45.32");
   public static final NaceRev2Code _45_4 = NaceRev2Code.of("45.4");
   public static final NaceRev2Code _45_40 = NaceRev2Code.of("45.40");
   public static final NaceRev2Code _46 = NaceRev2Code.of("46");
   public static final NaceRev2Code _46_1 = NaceRev2Code.of("46.1");
   public static final NaceRev2Code _46_11 = NaceRev2Code.of("46.11");
   public static final NaceRev2Code _46_12 = NaceRev2Code.of("46.12");
   public static final NaceRev2Code _46_13 = NaceRev2Code.of("46.13");
   public static final NaceRev2Code _46_14 = NaceRev2Code.of("46.14");
   public static final NaceRev2Code _46_15 = NaceRev2Code.of("46.15");
   public static final NaceRev2Code _46_16 = NaceRev2Code.of("46.16");
   public static final NaceRev2Code _46_17 = NaceRev2Code.of("46.17");
   public static final NaceRev2Code _46_18 = NaceRev2Code.of("46.18");
   public static final NaceRev2Code _46_19 = NaceRev2Code.of("46.19");
   public static final NaceRev2Code _46_2 = NaceRev2Code.of("46.2");
   public static final NaceRev2Code _46_21 = NaceRev2Code.of("46.21");
   public static final NaceRev2Code _46_22 = NaceRev2Code.of("46.22");
   public static final NaceRev2Code _46_23 = NaceRev2Code.of("46.23");
   public static final NaceRev2Code _46_24 = NaceRev2Code.of("46.24");
   public static final NaceRev2Code _46_3 = NaceRev2Code.of("46.3");
   public static final NaceRev2Code _46_31 = NaceRev2Code.of("46.31");
   public static final NaceRev2Code _46_32 = NaceRev2Code.of("46.32");
   public static final NaceRev2Code _46_33 = NaceRev2Code.of("46.33");
   public static final NaceRev2Code _46_34 = NaceRev2Code.of("46.34");
   public static final NaceRev2Code _46_35 = NaceRev2Code.of("46.35");
   public static final NaceRev2Code _46_36 = NaceRev2Code.of("46.36");
   public static final NaceRev2Code _46_37 = NaceRev2Code.of("46.37");
   public static final NaceRev2Code _46_38 = NaceRev2Code.of("46.38");
   public static final NaceRev2Code _46_39 = NaceRev2Code.of("46.39");
   public static final NaceRev2Code _46_4 = NaceRev2Code.of("46.4");
   public static final NaceRev2Code _46_41 = NaceRev2Code.of("46.41");
   public static final NaceRev2Code _46_42 = NaceRev2Code.of("46.42");
   public static final NaceRev2Code _46_43 = NaceRev2Code.of("46.43");
   public static final NaceRev2Code _46_44 = NaceRev2Code.of("46.44");
   public static final NaceRev2Code _46_45 = NaceRev2Code.of("46.45");
   public static final NaceRev2Code _46_46 = NaceRev2Code.of("46.46");
   public static final NaceRev2Code _46_47 = NaceRev2Code.of("46.47");
   public static final NaceRev2Code _46_48 = NaceRev2Code.of("46.48");
   public static final NaceRev2Code _46_49 = NaceRev2Code.of("46.49");
   public static final NaceRev2Code _46_5 = NaceRev2Code.of("46.5");
   public static final NaceRev2Code _46_51 = NaceRev2Code.of("46.51");
   public static final NaceRev2Code _46_52 = NaceRev2Code.of("46.52");
   public static final NaceRev2Code _46_6 = NaceRev2Code.of("46.6");
   public static final NaceRev2Code _46_61 = NaceRev2Code.of("46.61");
   public static final NaceRev2Code _46_62 = NaceRev2Code.of("46.62");
   public static final NaceRev2Code _46_63 = NaceRev2Code.of("46.63");
   public static final NaceRev2Code _46_64 = NaceRev2Code.of("46.64");
   public static final NaceRev2Code _46_65 = NaceRev2Code.of("46.65");
   public static final NaceRev2Code _46_66 = NaceRev2Code.of("46.66");
   public static final NaceRev2Code _46_69 = NaceRev2Code.of("46.69");
   public static final NaceRev2Code _46_7 = NaceRev2Code.of("46.7");
   public static final NaceRev2Code _46_71 = NaceRev2Code.of("46.71");
   public static final NaceRev2Code _46_72 = NaceRev2Code.of("46.72");
   public static final NaceRev2Code _46_73 = NaceRev2Code.of("46.73");
   public static final NaceRev2Code _46_74 = NaceRev2Code.of("46.74");
   public static final NaceRev2Code _46_75 = NaceRev2Code.of("46.75");
   public static final NaceRev2Code _46_76 = NaceRev2Code.of("46.76");
   public static final NaceRev2Code _46_77 = NaceRev2Code.of("46.77");
   public static final NaceRev2Code _46_9 = NaceRev2Code.of("46.9");
   public static final NaceRev2Code _46_90 = NaceRev2Code.of("46.90");
   public static final NaceRev2Code _47 = NaceRev2Code.of("47");
   public static final NaceRev2Code _47_1 = NaceRev2Code.of("47.1");
   public static final NaceRev2Code _47_11 = NaceRev2Code.of("47.11");
   public static final NaceRev2Code _47_19 = NaceRev2Code.of("47.19");
   public static final NaceRev2Code _47_2 = NaceRev2Code.of("47.2");
   public static final NaceRev2Code _47_21 = NaceRev2Code.of("47.21");
   public static final NaceRev2Code _47_22 = NaceRev2Code.of("47.22");
   public static final NaceRev2Code _47_23 = NaceRev2Code.of("47.23");
   public static final NaceRev2Code _47_24 = NaceRev2Code.of("47.24");
   public static final NaceRev2Code _47_25 = NaceRev2Code.of("47.25");
   public static final NaceRev2Code _47_26 = NaceRev2Code.of("47.26");
   public static final NaceRev2Code _47_29 = NaceRev2Code.of("47.29");
   public static final NaceRev2Code _47_3 = NaceRev2Code.of("47.3");
   public static final NaceRev2Code _47_30 = NaceRev2Code.of("47.30");
   public static final NaceRev2Code _47_4 = NaceRev2Code.of("47.4");
   public static final NaceRev2Code _47_41 = NaceRev2Code.of("47.41");
   public static final NaceRev2Code _47_42 = NaceRev2Code.of("47.42");
   public static final NaceRev2Code _47_43 = NaceRev2Code.of("47.43");
   public static final NaceRev2Code _47_5 = NaceRev2Code.of("47.5");
   public static final NaceRev2Code _47_51 = NaceRev2Code.of("47.51");
   public static final NaceRev2Code _47_52 = NaceRev2Code.of("47.52");
   public static final NaceRev2Code _47_53 = NaceRev2Code.of("47.53");
   public static final NaceRev2Code _47_54 = NaceRev2Code.of("47.54");
   public static final NaceRev2Code _47_59 = NaceRev2Code.of("47.59");
   public static final NaceRev2Code _47_6 = NaceRev2Code.of("47.6");
   public static final NaceRev2Code _47_61 = NaceRev2Code.of("47.61");
   public static final NaceRev2Code _47_62 = NaceRev2Code.of("47.62");
   public static final NaceRev2Code _47_63 = NaceRev2Code.of("47.63");
   public static final NaceRev2Code _47_64 = NaceRev2Code.of("47.64");
   public static final NaceRev2Code _47_65 = NaceRev2Code.of("47.65");
   public static final NaceRev2Code _47_7 = NaceRev2Code.of("47.7");
   public static final NaceRev2Code _47_71 = NaceRev2Code.of("47.71");
   public static final NaceRev2Code _47_72 = NaceRev2Code.of("47.72");
   public static final NaceRev2Code _47_73 = NaceRev2Code.of("47.73");
   public static final NaceRev2Code _47_74 = NaceRev2Code.of("47.74");
   public static final NaceRev2Code _47_75 = NaceRev2Code.of("47.75");
   public static final NaceRev2Code _47_76 = NaceRev2Code.of("47.76");
   public static final NaceRev2Code _47_77 = NaceRev2Code.of("47.77");
   public static final NaceRev2Code _47_78 = NaceRev2Code.of("47.78");
   public static final NaceRev2Code _47_79 = NaceRev2Code.of("47.79");
   public static final NaceRev2Code _47_8 = NaceRev2Code.of("47.8");
   public static final NaceRev2Code _47_81 = NaceRev2Code.of("47.81");
   public static final NaceRev2Code _47_82 = NaceRev2Code.of("47.82");
   public static final NaceRev2Code _47_89 = NaceRev2Code.of("47.89");
   public static final NaceRev2Code _47_9 = NaceRev2Code.of("47.9");
   public static final NaceRev2Code _47_91 = NaceRev2Code.of("47.91");
   public static final NaceRev2Code _47_99 = NaceRev2Code.of("47.99");
   public static final NaceRev2Code _49 = NaceRev2Code.of("49");
   public static final NaceRev2Code _49_1 = NaceRev2Code.of("49.1");
   public static final NaceRev2Code _49_10 = NaceRev2Code.of("49.10");
   public static final NaceRev2Code _49_2 = NaceRev2Code.of("49.2");
   public static final NaceRev2Code _49_20 = NaceRev2Code.of("49.20");
   public static final NaceRev2Code _49_3 = NaceRev2Code.of("49.3");
   public static final NaceRev2Code _49_31 = NaceRev2Code.of("49.31");
   public static final NaceRev2Code _49_32 = NaceRev2Code.of("49.32");
   public static final NaceRev2Code _49_39 = NaceRev2Code.of("49.39");
   public static final NaceRev2Code _49_4 = NaceRev2Code.of("49.4");
   public static final NaceRev2Code _49_41 = NaceRev2Code.of("49.41");
   public static final NaceRev2Code _49_42 = NaceRev2Code.of("49.42");
   public static final NaceRev2Code _49_5 = NaceRev2Code.of("49.5");
   public static final NaceRev2Code _49_50 = NaceRev2Code.of("49.50");
   public static final NaceRev2Code _50 = NaceRev2Code.of("50");
   public static final NaceRev2Code _50_1 = NaceRev2Code.of("50.1");
   public static final NaceRev2Code _50_10 = NaceRev2Code.of("50.10");
   public static final NaceRev2Code _50_2 = NaceRev2Code.of("50.2");
   public static final NaceRev2Code _50_20 = NaceRev2Code.of("50.20");
   public static final NaceRev2Code _50_3 = NaceRev2Code.of("50.3");
   public static final NaceRev2Code _50_30 = NaceRev2Code.of("50.30");
   public static final NaceRev2Code _50_4 = NaceRev2Code.of("50.4");
   public static final NaceRev2Code _50_40 = NaceRev2Code.of("50.40");
   public static final NaceRev2Code _51 = NaceRev2Code.of("51");
   public static final NaceRev2Code _51_1 = NaceRev2Code.of("51.1");
   public static final NaceRev2Code _51_10 = NaceRev2Code.of("51.10");
   public static final NaceRev2Code _51_2 = NaceRev2Code.of("51.2");
   public static final NaceRev2Code _51_21 = NaceRev2Code.of("51.21");
   public static final NaceRev2Code _51_22 = NaceRev2Code.of("51.22");
   public static final NaceRev2Code _52 = NaceRev2Code.of("52");
   public static final NaceRev2Code _52_1 = NaceRev2Code.of("52.1");
   public static final NaceRev2Code _52_10 = NaceRev2Code.of("52.10");
   public static final NaceRev2Code _52_2 = NaceRev2Code.of("52.2");
   public static final NaceRev2Code _52_21 = NaceRev2Code.of("52.21");
   public static final NaceRev2Code _52_22 = NaceRev2Code.of("52.22");
   public static final NaceRev2Code _52_23 = NaceRev2Code.of("52.23");
   public static final NaceRev2Code _52_24 = NaceRev2Code.of("52.24");
   public static final NaceRev2Code _52_29 = NaceRev2Code.of("52.29");
   public static final NaceRev2Code _53 = NaceRev2Code.of("53");
   public static final NaceRev2Code _53_1 = NaceRev2Code.of("53.1");
   public static final NaceRev2Code _53_10 = NaceRev2Code.of("53.10");
   public static final NaceRev2Code _53_2 = NaceRev2Code.of("53.2");
   public static final NaceRev2Code _53_20 = NaceRev2Code.of("53.20");
   public static final NaceRev2Code _55 = NaceRev2Code.of("55");
   public static final NaceRev2Code _55_1 = NaceRev2Code.of("55.1");
   public static final NaceRev2Code _55_10 = NaceRev2Code.of("55.10");
   public static final NaceRev2Code _55_2 = NaceRev2Code.of("55.2");
   public static final NaceRev2Code _55_20 = NaceRev2Code.of("55.20");
   public static final NaceRev2Code _55_3 = NaceRev2Code.of("55.3");
   public static final NaceRev2Code _55_30 = NaceRev2Code.of("55.30");
   public static final NaceRev2Code _55_9 = NaceRev2Code.of("55.9");
   public static final NaceRev2Code _55_90 = NaceRev2Code.of("55.90");
   public static final NaceRev2Code _56 = NaceRev2Code.of("56");
   public static final NaceRev2Code _56_1 = NaceRev2Code.of("56.1");
   public static final NaceRev2Code _56_10 = NaceRev2Code.of("56.10");
   public static final NaceRev2Code _56_2 = NaceRev2Code.of("56.2");
   public static final NaceRev2Code _56_21 = NaceRev2Code.of("56.21");
   public static final NaceRev2Code _56_29 = NaceRev2Code.of("56.29");
   public static final NaceRev2Code _56_3 = NaceRev2Code.of("56.3");
   public static final NaceRev2Code _56_30 = NaceRev2Code.of("56.30");
   public static final NaceRev2Code _58 = NaceRev2Code.of("58");
   public static final NaceRev2Code _58_1 = NaceRev2Code.of("58.1");
   public static final NaceRev2Code _58_11 = NaceRev2Code.of("58.11");
   public static final NaceRev2Code _58_12 = NaceRev2Code.of("58.12");
   public static final NaceRev2Code _58_13 = NaceRev2Code.of("58.13");
   public static final NaceRev2Code _58_14 = NaceRev2Code.of("58.14");
   public static final NaceRev2Code _58_19 = NaceRev2Code.of("58.19");
   public static final NaceRev2Code _58_2 = NaceRev2Code.of("58.2");
   public static final NaceRev2Code _58_21 = NaceRev2Code.of("58.21");
   public static final NaceRev2Code _58_29 = NaceRev2Code.of("58.29");
   public static final NaceRev2Code _59 = NaceRev2Code.of("59");
   public static final NaceRev2Code _59_1 = NaceRev2Code.of("59.1");
   public static final NaceRev2Code _59_11 = NaceRev2Code.of("59.11");
   public static final NaceRev2Code _59_12 = NaceRev2Code.of("59.12");
   public static final NaceRev2Code _59_13 = NaceRev2Code.of("59.13");
   public static final NaceRev2Code _59_14 = NaceRev2Code.of("59.14");
   public static final NaceRev2Code _59_2 = NaceRev2Code.of("59.2");
   public static final NaceRev2Code _59_20 = NaceRev2Code.of("59.20");
   public static final NaceRev2Code _60 = NaceRev2Code.of("60");
   public static final NaceRev2Code _60_1 = NaceRev2Code.of("60.1");
   public static final NaceRev2Code _60_10 = NaceRev2Code.of("60.10");
   public static final NaceRev2Code _60_2 = NaceRev2Code.of("60.2");
   public static final NaceRev2Code _60_20 = NaceRev2Code.of("60.20");
   public static final NaceRev2Code _61 = NaceRev2Code.of("61");
   public static final NaceRev2Code _61_1 = NaceRev2Code.of("61.1");
   public static final NaceRev2Code _61_10 = NaceRev2Code.of("61.10");
   public static final NaceRev2Code _61_2 = NaceRev2Code.of("61.2");
   public static final NaceRev2Code _61_20 = NaceRev2Code.of("61.20");
   public static final NaceRev2Code _61_3 = NaceRev2Code.of("61.3");
   public static final NaceRev2Code _61_30 = NaceRev2Code.of("61.30");
   public static final NaceRev2Code _61_9 = NaceRev2Code.of("61.9");
   public static final NaceRev2Code _61_90 = NaceRev2Code.of("61.90");
   public static final NaceRev2Code _62 = NaceRev2Code.of("62");
   public static final NaceRev2Code _62_0 = NaceRev2Code.of("62.0");
   public static final NaceRev2Code _62_01 = NaceRev2Code.of("62.01");
   public static final NaceRev2Code _62_02 = NaceRev2Code.of("62.02");
   public static final NaceRev2Code _62_03 = NaceRev2Code.of("62.03");
   public static final NaceRev2Code _62_09 = NaceRev2Code.of("62.09");
   public static final NaceRev2Code _63 = NaceRev2Code.of("63");
   public static final NaceRev2Code _63_1 = NaceRev2Code.of("63.1");
   public static final NaceRev2Code _63_11 = NaceRev2Code.of("63.11");
   public static final NaceRev2Code _63_12 = NaceRev2Code.of("63.12");
   public static final NaceRev2Code _63_9 = NaceRev2Code.of("63.9");
   public static final NaceRev2Code _63_91 = NaceRev2Code.of("63.91");
   public static final NaceRev2Code _63_99 = NaceRev2Code.of("63.99");
   public static final NaceRev2Code _64 = NaceRev2Code.of("64");
   public static final NaceRev2Code _64_1 = NaceRev2Code.of("64.1");
   public static final NaceRev2Code _64_11 = NaceRev2Code.of("64.11");
   public static final NaceRev2Code _64_19 = NaceRev2Code.of("64.19");
   public static final NaceRev2Code _64_2 = NaceRev2Code.of("64.2");
   public static final NaceRev2Code _64_20 = NaceRev2Code.of("64.20");
   public static final NaceRev2Code _64_3 = NaceRev2Code.of("64.3");
   public static final NaceRev2Code _64_30 = NaceRev2Code.of("64.30");
   public static final NaceRev2Code _64_9 = NaceRev2Code.of("64.9");
   public static final NaceRev2Code _64_91 = NaceRev2Code.of("64.91");
   public static final NaceRev2Code _64_92 = NaceRev2Code.of("64.92");
   public static final NaceRev2Code _64_99 = NaceRev2Code.of("64.99");
   public static final NaceRev2Code _65 = NaceRev2Code.of("65");
   public static final NaceRev2Code _65_1 = NaceRev2Code.of("65.1");
   public static final NaceRev2Code _65_11 = NaceRev2Code.of("65.11");
   public static final NaceRev2Code _65_12 = NaceRev2Code.of("65.12");
   public static final NaceRev2Code _65_2 = NaceRev2Code.of("65.2");
   public static final NaceRev2Code _65_20 = NaceRev2Code.of("65.20");
   public static final NaceRev2Code _65_3 = NaceRev2Code.of("65.3");
   public static final NaceRev2Code _65_30 = NaceRev2Code.of("65.30");
   public static final NaceRev2Code _66 = NaceRev2Code.of("66");
   public static final NaceRev2Code _66_1 = NaceRev2Code.of("66.1");
   public static final NaceRev2Code _66_11 = NaceRev2Code.of("66.11");
   public static final NaceRev2Code _66_12 = NaceRev2Code.of("66.12");
   public static final NaceRev2Code _66_19 = NaceRev2Code.of("66.19");
   public static final NaceRev2Code _66_2 = NaceRev2Code.of("66.2");
   public static final NaceRev2Code _66_21 = NaceRev2Code.of("66.21");
   public static final NaceRev2Code _66_22 = NaceRev2Code.of("66.22");
   public static final NaceRev2Code _66_29 = NaceRev2Code.of("66.29");
   public static final NaceRev2Code _66_3 = NaceRev2Code.of("66.3");
   public static final NaceRev2Code _66_30 = NaceRev2Code.of("66.30");
   public static final NaceRev2Code _68 = NaceRev2Code.of("68");
   public static final NaceRev2Code _68_1 = NaceRev2Code.of("68.1");
   public static final NaceRev2Code _68_10 = NaceRev2Code.of("68.10");
   public static final NaceRev2Code _68_2 = NaceRev2Code.of("68.2");
   public static final NaceRev2Code _68_20 = NaceRev2Code.of("68.20");
   public static final NaceRev2Code _68_3 = NaceRev2Code.of("68.3");
   public static final NaceRev2Code _68_31 = NaceRev2Code.of("68.31");
   public static final NaceRev2Code _68_32 = NaceRev2Code.of("68.32");
   public static final NaceRev2Code _69 = NaceRev2Code.of("69");
   public static final NaceRev2Code _69_1 = NaceRev2Code.of("69.1");
   public static final NaceRev2Code _69_10 = NaceRev2Code.of("69.10");
   public static final NaceRev2Code _69_2 = NaceRev2Code.of("69.2");
   public static final NaceRev2Code _69_20 = NaceRev2Code.of("69.20");
   public static final NaceRev2Code _70 = NaceRev2Code.of("70");
   public static final NaceRev2Code _70_1 = NaceRev2Code.of("70.1");
   public static final NaceRev2Code _70_10 = NaceRev2Code.of("70.10");
   public static final NaceRev2Code _70_2 = NaceRev2Code.of("70.2");
   public static final NaceRev2Code _70_21 = NaceRev2Code.of("70.21");
   public static final NaceRev2Code _70_22 = NaceRev2Code.of("70.22");
   public static final NaceRev2Code _71 = NaceRev2Code.of("71");
   public static final NaceRev2Code _71_1 = NaceRev2Code.of("71.1");
   public static final NaceRev2Code _71_11 = NaceRev2Code.of("71.11");
   public static final NaceRev2Code _71_12 = NaceRev2Code.of("71.12");
   public static final NaceRev2Code _71_2 = NaceRev2Code.of("71.2");
   public static final NaceRev2Code _71_20 = NaceRev2Code.of("71.20");
   public static final NaceRev2Code _72 = NaceRev2Code.of("72");
   public static final NaceRev2Code _72_1 = NaceRev2Code.of("72.1");
   public static final NaceRev2Code _72_11 = NaceRev2Code.of("72.11");
   public static final NaceRev2Code _72_19 = NaceRev2Code.of("72.19");
   public static final NaceRev2Code _72_2 = NaceRev2Code.of("72.2");
   public static final NaceRev2Code _72_20 = NaceRev2Code.of("72.20");
   public static final NaceRev2Code _73 = NaceRev2Code.of("73");
   public static final NaceRev2Code _73_1 = NaceRev2Code.of("73.1");
   public static final NaceRev2Code _73_11 = NaceRev2Code.of("73.11");
   public static final NaceRev2Code _73_12 = NaceRev2Code.of("73.12");
   public static final NaceRev2Code _73_2 = NaceRev2Code.of("73.2");
   public static final NaceRev2Code _73_20 = NaceRev2Code.of("73.20");
   public static final NaceRev2Code _74 = NaceRev2Code.of("74");
   public static final NaceRev2Code _74_1 = NaceRev2Code.of("74.1");
   public static final NaceRev2Code _74_10 = NaceRev2Code.of("74.10");
   public static final NaceRev2Code _74_2 = NaceRev2Code.of("74.2");
   public static final NaceRev2Code _74_20 = NaceRev2Code.of("74.20");
   public static final NaceRev2Code _74_3 = NaceRev2Code.of("74.3");
   public static final NaceRev2Code _74_30 = NaceRev2Code.of("74.30");
   public static final NaceRev2Code _74_9 = NaceRev2Code.of("74.9");
   public static final NaceRev2Code _74_90 = NaceRev2Code.of("74.90");
   public static final NaceRev2Code _75 = NaceRev2Code.of("75");
   public static final NaceRev2Code _75_0 = NaceRev2Code.of("75.0");
   public static final NaceRev2Code _75_00 = NaceRev2Code.of("75.00");
   public static final NaceRev2Code _77 = NaceRev2Code.of("77");
   public static final NaceRev2Code _77_1 = NaceRev2Code.of("77.1");
   public static final NaceRev2Code _77_11 = NaceRev2Code.of("77.11");
   public static final NaceRev2Code _77_12 = NaceRev2Code.of("77.12");
   public static final NaceRev2Code _77_2 = NaceRev2Code.of("77.2");
   public static final NaceRev2Code _77_21 = NaceRev2Code.of("77.21");
   public static final NaceRev2Code _77_22 = NaceRev2Code.of("77.22");
   public static final NaceRev2Code _77_29 = NaceRev2Code.of("77.29");
   public static final NaceRev2Code _77_3 = NaceRev2Code.of("77.3");
   public static final NaceRev2Code _77_31 = NaceRev2Code.of("77.31");
   public static final NaceRev2Code _77_32 = NaceRev2Code.of("77.32");
   public static final NaceRev2Code _77_33 = NaceRev2Code.of("77.33");
   public static final NaceRev2Code _77_34 = NaceRev2Code.of("77.34");
   public static final NaceRev2Code _77_35 = NaceRev2Code.of("77.35");
   public static final NaceRev2Code _77_39 = NaceRev2Code.of("77.39");
   public static final NaceRev2Code _77_4 = NaceRev2Code.of("77.4");
   public static final NaceRev2Code _77_40 = NaceRev2Code.of("77.40");
   public static final NaceRev2Code _78 = NaceRev2Code.of("78");
   public static final NaceRev2Code _78_1 = NaceRev2Code.of("78.1");
   public static final NaceRev2Code _78_10 = NaceRev2Code.of("78.10");
   public static final NaceRev2Code _78_2 = NaceRev2Code.of("78.2");
   public static final NaceRev2Code _78_20 = NaceRev2Code.of("78.20");
   public static final NaceRev2Code _78_3 = NaceRev2Code.of("78.3");
   public static final NaceRev2Code _78_30 = NaceRev2Code.of("78.30");
   public static final NaceRev2Code _79 = NaceRev2Code.of("79");
   public static final NaceRev2Code _79_1 = NaceRev2Code.of("79.1");
   public static final NaceRev2Code _79_11 = NaceRev2Code.of("79.11");
   public static final NaceRev2Code _79_12 = NaceRev2Code.of("79.12");
   public static final NaceRev2Code _79_9 = NaceRev2Code.of("79.9");
   public static final NaceRev2Code _79_90 = NaceRev2Code.of("79.90");
   public static final NaceRev2Code _80 = NaceRev2Code.of("80");
   public static final NaceRev2Code _80_1 = NaceRev2Code.of("80.1");
   public static final NaceRev2Code _80_10 = NaceRev2Code.of("80.10");
   public static final NaceRev2Code _80_2 = NaceRev2Code.of("80.2");
   public static final NaceRev2Code _80_20 = NaceRev2Code.of("80.20");
   public static final NaceRev2Code _80_3 = NaceRev2Code.of("80.3");
   public static final NaceRev2Code _80_30 = NaceRev2Code.of("80.30");
   public static final NaceRev2Code _81 = NaceRev2Code.of("81");
   public static final NaceRev2Code _81_1 = NaceRev2Code.of("81.1");
   public static final NaceRev2Code _81_10 = NaceRev2Code.of("81.10");
   public static final NaceRev2Code _81_2 = NaceRev2Code.of("81.2");
   public static final NaceRev2Code _81_21 = NaceRev2Code.of("81.21");
   public static final NaceRev2Code _81_22 = NaceRev2Code.of("81.22");
   public static final NaceRev2Code _81_29 = NaceRev2Code.of("81.29");
   public static final NaceRev2Code _81_3 = NaceRev2Code.of("81.3");
   public static final NaceRev2Code _81_30 = NaceRev2Code.of("81.30");
   public static final NaceRev2Code _82 = NaceRev2Code.of("82");
   public static final NaceRev2Code _82_1 = NaceRev2Code.of("82.1");
   public static final NaceRev2Code _82_11 = NaceRev2Code.of("82.11");
   public static final NaceRev2Code _82_19 = NaceRev2Code.of("82.19");
   public static final NaceRev2Code _82_2 = NaceRev2Code.of("82.2");
   public static final NaceRev2Code _82_20 = NaceRev2Code.of("82.20");
   public static final NaceRev2Code _82_3 = NaceRev2Code.of("82.3");
   public static final NaceRev2Code _82_30 = NaceRev2Code.of("82.30");
   public static final NaceRev2Code _82_9 = NaceRev2Code.of("82.9");
   public static final NaceRev2Code _82_91 = NaceRev2Code.of("82.91");
   public static final NaceRev2Code _82_92 = NaceRev2Code.of("82.92");
   public static final NaceRev2Code _82_99 = NaceRev2Code.of("82.99");
   public static final NaceRev2Code _84 = NaceRev2Code.of("84");
   public static final NaceRev2Code _84_1 = NaceRev2Code.of("84.1");
   public static final NaceRev2Code _84_11 = NaceRev2Code.of("84.11");
   public static final NaceRev2Code _84_12 = NaceRev2Code.of("84.12");
   public static final NaceRev2Code _84_13 = NaceRev2Code.of("84.13");
   public static final NaceRev2Code _84_2 = NaceRev2Code.of("84.2");
   public static final NaceRev2Code _84_21 = NaceRev2Code.of("84.21");
   public static final NaceRev2Code _84_22 = NaceRev2Code.of("84.22");
   public static final NaceRev2Code _84_23 = NaceRev2Code.of("84.23");
   public static final NaceRev2Code _84_24 = NaceRev2Code.of("84.24");
   public static final NaceRev2Code _84_25 = NaceRev2Code.of("84.25");
   public static final NaceRev2Code _84_3 = NaceRev2Code.of("84.3");
   public static final NaceRev2Code _84_30 = NaceRev2Code.of("84.30");
   public static final NaceRev2Code _85 = NaceRev2Code.of("85");
   public static final NaceRev2Code _85_1 = NaceRev2Code.of("85.1");
   public static final NaceRev2Code _85_10 = NaceRev2Code.of("85.10");
   public static final NaceRev2Code _85_2 = NaceRev2Code.of("85.2");
   public static final NaceRev2Code _85_20 = NaceRev2Code.of("85.20");
   public static final NaceRev2Code _85_3 = NaceRev2Code.of("85.3");
   public static final NaceRev2Code _85_31 = NaceRev2Code.of("85.31");
   public static final NaceRev2Code _85_32 = NaceRev2Code.of("85.32");
   public static final NaceRev2Code _85_4 = NaceRev2Code.of("85.4");
   public static final NaceRev2Code _85_41 = NaceRev2Code.of("85.41");
   public static final NaceRev2Code _85_42 = NaceRev2Code.of("85.42");
   public static final NaceRev2Code _85_5 = NaceRev2Code.of("85.5");
   public static final NaceRev2Code _85_51 = NaceRev2Code.of("85.51");
   public static final NaceRev2Code _85_52 = NaceRev2Code.of("85.52");
   public static final NaceRev2Code _85_53 = NaceRev2Code.of("85.53");
   public static final NaceRev2Code _85_59 = NaceRev2Code.of("85.59");
   public static final NaceRev2Code _85_6 = NaceRev2Code.of("85.6");
   public static final NaceRev2Code _85_60 = NaceRev2Code.of("85.60");
   public static final NaceRev2Code _86 = NaceRev2Code.of("86");
   public static final NaceRev2Code _86_1 = NaceRev2Code.of("86.1");
   public static final NaceRev2Code _86_10 = NaceRev2Code.of("86.10");
   public static final NaceRev2Code _86_2 = NaceRev2Code.of("86.2");
   public static final NaceRev2Code _86_21 = NaceRev2Code.of("86.21");
   public static final NaceRev2Code _86_22 = NaceRev2Code.of("86.22");
   public static final NaceRev2Code _86_23 = NaceRev2Code.of("86.23");
   public static final NaceRev2Code _86_9 = NaceRev2Code.of("86.9");
   public static final NaceRev2Code _86_90 = NaceRev2Code.of("86.90");
   public static final NaceRev2Code _87 = NaceRev2Code.of("87");
   public static final NaceRev2Code _87_1 = NaceRev2Code.of("87.1");
   public static final NaceRev2Code _87_10 = NaceRev2Code.of("87.10");
   public static final NaceRev2Code _87_2 = NaceRev2Code.of("87.2");
   public static final NaceRev2Code _87_20 = NaceRev2Code.of("87.20");
   public static final NaceRev2Code _87_3 = NaceRev2Code.of("87.3");
   public static final NaceRev2Code _87_30 = NaceRev2Code.of("87.30");
   public static final NaceRev2Code _87_9 = NaceRev2Code.of("87.9");
   public static final NaceRev2Code _87_90 = NaceRev2Code.of("87.90");
   public static final NaceRev2Code _88 = NaceRev2Code.of("88");
   public static final NaceRev2Code _88_1 = NaceRev2Code.of("88.1");
   public static final NaceRev2Code _88_10 = NaceRev2Code.of("88.10");
   public static final NaceRev2Code _88_9 = NaceRev2Code.of("88.9");
   public static final NaceRev2Code _88_91 = NaceRev2Code.of("88.91");
   public static final NaceRev2Code _88_99 = NaceRev2Code.of("88.99");
   public static final NaceRev2Code _90 = NaceRev2Code.of("90");
   public static final NaceRev2Code _90_0 = NaceRev2Code.of("90.0");
   public static final NaceRev2Code _90_01 = NaceRev2Code.of("90.01");
   public static final NaceRev2Code _90_02 = NaceRev2Code.of("90.02");
   public static final NaceRev2Code _90_03 = NaceRev2Code.of("90.03");
   public static final NaceRev2Code _90_04 = NaceRev2Code.of("90.04");
   public static final NaceRev2Code _91 = NaceRev2Code.of("91");
   public static final NaceRev2Code _91_0 = NaceRev2Code.of("91.0");
   public static final NaceRev2Code _91_01 = NaceRev2Code.of("91.01");
   public static final NaceRev2Code _91_02 = NaceRev2Code.of("91.02");
   public static final NaceRev2Code _91_03 = NaceRev2Code.of("91.03");
   public static final NaceRev2Code _91_04 = NaceRev2Code.of("91.04");
   public static final NaceRev2Code _92 = NaceRev2Code.of("92");
   public static final NaceRev2Code _92_0 = NaceRev2Code.of("92.0");
   public static final NaceRev2Code _92_00 = NaceRev2Code.of("92.00");
   public static final NaceRev2Code _93 = NaceRev2Code.of("93");
   public static final NaceRev2Code _93_1 = NaceRev2Code.of("93.1");
   public static final NaceRev2Code _93_11 = NaceRev2Code.of("93.11");
   public static final NaceRev2Code _93_12 = NaceRev2Code.of("93.12");
   public static final NaceRev2Code _93_13 = NaceRev2Code.of("93.13");
   public static final NaceRev2Code _93_19 = NaceRev2Code.of("93.19");
   public static final NaceRev2Code _93_2 = NaceRev2Code.of("93.2");
   public static final NaceRev2Code _93_21 = NaceRev2Code.of("93.21");
   public static final NaceRev2Code _93_29 = NaceRev2Code.of("93.29");
   public static final NaceRev2Code _94 = NaceRev2Code.of("94");
   public static final NaceRev2Code _94_1 = NaceRev2Code.of("94.1");
   public static final NaceRev2Code _94_11 = NaceRev2Code.of("94.11");
   public static final NaceRev2Code _94_12 = NaceRev2Code.of("94.12");
   public static final NaceRev2Code _94_2 = NaceRev2Code.of("94.2");
   public static final NaceRev2Code _94_20 = NaceRev2Code.of("94.20");
   public static final NaceRev2Code _94_9 = NaceRev2Code.of("94.9");
   public static final NaceRev2Code _94_91 = NaceRev2Code.of("94.91");
   public static final NaceRev2Code _94_92 = NaceRev2Code.of("94.92");
   public static final NaceRev2Code _94_99 = NaceRev2Code.of("94.99");
   public static final NaceRev2Code _95 = NaceRev2Code.of("95");
   public static final NaceRev2Code _95_1 = NaceRev2Code.of("95.1");
   public static final NaceRev2Code _95_11 = NaceRev2Code.of("95.11");
   public static final NaceRev2Code _95_12 = NaceRev2Code.of("95.12");
   public static final NaceRev2Code _95_2 = NaceRev2Code.of("95.2");
   public static final NaceRev2Code _95_21 = NaceRev2Code.of("95.21");
   public static final NaceRev2Code _95_22 = NaceRev2Code.of("95.22");
   public static final NaceRev2Code _95_23 = NaceRev2Code.of("95.23");
   public static final NaceRev2Code _95_24 = NaceRev2Code.of("95.24");
   public static final NaceRev2Code _95_25 = NaceRev2Code.of("95.25");
   public static final NaceRev2Code _95_29 = NaceRev2Code.of("95.29");
   public static final NaceRev2Code _96 = NaceRev2Code.of("96");
   public static final NaceRev2Code _96_0 = NaceRev2Code.of("96.0");
   public static final NaceRev2Code _96_01 = NaceRev2Code.of("96.01");
   public static final NaceRev2Code _96_02 = NaceRev2Code.of("96.02");
   public static final NaceRev2Code _96_03 = NaceRev2Code.of("96.03");
   public static final NaceRev2Code _96_04 = NaceRev2Code.of("96.04");
   public static final NaceRev2Code _96_09 = NaceRev2Code.of("96.09");
   public static final NaceRev2Code _97 = NaceRev2Code.of("97");
   public static final NaceRev2Code _97_0 = NaceRev2Code.of("97.0");
   public static final NaceRev2Code _97_00 = NaceRev2Code.of("97.00");
   public static final NaceRev2Code _98 = NaceRev2Code.of("98");
   public static final NaceRev2Code _98_1 = NaceRev2Code.of("98.1");
   public static final NaceRev2Code _98_10 = NaceRev2Code.of("98.10");
   public static final NaceRev2Code _98_2 = NaceRev2Code.of("98.2");
   public static final NaceRev2Code _98_20 = NaceRev2Code.of("98.20");
   public static final NaceRev2Code _99 = NaceRev2Code.of("99");
   public static final NaceRev2Code _99_0 = NaceRev2Code.of("99.0");
   public static final NaceRev2Code _99_00 = NaceRev2Code.of("99.00");
   public static final NaceRev2Code A = NaceRev2Code.of("A");
   public static final NaceRev2Code B = NaceRev2Code.of("B");
   public static final NaceRev2Code C = NaceRev2Code.of("C");
   public static final NaceRev2Code D = NaceRev2Code.of("D");
   public static final NaceRev2Code E = NaceRev2Code.of("E");
   public static final NaceRev2Code F = NaceRev2Code.of("F");
   public static final NaceRev2Code G = NaceRev2Code.of("G");
   public static final NaceRev2Code H = NaceRev2Code.of("H");
   public static final NaceRev2Code I = NaceRev2Code.of("I");
   public static final NaceRev2Code J = NaceRev2Code.of("J");
   public static final NaceRev2Code K = NaceRev2Code.of("K");
   public static final NaceRev2Code L = NaceRev2Code.of("L");
   public static final NaceRev2Code M = NaceRev2Code.of("M");
   public static final NaceRev2Code N = NaceRev2Code.of("N");
   public static final NaceRev2Code O = NaceRev2Code.of("O");
   public static final NaceRev2Code P = NaceRev2Code.of("P");
   public static final NaceRev2Code Q = NaceRev2Code.of("Q");
   public static final NaceRev2Code R = NaceRev2Code.of("R");
   public static final NaceRev2Code S = NaceRev2Code.of("S");
   public static final NaceRev2Code T = NaceRev2Code.of("T");
   public static final NaceRev2Code U = NaceRev2Code.of("U");

}
