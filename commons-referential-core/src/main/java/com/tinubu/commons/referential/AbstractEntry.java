/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * Abstract {@link Entry} implementation.
 *
 * @param <R> referential type
 * @param <E> entry type
 * @param <D> entry data type
 */
public abstract class AbstractEntry<R extends Referential<E>, E extends Entry<E>, D extends EntryData>
      implements Entry<E> {

   protected final R referential;
   protected final D entryData;

   protected AbstractEntry(R referential, D entryData) {
      this.referential = notNull(referential, "referential");
      this.entryData = notNull(entryData, "entryData");
   }

   @Override
   public R referential() {
      return referential;
   }

   @Override
   public D entryData() {
      return this.entryData;
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", getClass().getSimpleName() + "[", "]")
            .add("referential=" + referential.displayName())
            .add("entryData=" + entryData)
            .toString();
   }

   /**
    * @param o other object to compare
    *
    * @return true if entries are equals
    */
   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      AbstractEntry<?, ?, ?> that = (AbstractEntry<?, ?, ?>) o;
      return Objects.equals(referential, that.referential) && Objects.equals(entryData, that.entryData);
   }

   @Override
   public int hashCode() {
      return Objects.hash(referential, entryData);
   }
}
