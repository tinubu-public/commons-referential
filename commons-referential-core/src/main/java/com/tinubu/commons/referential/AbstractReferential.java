/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential;

import static com.tinubu.commons.lang.model.ImmutableUtils.immutableList;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.stream.Collectors.toMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Abstract {@link Referential} implementation.
 *
 * @param <E> entry type
 * @param <D> entry data type
 */
public abstract class AbstractReferential<E extends Entry<E>, D extends EntryData> implements Referential<E> {

   private final ReferentialCode<E, ?> referentialCode;
   private final EntryDataLoader<D> entryDataLoader;
   private final Predicate<D> entryDataFilter;

   private Map<EntryCode, E> entriesCache;

   protected AbstractReferential(ReferentialCode<E, ?> referentialCode,
                                 EntryDataLoader<D> entryDataLoader,
                                 Predicate<D> entryDataFilter) {
      this.referentialCode = notNull(referentialCode, "referentialCode");
      this.entryDataLoader = notNull(entryDataLoader, "entryDataLoader");
      this.entryDataFilter = notNull(entryDataFilter, "entryDataFilter");
   }

   protected AbstractReferential(ReferentialCode<E, ?> referentialCode, EntryDataLoader<D> entryDataLoader) {
      this(referentialCode, entryDataLoader, __ -> true);
   }

   /**
    * Generates a new {@link Entry} instance from specified data.
    *
    * @param entryData entry data
    *
    * @return new entry instance
    */
   protected abstract E loadEntry(D entryData);

   @Override
   public ReferentialCode<E, ?> referentialCode() {
      return referentialCode;
   }

   @Override
   public Optional<E> optionalEntry(EntryCode entryCode) {
      notNull(entryCode, "entryCode");

      return nullable(entriesCache().get(entryCode));
   }

   @Override
   public E entry(EntryCode entryCode) {
      notNull(entryCode, "entryCode");

      return optionalEntry(entryCode).orElseThrow(() -> new UnknownEntryException(referentialCode, entryCode));
   }

   @Override
   public List<E> entries() {
      return immutableList(new ArrayList<>(entriesCache().values()));
   }

   private Map<EntryCode, E> entriesCache() {
      if (entriesCache == null || entryDataLoader.hasChanged()) {
         entriesCache = loadEntriesCache();
      }
      return entriesCache;
   }

   /**
    * Invalidates current entries cache. Entries will be reloaded at next access.
    */
   public void invalidateCache() {
      this.entriesCache = null;
   }

   private Map<EntryCode, E> loadEntriesCache() {
      return entryDataLoader.loadEntryDatas(entryDataFilter).stream().map(this::loadEntry).collect(toMap(Entry::entryCode, Function.identity()));
   }

}
