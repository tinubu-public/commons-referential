/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential;

import static com.tinubu.commons.lang.model.ImmutableUtils.immutableMap;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.or;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Collections.emptyMap;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code;

/**
 * Translations support for {@link EntryData}.
 */
public class DefaultTranslatableName implements TranslatableName {

   private final Map<Iso6392Alpha3Code, String> translations;
   /** Optional delegator when requested translation is not available. */
   private final TranslatableName delegate;

   private DefaultTranslatableName(Map<Iso6392Alpha3Code, String> translations, TranslatableName delegate) {
      this.translations = immutableMap(notNull(translations, "translations"));
      this.delegate = delegate;
   }

   private DefaultTranslatableName(Map<Iso6392Alpha3Code, String> translations) {
      this(translations, null);
   }

   @Override
   public TranslatableName delegate(TranslatableName delegate) {
      return new DefaultTranslatableName(translations, delegate);
   }

   @Override
   public Optional<String> name(Iso6392Alpha3Code language) {
      return or(nullable(translations.get(language)),
                () -> nullable(delegate).flatMap(delegate -> delegate.name(language)));
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      DefaultTranslatableName that = (DefaultTranslatableName) o;
      return Objects.equals(translations, that.translations) && Objects.equals(delegate, that.delegate);
   }

   @Override
   public int hashCode() {
      return Objects.hash(translations, delegate);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", DefaultTranslatableName.class.getSimpleName() + "[", "]")
            .add("translations=" + translations)
            .add("delegate=" + delegate)
            .toString();
   }

   public static class DefaultTranslatableNameBuilder {
      private final Map<Iso6392Alpha3Code, String> translations = new HashMap<>();

      public static DefaultTranslatableName empty() {
         return new DefaultTranslatableName(emptyMap());
      }

      public DefaultTranslatableNameBuilder name(Iso6392Alpha3Code language, String name) {
         notNull(language, "language");

         if (name != null) {
            translations.put(language, name);
         }

         return this;
      }

      public DefaultTranslatableName build() {
         return new DefaultTranslatableName(translations);
      }
   }

}
