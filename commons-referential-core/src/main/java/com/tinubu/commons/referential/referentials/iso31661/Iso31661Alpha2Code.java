/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.iso31661;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.referential.EntryCode;

/**
 * ISO 3166-1 alpha-2 country code.
 * Country code format is {@code <ALPHA><ALPHA>} in uppercase (e.g.: {@code AA}).
 */
public class Iso31661Alpha2Code extends EntryCode {

   private Iso31661Alpha2Code(String entryCode) {
      super(entryCode);
   }

   /**
    * Creates ISO 3166-1 alpha-2 code from entry code.
    * Entry code is case-insensitive.
    */
   public static Iso31661Alpha2Code of(String entryCode) {
      return new Iso31661Alpha2Code(notNull(entryCode, "entryCode").toUpperCase());
   }

   @Override
   protected boolean validEntryCodeFormat(String entryCode) {
      return entryCode.length() == 2 && entryCode.chars().allMatch(c -> c >= 'A' && c <= 'Z');
   }

   /* Generated codes at 2021-10-07T21:40:13+02:00 using default loader. */
   public static final Iso31661Alpha2Code AA = Iso31661Alpha2Code.of("AA");
   public static final Iso31661Alpha2Code AD = Iso31661Alpha2Code.of("AD");
   public static final Iso31661Alpha2Code AE = Iso31661Alpha2Code.of("AE");
   public static final Iso31661Alpha2Code AF = Iso31661Alpha2Code.of("AF");
   public static final Iso31661Alpha2Code AG = Iso31661Alpha2Code.of("AG");
   public static final Iso31661Alpha2Code AI = Iso31661Alpha2Code.of("AI");
   public static final Iso31661Alpha2Code AL = Iso31661Alpha2Code.of("AL");
   public static final Iso31661Alpha2Code AM = Iso31661Alpha2Code.of("AM");
   public static final Iso31661Alpha2Code AO = Iso31661Alpha2Code.of("AO");
   public static final Iso31661Alpha2Code AQ = Iso31661Alpha2Code.of("AQ");
   public static final Iso31661Alpha2Code AR = Iso31661Alpha2Code.of("AR");
   public static final Iso31661Alpha2Code AS = Iso31661Alpha2Code.of("AS");
   public static final Iso31661Alpha2Code AT = Iso31661Alpha2Code.of("AT");
   public static final Iso31661Alpha2Code AU = Iso31661Alpha2Code.of("AU");
   public static final Iso31661Alpha2Code AW = Iso31661Alpha2Code.of("AW");
   public static final Iso31661Alpha2Code AX = Iso31661Alpha2Code.of("AX");
   public static final Iso31661Alpha2Code AZ = Iso31661Alpha2Code.of("AZ");
   public static final Iso31661Alpha2Code BA = Iso31661Alpha2Code.of("BA");
   public static final Iso31661Alpha2Code BB = Iso31661Alpha2Code.of("BB");
   public static final Iso31661Alpha2Code BD = Iso31661Alpha2Code.of("BD");
   public static final Iso31661Alpha2Code BE = Iso31661Alpha2Code.of("BE");
   public static final Iso31661Alpha2Code BF = Iso31661Alpha2Code.of("BF");
   public static final Iso31661Alpha2Code BG = Iso31661Alpha2Code.of("BG");
   public static final Iso31661Alpha2Code BH = Iso31661Alpha2Code.of("BH");
   public static final Iso31661Alpha2Code BI = Iso31661Alpha2Code.of("BI");
   public static final Iso31661Alpha2Code BJ = Iso31661Alpha2Code.of("BJ");
   public static final Iso31661Alpha2Code BL = Iso31661Alpha2Code.of("BL");
   public static final Iso31661Alpha2Code BM = Iso31661Alpha2Code.of("BM");
   public static final Iso31661Alpha2Code BN = Iso31661Alpha2Code.of("BN");
   public static final Iso31661Alpha2Code BO = Iso31661Alpha2Code.of("BO");
   public static final Iso31661Alpha2Code BQ = Iso31661Alpha2Code.of("BQ");
   public static final Iso31661Alpha2Code BR = Iso31661Alpha2Code.of("BR");
   public static final Iso31661Alpha2Code BS = Iso31661Alpha2Code.of("BS");
   public static final Iso31661Alpha2Code BT = Iso31661Alpha2Code.of("BT");
   public static final Iso31661Alpha2Code BV = Iso31661Alpha2Code.of("BV");
   public static final Iso31661Alpha2Code BW = Iso31661Alpha2Code.of("BW");
   public static final Iso31661Alpha2Code BY = Iso31661Alpha2Code.of("BY");
   public static final Iso31661Alpha2Code BZ = Iso31661Alpha2Code.of("BZ");
   public static final Iso31661Alpha2Code CA = Iso31661Alpha2Code.of("CA");
   public static final Iso31661Alpha2Code CC = Iso31661Alpha2Code.of("CC");
   public static final Iso31661Alpha2Code CD = Iso31661Alpha2Code.of("CD");
   public static final Iso31661Alpha2Code CF = Iso31661Alpha2Code.of("CF");
   public static final Iso31661Alpha2Code CG = Iso31661Alpha2Code.of("CG");
   public static final Iso31661Alpha2Code CH = Iso31661Alpha2Code.of("CH");
   public static final Iso31661Alpha2Code CI = Iso31661Alpha2Code.of("CI");
   public static final Iso31661Alpha2Code CK = Iso31661Alpha2Code.of("CK");
   public static final Iso31661Alpha2Code CL = Iso31661Alpha2Code.of("CL");
   public static final Iso31661Alpha2Code CM = Iso31661Alpha2Code.of("CM");
   public static final Iso31661Alpha2Code CN = Iso31661Alpha2Code.of("CN");
   public static final Iso31661Alpha2Code CO = Iso31661Alpha2Code.of("CO");
   public static final Iso31661Alpha2Code CR = Iso31661Alpha2Code.of("CR");
   public static final Iso31661Alpha2Code CU = Iso31661Alpha2Code.of("CU");
   public static final Iso31661Alpha2Code CV = Iso31661Alpha2Code.of("CV");
   public static final Iso31661Alpha2Code CW = Iso31661Alpha2Code.of("CW");
   public static final Iso31661Alpha2Code CX = Iso31661Alpha2Code.of("CX");
   public static final Iso31661Alpha2Code CY = Iso31661Alpha2Code.of("CY");
   public static final Iso31661Alpha2Code CZ = Iso31661Alpha2Code.of("CZ");
   public static final Iso31661Alpha2Code DE = Iso31661Alpha2Code.of("DE");
   public static final Iso31661Alpha2Code DJ = Iso31661Alpha2Code.of("DJ");
   public static final Iso31661Alpha2Code DK = Iso31661Alpha2Code.of("DK");
   public static final Iso31661Alpha2Code DM = Iso31661Alpha2Code.of("DM");
   public static final Iso31661Alpha2Code DO = Iso31661Alpha2Code.of("DO");
   public static final Iso31661Alpha2Code DZ = Iso31661Alpha2Code.of("DZ");
   public static final Iso31661Alpha2Code EC = Iso31661Alpha2Code.of("EC");
   public static final Iso31661Alpha2Code EE = Iso31661Alpha2Code.of("EE");
   public static final Iso31661Alpha2Code EG = Iso31661Alpha2Code.of("EG");
   public static final Iso31661Alpha2Code EH = Iso31661Alpha2Code.of("EH");
   public static final Iso31661Alpha2Code ER = Iso31661Alpha2Code.of("ER");
   public static final Iso31661Alpha2Code ES = Iso31661Alpha2Code.of("ES");
   public static final Iso31661Alpha2Code ET = Iso31661Alpha2Code.of("ET");
   public static final Iso31661Alpha2Code FI = Iso31661Alpha2Code.of("FI");
   public static final Iso31661Alpha2Code FJ = Iso31661Alpha2Code.of("FJ");
   public static final Iso31661Alpha2Code FK = Iso31661Alpha2Code.of("FK");
   public static final Iso31661Alpha2Code FM = Iso31661Alpha2Code.of("FM");
   public static final Iso31661Alpha2Code FO = Iso31661Alpha2Code.of("FO");
   public static final Iso31661Alpha2Code FR = Iso31661Alpha2Code.of("FR");
   public static final Iso31661Alpha2Code GA = Iso31661Alpha2Code.of("GA");
   public static final Iso31661Alpha2Code GB = Iso31661Alpha2Code.of("GB");
   public static final Iso31661Alpha2Code GD = Iso31661Alpha2Code.of("GD");
   public static final Iso31661Alpha2Code GE = Iso31661Alpha2Code.of("GE");
   public static final Iso31661Alpha2Code GF = Iso31661Alpha2Code.of("GF");
   public static final Iso31661Alpha2Code GG = Iso31661Alpha2Code.of("GG");
   public static final Iso31661Alpha2Code GH = Iso31661Alpha2Code.of("GH");
   public static final Iso31661Alpha2Code GI = Iso31661Alpha2Code.of("GI");
   public static final Iso31661Alpha2Code GL = Iso31661Alpha2Code.of("GL");
   public static final Iso31661Alpha2Code GM = Iso31661Alpha2Code.of("GM");
   public static final Iso31661Alpha2Code GN = Iso31661Alpha2Code.of("GN");
   public static final Iso31661Alpha2Code GP = Iso31661Alpha2Code.of("GP");
   public static final Iso31661Alpha2Code GQ = Iso31661Alpha2Code.of("GQ");
   public static final Iso31661Alpha2Code GR = Iso31661Alpha2Code.of("GR");
   public static final Iso31661Alpha2Code GS = Iso31661Alpha2Code.of("GS");
   public static final Iso31661Alpha2Code GT = Iso31661Alpha2Code.of("GT");
   public static final Iso31661Alpha2Code GU = Iso31661Alpha2Code.of("GU");
   public static final Iso31661Alpha2Code GW = Iso31661Alpha2Code.of("GW");
   public static final Iso31661Alpha2Code GY = Iso31661Alpha2Code.of("GY");
   public static final Iso31661Alpha2Code HK = Iso31661Alpha2Code.of("HK");
   public static final Iso31661Alpha2Code HM = Iso31661Alpha2Code.of("HM");
   public static final Iso31661Alpha2Code HN = Iso31661Alpha2Code.of("HN");
   public static final Iso31661Alpha2Code HR = Iso31661Alpha2Code.of("HR");
   public static final Iso31661Alpha2Code HT = Iso31661Alpha2Code.of("HT");
   public static final Iso31661Alpha2Code HU = Iso31661Alpha2Code.of("HU");
   public static final Iso31661Alpha2Code ID = Iso31661Alpha2Code.of("ID");
   public static final Iso31661Alpha2Code IE = Iso31661Alpha2Code.of("IE");
   public static final Iso31661Alpha2Code IL = Iso31661Alpha2Code.of("IL");
   public static final Iso31661Alpha2Code IM = Iso31661Alpha2Code.of("IM");
   public static final Iso31661Alpha2Code IN = Iso31661Alpha2Code.of("IN");
   public static final Iso31661Alpha2Code IO = Iso31661Alpha2Code.of("IO");
   public static final Iso31661Alpha2Code IQ = Iso31661Alpha2Code.of("IQ");
   public static final Iso31661Alpha2Code IR = Iso31661Alpha2Code.of("IR");
   public static final Iso31661Alpha2Code IS = Iso31661Alpha2Code.of("IS");
   public static final Iso31661Alpha2Code IT = Iso31661Alpha2Code.of("IT");
   public static final Iso31661Alpha2Code JE = Iso31661Alpha2Code.of("JE");
   public static final Iso31661Alpha2Code JM = Iso31661Alpha2Code.of("JM");
   public static final Iso31661Alpha2Code JO = Iso31661Alpha2Code.of("JO");
   public static final Iso31661Alpha2Code JP = Iso31661Alpha2Code.of("JP");
   public static final Iso31661Alpha2Code KE = Iso31661Alpha2Code.of("KE");
   public static final Iso31661Alpha2Code KG = Iso31661Alpha2Code.of("KG");
   public static final Iso31661Alpha2Code KH = Iso31661Alpha2Code.of("KH");
   public static final Iso31661Alpha2Code KI = Iso31661Alpha2Code.of("KI");
   public static final Iso31661Alpha2Code KM = Iso31661Alpha2Code.of("KM");
   public static final Iso31661Alpha2Code KN = Iso31661Alpha2Code.of("KN");
   public static final Iso31661Alpha2Code KP = Iso31661Alpha2Code.of("KP");
   public static final Iso31661Alpha2Code KR = Iso31661Alpha2Code.of("KR");
   public static final Iso31661Alpha2Code KW = Iso31661Alpha2Code.of("KW");
   public static final Iso31661Alpha2Code KY = Iso31661Alpha2Code.of("KY");
   public static final Iso31661Alpha2Code KZ = Iso31661Alpha2Code.of("KZ");
   public static final Iso31661Alpha2Code LA = Iso31661Alpha2Code.of("LA");
   public static final Iso31661Alpha2Code LB = Iso31661Alpha2Code.of("LB");
   public static final Iso31661Alpha2Code LC = Iso31661Alpha2Code.of("LC");
   public static final Iso31661Alpha2Code LI = Iso31661Alpha2Code.of("LI");
   public static final Iso31661Alpha2Code LK = Iso31661Alpha2Code.of("LK");
   public static final Iso31661Alpha2Code LR = Iso31661Alpha2Code.of("LR");
   public static final Iso31661Alpha2Code LS = Iso31661Alpha2Code.of("LS");
   public static final Iso31661Alpha2Code LT = Iso31661Alpha2Code.of("LT");
   public static final Iso31661Alpha2Code LU = Iso31661Alpha2Code.of("LU");
   public static final Iso31661Alpha2Code LV = Iso31661Alpha2Code.of("LV");
   public static final Iso31661Alpha2Code LY = Iso31661Alpha2Code.of("LY");
   public static final Iso31661Alpha2Code MA = Iso31661Alpha2Code.of("MA");
   public static final Iso31661Alpha2Code MC = Iso31661Alpha2Code.of("MC");
   public static final Iso31661Alpha2Code MD = Iso31661Alpha2Code.of("MD");
   public static final Iso31661Alpha2Code ME = Iso31661Alpha2Code.of("ME");
   public static final Iso31661Alpha2Code MF = Iso31661Alpha2Code.of("MF");
   public static final Iso31661Alpha2Code MG = Iso31661Alpha2Code.of("MG");
   public static final Iso31661Alpha2Code MH = Iso31661Alpha2Code.of("MH");
   public static final Iso31661Alpha2Code MK = Iso31661Alpha2Code.of("MK");
   public static final Iso31661Alpha2Code ML = Iso31661Alpha2Code.of("ML");
   public static final Iso31661Alpha2Code MM = Iso31661Alpha2Code.of("MM");
   public static final Iso31661Alpha2Code MN = Iso31661Alpha2Code.of("MN");
   public static final Iso31661Alpha2Code MO = Iso31661Alpha2Code.of("MO");
   public static final Iso31661Alpha2Code MP = Iso31661Alpha2Code.of("MP");
   public static final Iso31661Alpha2Code MQ = Iso31661Alpha2Code.of("MQ");
   public static final Iso31661Alpha2Code MR = Iso31661Alpha2Code.of("MR");
   public static final Iso31661Alpha2Code MS = Iso31661Alpha2Code.of("MS");
   public static final Iso31661Alpha2Code MT = Iso31661Alpha2Code.of("MT");
   public static final Iso31661Alpha2Code MU = Iso31661Alpha2Code.of("MU");
   public static final Iso31661Alpha2Code MV = Iso31661Alpha2Code.of("MV");
   public static final Iso31661Alpha2Code MW = Iso31661Alpha2Code.of("MW");
   public static final Iso31661Alpha2Code MX = Iso31661Alpha2Code.of("MX");
   public static final Iso31661Alpha2Code MY = Iso31661Alpha2Code.of("MY");
   public static final Iso31661Alpha2Code MZ = Iso31661Alpha2Code.of("MZ");
   public static final Iso31661Alpha2Code NA = Iso31661Alpha2Code.of("NA");
   public static final Iso31661Alpha2Code NC = Iso31661Alpha2Code.of("NC");
   public static final Iso31661Alpha2Code NE = Iso31661Alpha2Code.of("NE");
   public static final Iso31661Alpha2Code NF = Iso31661Alpha2Code.of("NF");
   public static final Iso31661Alpha2Code NG = Iso31661Alpha2Code.of("NG");
   public static final Iso31661Alpha2Code NI = Iso31661Alpha2Code.of("NI");
   public static final Iso31661Alpha2Code NL = Iso31661Alpha2Code.of("NL");
   public static final Iso31661Alpha2Code NO = Iso31661Alpha2Code.of("NO");
   public static final Iso31661Alpha2Code NP = Iso31661Alpha2Code.of("NP");
   public static final Iso31661Alpha2Code NR = Iso31661Alpha2Code.of("NR");
   public static final Iso31661Alpha2Code NU = Iso31661Alpha2Code.of("NU");
   public static final Iso31661Alpha2Code NZ = Iso31661Alpha2Code.of("NZ");
   public static final Iso31661Alpha2Code OM = Iso31661Alpha2Code.of("OM");
   public static final Iso31661Alpha2Code PA = Iso31661Alpha2Code.of("PA");
   public static final Iso31661Alpha2Code PE = Iso31661Alpha2Code.of("PE");
   public static final Iso31661Alpha2Code PF = Iso31661Alpha2Code.of("PF");
   public static final Iso31661Alpha2Code PG = Iso31661Alpha2Code.of("PG");
   public static final Iso31661Alpha2Code PH = Iso31661Alpha2Code.of("PH");
   public static final Iso31661Alpha2Code PK = Iso31661Alpha2Code.of("PK");
   public static final Iso31661Alpha2Code PL = Iso31661Alpha2Code.of("PL");
   public static final Iso31661Alpha2Code PM = Iso31661Alpha2Code.of("PM");
   public static final Iso31661Alpha2Code PN = Iso31661Alpha2Code.of("PN");
   public static final Iso31661Alpha2Code PR = Iso31661Alpha2Code.of("PR");
   public static final Iso31661Alpha2Code PS = Iso31661Alpha2Code.of("PS");
   public static final Iso31661Alpha2Code PT = Iso31661Alpha2Code.of("PT");
   public static final Iso31661Alpha2Code PW = Iso31661Alpha2Code.of("PW");
   public static final Iso31661Alpha2Code PY = Iso31661Alpha2Code.of("PY");
   public static final Iso31661Alpha2Code QA = Iso31661Alpha2Code.of("QA");
   public static final Iso31661Alpha2Code QM = Iso31661Alpha2Code.of("QM");
   public static final Iso31661Alpha2Code QN = Iso31661Alpha2Code.of("QN");
   public static final Iso31661Alpha2Code QO = Iso31661Alpha2Code.of("QO");
   public static final Iso31661Alpha2Code QP = Iso31661Alpha2Code.of("QP");
   public static final Iso31661Alpha2Code QQ = Iso31661Alpha2Code.of("QQ");
   public static final Iso31661Alpha2Code QR = Iso31661Alpha2Code.of("QR");
   public static final Iso31661Alpha2Code QS = Iso31661Alpha2Code.of("QS");
   public static final Iso31661Alpha2Code QT = Iso31661Alpha2Code.of("QT");
   public static final Iso31661Alpha2Code QU = Iso31661Alpha2Code.of("QU");
   public static final Iso31661Alpha2Code QV = Iso31661Alpha2Code.of("QV");
   public static final Iso31661Alpha2Code QW = Iso31661Alpha2Code.of("QW");
   public static final Iso31661Alpha2Code QX = Iso31661Alpha2Code.of("QX");
   public static final Iso31661Alpha2Code QY = Iso31661Alpha2Code.of("QY");
   public static final Iso31661Alpha2Code QZ = Iso31661Alpha2Code.of("QZ");
   public static final Iso31661Alpha2Code RE = Iso31661Alpha2Code.of("RE");
   public static final Iso31661Alpha2Code RO = Iso31661Alpha2Code.of("RO");
   public static final Iso31661Alpha2Code RS = Iso31661Alpha2Code.of("RS");
   public static final Iso31661Alpha2Code RU = Iso31661Alpha2Code.of("RU");
   public static final Iso31661Alpha2Code RW = Iso31661Alpha2Code.of("RW");
   public static final Iso31661Alpha2Code SA = Iso31661Alpha2Code.of("SA");
   public static final Iso31661Alpha2Code SB = Iso31661Alpha2Code.of("SB");
   public static final Iso31661Alpha2Code SC = Iso31661Alpha2Code.of("SC");
   public static final Iso31661Alpha2Code SD = Iso31661Alpha2Code.of("SD");
   public static final Iso31661Alpha2Code SE = Iso31661Alpha2Code.of("SE");
   public static final Iso31661Alpha2Code SG = Iso31661Alpha2Code.of("SG");
   public static final Iso31661Alpha2Code SH = Iso31661Alpha2Code.of("SH");
   public static final Iso31661Alpha2Code SI = Iso31661Alpha2Code.of("SI");
   public static final Iso31661Alpha2Code SJ = Iso31661Alpha2Code.of("SJ");
   public static final Iso31661Alpha2Code SK = Iso31661Alpha2Code.of("SK");
   public static final Iso31661Alpha2Code SL = Iso31661Alpha2Code.of("SL");
   public static final Iso31661Alpha2Code SM = Iso31661Alpha2Code.of("SM");
   public static final Iso31661Alpha2Code SN = Iso31661Alpha2Code.of("SN");
   public static final Iso31661Alpha2Code SO = Iso31661Alpha2Code.of("SO");
   public static final Iso31661Alpha2Code SR = Iso31661Alpha2Code.of("SR");
   public static final Iso31661Alpha2Code SS = Iso31661Alpha2Code.of("SS");
   public static final Iso31661Alpha2Code ST = Iso31661Alpha2Code.of("ST");
   public static final Iso31661Alpha2Code SV = Iso31661Alpha2Code.of("SV");
   public static final Iso31661Alpha2Code SX = Iso31661Alpha2Code.of("SX");
   public static final Iso31661Alpha2Code SY = Iso31661Alpha2Code.of("SY");
   public static final Iso31661Alpha2Code SZ = Iso31661Alpha2Code.of("SZ");
   public static final Iso31661Alpha2Code TC = Iso31661Alpha2Code.of("TC");
   public static final Iso31661Alpha2Code TD = Iso31661Alpha2Code.of("TD");
   public static final Iso31661Alpha2Code TF = Iso31661Alpha2Code.of("TF");
   public static final Iso31661Alpha2Code TG = Iso31661Alpha2Code.of("TG");
   public static final Iso31661Alpha2Code TH = Iso31661Alpha2Code.of("TH");
   public static final Iso31661Alpha2Code TJ = Iso31661Alpha2Code.of("TJ");
   public static final Iso31661Alpha2Code TK = Iso31661Alpha2Code.of("TK");
   public static final Iso31661Alpha2Code TL = Iso31661Alpha2Code.of("TL");
   public static final Iso31661Alpha2Code TM = Iso31661Alpha2Code.of("TM");
   public static final Iso31661Alpha2Code TN = Iso31661Alpha2Code.of("TN");
   public static final Iso31661Alpha2Code TO = Iso31661Alpha2Code.of("TO");
   public static final Iso31661Alpha2Code TR = Iso31661Alpha2Code.of("TR");
   public static final Iso31661Alpha2Code TT = Iso31661Alpha2Code.of("TT");
   public static final Iso31661Alpha2Code TV = Iso31661Alpha2Code.of("TV");
   public static final Iso31661Alpha2Code TW = Iso31661Alpha2Code.of("TW");
   public static final Iso31661Alpha2Code TZ = Iso31661Alpha2Code.of("TZ");
   public static final Iso31661Alpha2Code UA = Iso31661Alpha2Code.of("UA");
   public static final Iso31661Alpha2Code UG = Iso31661Alpha2Code.of("UG");
   public static final Iso31661Alpha2Code UM = Iso31661Alpha2Code.of("UM");
   public static final Iso31661Alpha2Code US = Iso31661Alpha2Code.of("US");
   public static final Iso31661Alpha2Code UY = Iso31661Alpha2Code.of("UY");
   public static final Iso31661Alpha2Code UZ = Iso31661Alpha2Code.of("UZ");
   public static final Iso31661Alpha2Code VA = Iso31661Alpha2Code.of("VA");
   public static final Iso31661Alpha2Code VC = Iso31661Alpha2Code.of("VC");
   public static final Iso31661Alpha2Code VE = Iso31661Alpha2Code.of("VE");
   public static final Iso31661Alpha2Code VG = Iso31661Alpha2Code.of("VG");
   public static final Iso31661Alpha2Code VI = Iso31661Alpha2Code.of("VI");
   public static final Iso31661Alpha2Code VN = Iso31661Alpha2Code.of("VN");
   public static final Iso31661Alpha2Code VU = Iso31661Alpha2Code.of("VU");
   public static final Iso31661Alpha2Code WF = Iso31661Alpha2Code.of("WF");
   public static final Iso31661Alpha2Code WS = Iso31661Alpha2Code.of("WS");
   public static final Iso31661Alpha2Code XA = Iso31661Alpha2Code.of("XA");
   public static final Iso31661Alpha2Code XB = Iso31661Alpha2Code.of("XB");
   public static final Iso31661Alpha2Code XC = Iso31661Alpha2Code.of("XC");
   public static final Iso31661Alpha2Code XD = Iso31661Alpha2Code.of("XD");
   public static final Iso31661Alpha2Code XE = Iso31661Alpha2Code.of("XE");
   public static final Iso31661Alpha2Code XF = Iso31661Alpha2Code.of("XF");
   public static final Iso31661Alpha2Code XG = Iso31661Alpha2Code.of("XG");
   public static final Iso31661Alpha2Code XH = Iso31661Alpha2Code.of("XH");
   public static final Iso31661Alpha2Code XI = Iso31661Alpha2Code.of("XI");
   public static final Iso31661Alpha2Code XJ = Iso31661Alpha2Code.of("XJ");
   public static final Iso31661Alpha2Code XK = Iso31661Alpha2Code.of("XK");
   public static final Iso31661Alpha2Code XL = Iso31661Alpha2Code.of("XL");
   public static final Iso31661Alpha2Code XM = Iso31661Alpha2Code.of("XM");
   public static final Iso31661Alpha2Code XN = Iso31661Alpha2Code.of("XN");
   public static final Iso31661Alpha2Code XO = Iso31661Alpha2Code.of("XO");
   public static final Iso31661Alpha2Code XP = Iso31661Alpha2Code.of("XP");
   public static final Iso31661Alpha2Code XQ = Iso31661Alpha2Code.of("XQ");
   public static final Iso31661Alpha2Code XR = Iso31661Alpha2Code.of("XR");
   public static final Iso31661Alpha2Code XS = Iso31661Alpha2Code.of("XS");
   public static final Iso31661Alpha2Code XT = Iso31661Alpha2Code.of("XT");
   public static final Iso31661Alpha2Code XU = Iso31661Alpha2Code.of("XU");
   public static final Iso31661Alpha2Code XV = Iso31661Alpha2Code.of("XV");
   public static final Iso31661Alpha2Code XW = Iso31661Alpha2Code.of("XW");
   public static final Iso31661Alpha2Code XX = Iso31661Alpha2Code.of("XX");
   public static final Iso31661Alpha2Code XY = Iso31661Alpha2Code.of("XY");
   public static final Iso31661Alpha2Code XZ = Iso31661Alpha2Code.of("XZ");
   public static final Iso31661Alpha2Code YE = Iso31661Alpha2Code.of("YE");
   public static final Iso31661Alpha2Code YT = Iso31661Alpha2Code.of("YT");
   public static final Iso31661Alpha2Code ZA = Iso31661Alpha2Code.of("ZA");
   public static final Iso31661Alpha2Code ZM = Iso31661Alpha2Code.of("ZM");
   public static final Iso31661Alpha2Code ZW = Iso31661Alpha2Code.of("ZW");
   public static final Iso31661Alpha2Code ZZ = Iso31661Alpha2Code.of("ZZ");

}


