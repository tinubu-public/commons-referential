/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.iso31661;

import com.tinubu.commons.referential.EntryCode;

/**
 * ISO 3166-1 numeric country code.
 * Country code format is {@code <digit><digit><digit>} (e.g.: {@code 020}).
 */
public class Iso31661NumericCode extends EntryCode {

   private Iso31661NumericCode(String entryCode) {
      super(entryCode);
   }

   /**
    * Creates ISO 3166-1 numeric code from entry code.
    * Entry code is case-insensitive.
    */
   public static Iso31661NumericCode of(String entryCode) {
      return new Iso31661NumericCode(entryCode);
   }

   /** Creates ISO 3166-1 numeric code from numeric entry code. */
   public static Iso31661NumericCode of(int entryCode) {
      return new Iso31661NumericCode(String.format("%03d", entryCode));
   }

   @Override
   protected boolean validEntryCodeFormat(String entryCode) {
      return entryCode.length() == 3 && entryCode.chars().allMatch(c -> c >= '0' && c <= '9');
   }

   /* Generated codes at 2021-10-07T21:40:13+02:00 using default loader. */
   public static final Iso31661NumericCode _004 = Iso31661NumericCode.of("004");
   public static final Iso31661NumericCode _008 = Iso31661NumericCode.of("008");
   public static final Iso31661NumericCode _010 = Iso31661NumericCode.of("010");
   public static final Iso31661NumericCode _012 = Iso31661NumericCode.of("012");
   public static final Iso31661NumericCode _016 = Iso31661NumericCode.of("016");
   public static final Iso31661NumericCode _020 = Iso31661NumericCode.of("020");
   public static final Iso31661NumericCode _024 = Iso31661NumericCode.of("024");
   public static final Iso31661NumericCode _028 = Iso31661NumericCode.of("028");
   public static final Iso31661NumericCode _031 = Iso31661NumericCode.of("031");
   public static final Iso31661NumericCode _032 = Iso31661NumericCode.of("032");
   public static final Iso31661NumericCode _036 = Iso31661NumericCode.of("036");
   public static final Iso31661NumericCode _040 = Iso31661NumericCode.of("040");
   public static final Iso31661NumericCode _044 = Iso31661NumericCode.of("044");
   public static final Iso31661NumericCode _048 = Iso31661NumericCode.of("048");
   public static final Iso31661NumericCode _050 = Iso31661NumericCode.of("050");
   public static final Iso31661NumericCode _051 = Iso31661NumericCode.of("051");
   public static final Iso31661NumericCode _052 = Iso31661NumericCode.of("052");
   public static final Iso31661NumericCode _056 = Iso31661NumericCode.of("056");
   public static final Iso31661NumericCode _060 = Iso31661NumericCode.of("060");
   public static final Iso31661NumericCode _064 = Iso31661NumericCode.of("064");
   public static final Iso31661NumericCode _068 = Iso31661NumericCode.of("068");
   public static final Iso31661NumericCode _070 = Iso31661NumericCode.of("070");
   public static final Iso31661NumericCode _072 = Iso31661NumericCode.of("072");
   public static final Iso31661NumericCode _074 = Iso31661NumericCode.of("074");
   public static final Iso31661NumericCode _076 = Iso31661NumericCode.of("076");
   public static final Iso31661NumericCode _084 = Iso31661NumericCode.of("084");
   public static final Iso31661NumericCode _086 = Iso31661NumericCode.of("086");
   public static final Iso31661NumericCode _090 = Iso31661NumericCode.of("090");
   public static final Iso31661NumericCode _092 = Iso31661NumericCode.of("092");
   public static final Iso31661NumericCode _096 = Iso31661NumericCode.of("096");
   public static final Iso31661NumericCode _100 = Iso31661NumericCode.of("100");
   public static final Iso31661NumericCode _104 = Iso31661NumericCode.of("104");
   public static final Iso31661NumericCode _108 = Iso31661NumericCode.of("108");
   public static final Iso31661NumericCode _112 = Iso31661NumericCode.of("112");
   public static final Iso31661NumericCode _116 = Iso31661NumericCode.of("116");
   public static final Iso31661NumericCode _120 = Iso31661NumericCode.of("120");
   public static final Iso31661NumericCode _124 = Iso31661NumericCode.of("124");
   public static final Iso31661NumericCode _132 = Iso31661NumericCode.of("132");
   public static final Iso31661NumericCode _136 = Iso31661NumericCode.of("136");
   public static final Iso31661NumericCode _140 = Iso31661NumericCode.of("140");
   public static final Iso31661NumericCode _144 = Iso31661NumericCode.of("144");
   public static final Iso31661NumericCode _148 = Iso31661NumericCode.of("148");
   public static final Iso31661NumericCode _152 = Iso31661NumericCode.of("152");
   public static final Iso31661NumericCode _156 = Iso31661NumericCode.of("156");
   public static final Iso31661NumericCode _158 = Iso31661NumericCode.of("158");
   public static final Iso31661NumericCode _162 = Iso31661NumericCode.of("162");
   public static final Iso31661NumericCode _166 = Iso31661NumericCode.of("166");
   public static final Iso31661NumericCode _170 = Iso31661NumericCode.of("170");
   public static final Iso31661NumericCode _174 = Iso31661NumericCode.of("174");
   public static final Iso31661NumericCode _175 = Iso31661NumericCode.of("175");
   public static final Iso31661NumericCode _178 = Iso31661NumericCode.of("178");
   public static final Iso31661NumericCode _180 = Iso31661NumericCode.of("180");
   public static final Iso31661NumericCode _184 = Iso31661NumericCode.of("184");
   public static final Iso31661NumericCode _188 = Iso31661NumericCode.of("188");
   public static final Iso31661NumericCode _191 = Iso31661NumericCode.of("191");
   public static final Iso31661NumericCode _192 = Iso31661NumericCode.of("192");
   public static final Iso31661NumericCode _196 = Iso31661NumericCode.of("196");
   public static final Iso31661NumericCode _203 = Iso31661NumericCode.of("203");
   public static final Iso31661NumericCode _204 = Iso31661NumericCode.of("204");
   public static final Iso31661NumericCode _208 = Iso31661NumericCode.of("208");
   public static final Iso31661NumericCode _212 = Iso31661NumericCode.of("212");
   public static final Iso31661NumericCode _214 = Iso31661NumericCode.of("214");
   public static final Iso31661NumericCode _218 = Iso31661NumericCode.of("218");
   public static final Iso31661NumericCode _222 = Iso31661NumericCode.of("222");
   public static final Iso31661NumericCode _226 = Iso31661NumericCode.of("226");
   public static final Iso31661NumericCode _231 = Iso31661NumericCode.of("231");
   public static final Iso31661NumericCode _232 = Iso31661NumericCode.of("232");
   public static final Iso31661NumericCode _233 = Iso31661NumericCode.of("233");
   public static final Iso31661NumericCode _234 = Iso31661NumericCode.of("234");
   public static final Iso31661NumericCode _238 = Iso31661NumericCode.of("238");
   public static final Iso31661NumericCode _239 = Iso31661NumericCode.of("239");
   public static final Iso31661NumericCode _242 = Iso31661NumericCode.of("242");
   public static final Iso31661NumericCode _246 = Iso31661NumericCode.of("246");
   public static final Iso31661NumericCode _248 = Iso31661NumericCode.of("248");
   public static final Iso31661NumericCode _250 = Iso31661NumericCode.of("250");
   public static final Iso31661NumericCode _254 = Iso31661NumericCode.of("254");
   public static final Iso31661NumericCode _258 = Iso31661NumericCode.of("258");
   public static final Iso31661NumericCode _260 = Iso31661NumericCode.of("260");
   public static final Iso31661NumericCode _262 = Iso31661NumericCode.of("262");
   public static final Iso31661NumericCode _266 = Iso31661NumericCode.of("266");
   public static final Iso31661NumericCode _268 = Iso31661NumericCode.of("268");
   public static final Iso31661NumericCode _270 = Iso31661NumericCode.of("270");
   public static final Iso31661NumericCode _275 = Iso31661NumericCode.of("275");
   public static final Iso31661NumericCode _276 = Iso31661NumericCode.of("276");
   public static final Iso31661NumericCode _288 = Iso31661NumericCode.of("288");
   public static final Iso31661NumericCode _292 = Iso31661NumericCode.of("292");
   public static final Iso31661NumericCode _296 = Iso31661NumericCode.of("296");
   public static final Iso31661NumericCode _300 = Iso31661NumericCode.of("300");
   public static final Iso31661NumericCode _304 = Iso31661NumericCode.of("304");
   public static final Iso31661NumericCode _308 = Iso31661NumericCode.of("308");
   public static final Iso31661NumericCode _312 = Iso31661NumericCode.of("312");
   public static final Iso31661NumericCode _316 = Iso31661NumericCode.of("316");
   public static final Iso31661NumericCode _320 = Iso31661NumericCode.of("320");
   public static final Iso31661NumericCode _324 = Iso31661NumericCode.of("324");
   public static final Iso31661NumericCode _328 = Iso31661NumericCode.of("328");
   public static final Iso31661NumericCode _332 = Iso31661NumericCode.of("332");
   public static final Iso31661NumericCode _334 = Iso31661NumericCode.of("334");
   public static final Iso31661NumericCode _336 = Iso31661NumericCode.of("336");
   public static final Iso31661NumericCode _340 = Iso31661NumericCode.of("340");
   public static final Iso31661NumericCode _344 = Iso31661NumericCode.of("344");
   public static final Iso31661NumericCode _348 = Iso31661NumericCode.of("348");
   public static final Iso31661NumericCode _352 = Iso31661NumericCode.of("352");
   public static final Iso31661NumericCode _356 = Iso31661NumericCode.of("356");
   public static final Iso31661NumericCode _360 = Iso31661NumericCode.of("360");
   public static final Iso31661NumericCode _364 = Iso31661NumericCode.of("364");
   public static final Iso31661NumericCode _368 = Iso31661NumericCode.of("368");
   public static final Iso31661NumericCode _372 = Iso31661NumericCode.of("372");
   public static final Iso31661NumericCode _376 = Iso31661NumericCode.of("376");
   public static final Iso31661NumericCode _380 = Iso31661NumericCode.of("380");
   public static final Iso31661NumericCode _384 = Iso31661NumericCode.of("384");
   public static final Iso31661NumericCode _388 = Iso31661NumericCode.of("388");
   public static final Iso31661NumericCode _392 = Iso31661NumericCode.of("392");
   public static final Iso31661NumericCode _398 = Iso31661NumericCode.of("398");
   public static final Iso31661NumericCode _400 = Iso31661NumericCode.of("400");
   public static final Iso31661NumericCode _404 = Iso31661NumericCode.of("404");
   public static final Iso31661NumericCode _408 = Iso31661NumericCode.of("408");
   public static final Iso31661NumericCode _410 = Iso31661NumericCode.of("410");
   public static final Iso31661NumericCode _414 = Iso31661NumericCode.of("414");
   public static final Iso31661NumericCode _417 = Iso31661NumericCode.of("417");
   public static final Iso31661NumericCode _418 = Iso31661NumericCode.of("418");
   public static final Iso31661NumericCode _422 = Iso31661NumericCode.of("422");
   public static final Iso31661NumericCode _426 = Iso31661NumericCode.of("426");
   public static final Iso31661NumericCode _428 = Iso31661NumericCode.of("428");
   public static final Iso31661NumericCode _430 = Iso31661NumericCode.of("430");
   public static final Iso31661NumericCode _434 = Iso31661NumericCode.of("434");
   public static final Iso31661NumericCode _438 = Iso31661NumericCode.of("438");
   public static final Iso31661NumericCode _440 = Iso31661NumericCode.of("440");
   public static final Iso31661NumericCode _442 = Iso31661NumericCode.of("442");
   public static final Iso31661NumericCode _446 = Iso31661NumericCode.of("446");
   public static final Iso31661NumericCode _450 = Iso31661NumericCode.of("450");
   public static final Iso31661NumericCode _454 = Iso31661NumericCode.of("454");
   public static final Iso31661NumericCode _458 = Iso31661NumericCode.of("458");
   public static final Iso31661NumericCode _462 = Iso31661NumericCode.of("462");
   public static final Iso31661NumericCode _466 = Iso31661NumericCode.of("466");
   public static final Iso31661NumericCode _470 = Iso31661NumericCode.of("470");
   public static final Iso31661NumericCode _474 = Iso31661NumericCode.of("474");
   public static final Iso31661NumericCode _478 = Iso31661NumericCode.of("478");
   public static final Iso31661NumericCode _480 = Iso31661NumericCode.of("480");
   public static final Iso31661NumericCode _484 = Iso31661NumericCode.of("484");
   public static final Iso31661NumericCode _492 = Iso31661NumericCode.of("492");
   public static final Iso31661NumericCode _496 = Iso31661NumericCode.of("496");
   public static final Iso31661NumericCode _498 = Iso31661NumericCode.of("498");
   public static final Iso31661NumericCode _499 = Iso31661NumericCode.of("499");
   public static final Iso31661NumericCode _500 = Iso31661NumericCode.of("500");
   public static final Iso31661NumericCode _504 = Iso31661NumericCode.of("504");
   public static final Iso31661NumericCode _508 = Iso31661NumericCode.of("508");
   public static final Iso31661NumericCode _512 = Iso31661NumericCode.of("512");
   public static final Iso31661NumericCode _516 = Iso31661NumericCode.of("516");
   public static final Iso31661NumericCode _520 = Iso31661NumericCode.of("520");
   public static final Iso31661NumericCode _524 = Iso31661NumericCode.of("524");
   public static final Iso31661NumericCode _528 = Iso31661NumericCode.of("528");
   public static final Iso31661NumericCode _531 = Iso31661NumericCode.of("531");
   public static final Iso31661NumericCode _533 = Iso31661NumericCode.of("533");
   public static final Iso31661NumericCode _534 = Iso31661NumericCode.of("534");
   public static final Iso31661NumericCode _535 = Iso31661NumericCode.of("535");
   public static final Iso31661NumericCode _540 = Iso31661NumericCode.of("540");
   public static final Iso31661NumericCode _548 = Iso31661NumericCode.of("548");
   public static final Iso31661NumericCode _554 = Iso31661NumericCode.of("554");
   public static final Iso31661NumericCode _558 = Iso31661NumericCode.of("558");
   public static final Iso31661NumericCode _562 = Iso31661NumericCode.of("562");
   public static final Iso31661NumericCode _566 = Iso31661NumericCode.of("566");
   public static final Iso31661NumericCode _570 = Iso31661NumericCode.of("570");
   public static final Iso31661NumericCode _574 = Iso31661NumericCode.of("574");
   public static final Iso31661NumericCode _578 = Iso31661NumericCode.of("578");
   public static final Iso31661NumericCode _580 = Iso31661NumericCode.of("580");
   public static final Iso31661NumericCode _581 = Iso31661NumericCode.of("581");
   public static final Iso31661NumericCode _583 = Iso31661NumericCode.of("583");
   public static final Iso31661NumericCode _584 = Iso31661NumericCode.of("584");
   public static final Iso31661NumericCode _585 = Iso31661NumericCode.of("585");
   public static final Iso31661NumericCode _586 = Iso31661NumericCode.of("586");
   public static final Iso31661NumericCode _591 = Iso31661NumericCode.of("591");
   public static final Iso31661NumericCode _598 = Iso31661NumericCode.of("598");
   public static final Iso31661NumericCode _600 = Iso31661NumericCode.of("600");
   public static final Iso31661NumericCode _604 = Iso31661NumericCode.of("604");
   public static final Iso31661NumericCode _608 = Iso31661NumericCode.of("608");
   public static final Iso31661NumericCode _612 = Iso31661NumericCode.of("612");
   public static final Iso31661NumericCode _616 = Iso31661NumericCode.of("616");
   public static final Iso31661NumericCode _620 = Iso31661NumericCode.of("620");
   public static final Iso31661NumericCode _624 = Iso31661NumericCode.of("624");
   public static final Iso31661NumericCode _626 = Iso31661NumericCode.of("626");
   public static final Iso31661NumericCode _630 = Iso31661NumericCode.of("630");
   public static final Iso31661NumericCode _634 = Iso31661NumericCode.of("634");
   public static final Iso31661NumericCode _638 = Iso31661NumericCode.of("638");
   public static final Iso31661NumericCode _642 = Iso31661NumericCode.of("642");
   public static final Iso31661NumericCode _643 = Iso31661NumericCode.of("643");
   public static final Iso31661NumericCode _646 = Iso31661NumericCode.of("646");
   public static final Iso31661NumericCode _652 = Iso31661NumericCode.of("652");
   public static final Iso31661NumericCode _654 = Iso31661NumericCode.of("654");
   public static final Iso31661NumericCode _659 = Iso31661NumericCode.of("659");
   public static final Iso31661NumericCode _660 = Iso31661NumericCode.of("660");
   public static final Iso31661NumericCode _662 = Iso31661NumericCode.of("662");
   public static final Iso31661NumericCode _663 = Iso31661NumericCode.of("663");
   public static final Iso31661NumericCode _666 = Iso31661NumericCode.of("666");
   public static final Iso31661NumericCode _670 = Iso31661NumericCode.of("670");
   public static final Iso31661NumericCode _674 = Iso31661NumericCode.of("674");
   public static final Iso31661NumericCode _678 = Iso31661NumericCode.of("678");
   public static final Iso31661NumericCode _682 = Iso31661NumericCode.of("682");
   public static final Iso31661NumericCode _686 = Iso31661NumericCode.of("686");
   public static final Iso31661NumericCode _688 = Iso31661NumericCode.of("688");
   public static final Iso31661NumericCode _690 = Iso31661NumericCode.of("690");
   public static final Iso31661NumericCode _694 = Iso31661NumericCode.of("694");
   public static final Iso31661NumericCode _702 = Iso31661NumericCode.of("702");
   public static final Iso31661NumericCode _703 = Iso31661NumericCode.of("703");
   public static final Iso31661NumericCode _704 = Iso31661NumericCode.of("704");
   public static final Iso31661NumericCode _705 = Iso31661NumericCode.of("705");
   public static final Iso31661NumericCode _706 = Iso31661NumericCode.of("706");
   public static final Iso31661NumericCode _710 = Iso31661NumericCode.of("710");
   public static final Iso31661NumericCode _716 = Iso31661NumericCode.of("716");
   public static final Iso31661NumericCode _724 = Iso31661NumericCode.of("724");
   public static final Iso31661NumericCode _728 = Iso31661NumericCode.of("728");
   public static final Iso31661NumericCode _729 = Iso31661NumericCode.of("729");
   public static final Iso31661NumericCode _732 = Iso31661NumericCode.of("732");
   public static final Iso31661NumericCode _740 = Iso31661NumericCode.of("740");
   public static final Iso31661NumericCode _744 = Iso31661NumericCode.of("744");
   public static final Iso31661NumericCode _748 = Iso31661NumericCode.of("748");
   public static final Iso31661NumericCode _752 = Iso31661NumericCode.of("752");
   public static final Iso31661NumericCode _756 = Iso31661NumericCode.of("756");
   public static final Iso31661NumericCode _760 = Iso31661NumericCode.of("760");
   public static final Iso31661NumericCode _762 = Iso31661NumericCode.of("762");
   public static final Iso31661NumericCode _764 = Iso31661NumericCode.of("764");
   public static final Iso31661NumericCode _768 = Iso31661NumericCode.of("768");
   public static final Iso31661NumericCode _772 = Iso31661NumericCode.of("772");
   public static final Iso31661NumericCode _776 = Iso31661NumericCode.of("776");
   public static final Iso31661NumericCode _780 = Iso31661NumericCode.of("780");
   public static final Iso31661NumericCode _784 = Iso31661NumericCode.of("784");
   public static final Iso31661NumericCode _788 = Iso31661NumericCode.of("788");
   public static final Iso31661NumericCode _792 = Iso31661NumericCode.of("792");
   public static final Iso31661NumericCode _795 = Iso31661NumericCode.of("795");
   public static final Iso31661NumericCode _796 = Iso31661NumericCode.of("796");
   public static final Iso31661NumericCode _798 = Iso31661NumericCode.of("798");
   public static final Iso31661NumericCode _800 = Iso31661NumericCode.of("800");
   public static final Iso31661NumericCode _804 = Iso31661NumericCode.of("804");
   public static final Iso31661NumericCode _807 = Iso31661NumericCode.of("807");
   public static final Iso31661NumericCode _818 = Iso31661NumericCode.of("818");
   public static final Iso31661NumericCode _826 = Iso31661NumericCode.of("826");
   public static final Iso31661NumericCode _831 = Iso31661NumericCode.of("831");
   public static final Iso31661NumericCode _832 = Iso31661NumericCode.of("832");
   public static final Iso31661NumericCode _833 = Iso31661NumericCode.of("833");
   public static final Iso31661NumericCode _834 = Iso31661NumericCode.of("834");
   public static final Iso31661NumericCode _840 = Iso31661NumericCode.of("840");
   public static final Iso31661NumericCode _850 = Iso31661NumericCode.of("850");
   public static final Iso31661NumericCode _854 = Iso31661NumericCode.of("854");
   public static final Iso31661NumericCode _858 = Iso31661NumericCode.of("858");
   public static final Iso31661NumericCode _860 = Iso31661NumericCode.of("860");
   public static final Iso31661NumericCode _862 = Iso31661NumericCode.of("862");
   public static final Iso31661NumericCode _876 = Iso31661NumericCode.of("876");
   public static final Iso31661NumericCode _882 = Iso31661NumericCode.of("882");
   public static final Iso31661NumericCode _887 = Iso31661NumericCode.of("887");
   public static final Iso31661NumericCode _894 = Iso31661NumericCode.of("894");

}


