/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.iso31663;

import java.time.LocalDate;
import java.util.Optional;

import com.tinubu.commons.referential.AbstractEntry;
import com.tinubu.commons.referential.Entry;
import com.tinubu.commons.referential.EntryData;
import com.tinubu.commons.referential.TranslatableName;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2Code;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha3;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha3Code;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Numeric;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661NumericCode;
import com.tinubu.commons.referential.referentials.iso31663.Iso31663Alpha4.Iso31663Alpha4Data;

/**
 * Represents a ISO 3166-3 alpha-4 country.
 * Each country is associated with {@link Iso31663Alpha4Data}.
 */
public class Iso31663Alpha4
      extends AbstractEntry<Iso31663Alpha4Referential, Iso31663Alpha4, Iso31663Alpha4Data>
      implements Entry<Iso31663Alpha4> {

   Iso31663Alpha4(Iso31663Alpha4Referential referential, Iso31663Alpha4Data entryData) {
      super(referential, entryData);
   }

   @Override
   public Iso31663Alpha4Code entryCode() {
      return entryData.iso31663Alpha4();
   }

   @Override
   public TranslatableName entryName() {
      return entryData.name();
   }

   /**
    * Defines ISO 3166-3 alpha-4 data.
    */
   public interface Iso31663Alpha4Data extends EntryData {

      /** Entry code. */
      Iso31663Alpha4Code iso31663Alpha4();

      /**
       * ISO 3166-1 alpha-2 code historically used for this country. This code is not part of the current
       * {@link Iso31661Alpha2} referential anymore.
       */
      Iso31661Alpha2Code iso31661Alpha2();

      /**
       * ISO 3166-1 alpha-3 code historically used for this country. This code is not part of the current
       * {@link Iso31661Alpha3} referential anymore.
       */
      Iso31661Alpha3Code iso31661Alpha3();

      /**
       * Optional ISO 3166-1 numeric code historically used for this country. This code is not part of the
       * current {@link Iso31661Numeric} referential anymore.
       */
      Optional<Iso31661NumericCode> iso31661Numeric();

      /** Name translations for this entry. */
      TranslatableName name();

      /** Historical entry withdrawal date. */
      LocalDate withdrawalDate();

   }

}
