/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.iso31661;

import com.tinubu.commons.referential.AbstractReferential;
import com.tinubu.commons.referential.EntryDataLoader;
import com.tinubu.commons.referential.ReferentialCode;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha3.Iso31661Alpha3Data;

/**
 * ISO 3166-1 alpha-3 country referential.
 */
public class Iso31661Alpha3Referential extends AbstractReferential<Iso31661Alpha3, Iso31661Alpha3Data> {

   public static final ReferentialCode<Iso31661Alpha3, Iso31661Alpha3Referential> DEFAULT_REFERENTIAL_CODE =
         ReferentialCode.of("ISO_31661_ALPHA3");

   private static final String DISPLAY_NAME = "ISO 3166-1 alpha-3";

   private Iso31661Alpha3Referential(ReferentialCode<Iso31661Alpha3, Iso31661Alpha3Referential> referentialCode,
                                     EntryDataLoader<Iso31661Alpha3Data> entryDataLoader) {
      super(referentialCode, entryDataLoader);
   }

   @Override
   public String displayName() {
      return DISPLAY_NAME;
   }

   @Override
   public Iso31661Alpha3 loadEntry(Iso31661Alpha3Data entryData) {
      return new Iso31661Alpha3(this, entryData);
   }

   public static class Iso31661Alpha3ReferentialBuilder {
      private ReferentialCode<Iso31661Alpha3, Iso31661Alpha3Referential> referentialCode =
            DEFAULT_REFERENTIAL_CODE;
      private EntryDataLoader<Iso31661Alpha3Data> entryDataLoader;

      public Iso31661Alpha3ReferentialBuilder referentialCode(ReferentialCode<Iso31661Alpha3, Iso31661Alpha3Referential> referentialCode) {
         this.referentialCode = referentialCode;
         return this;
      }

      public Iso31661Alpha3ReferentialBuilder referentialLoader(EntryDataLoader<Iso31661Alpha3Data> entryDataLoader) {
         this.entryDataLoader = entryDataLoader;
         return this;
      }

      public ReferentialCode<Iso31661Alpha3, Iso31661Alpha3Referential> referentialCode() {
         return referentialCode;
      }

      public Class<Iso31661Alpha3Referential> referentialClass() {
         return Iso31661Alpha3Referential.class;
      }

      public Iso31661Alpha3Referential build() {
         return new Iso31661Alpha3Referential(referentialCode, entryDataLoader);
      }
   }

}
