/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.referential.EntryCode;
import com.tinubu.commons.referential.Referentials;
import com.tinubu.commons.referential.UnknownEntryException;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2Code;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2Referential;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha3;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha3Code;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha3Referential;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Numeric;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661NumericCode;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661NumericReferential;
import com.tinubu.commons.referential.referentials.iso31662.Iso31662;
import com.tinubu.commons.referential.referentials.iso31662.Iso31662Code;
import com.tinubu.commons.referential.referentials.iso31662.Iso31662Referential;
import com.tinubu.commons.referential.referentials.iso31663.Iso31663Alpha4;
import com.tinubu.commons.referential.referentials.iso31663.Iso31663Alpha4Code;
import com.tinubu.commons.referential.referentials.iso31663.Iso31663Alpha4Referential;
import com.tinubu.commons.referential.referentials.iso6391.Iso6391Alpha2;
import com.tinubu.commons.referential.referentials.iso6391.Iso6391Alpha2Code;
import com.tinubu.commons.referential.referentials.iso6391.Iso6391Alpha2Referential;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Referential;

/**
 * Specialized fast accessors for default ISO referentials.
 */
public class IsoReferentials {

   /**
    * ISO 3166-1 alpha-2 default referential accessor.
    *
    * @return {@link Iso31661Alpha2Referential} referential instance
    */
   public static Iso31661Alpha2Referential iso31661Alpha2Referential() {
      return Referentials.referential(Iso31661Alpha2Referential.DEFAULT_REFERENTIAL_CODE);
   }

   /**
    * ISO 3166-1 alpha-3 default referential accessor.
    *
    * @return {@link Iso31661Alpha3Referential} referential instance
    */
   public static Iso31661Alpha3Referential iso31661Alpha3Referential() {
      return Referentials.referential(Iso31661Alpha3Referential.DEFAULT_REFERENTIAL_CODE);
   }

   /**
    * ISO 3166-1 numeric default referential accessor.
    *
    * @return {@link Iso31661NumericReferential} referential instance
    */
   public static Iso31661NumericReferential iso31661NumericReferential() {
      return Referentials.referential(Iso31661NumericReferential.DEFAULT_REFERENTIAL_CODE);
   }

   /**
    * ISO 3166-2 default referential accessor.
    *
    * @return {@link Iso31662Referential} referential instance
    */
   public static Iso31662Referential iso31662Referential() {
      return Referentials.referential(Iso31662Referential.DEFAULT_REFERENTIAL_CODE);
   }

   /**
    * ISO 3166-3 alpha-4 default referential accessor.
    *
    * @return {@link Iso31663Alpha4Referential} referential instance
    */
   public static Iso31663Alpha4Referential iso31663Alpha4Referential() {
      return Referentials.referential(Iso31663Alpha4Referential.DEFAULT_REFERENTIAL_CODE);
   }

   /**
    * ISO 639-1 alpha-2 default referential accessor.
    *
    * @return {@link Iso6391Alpha2Referential} referential instance
    */
   public static Iso6391Alpha2Referential iso6391Alpha2Referential() {
      return Referentials.referential(Iso6391Alpha2Referential.DEFAULT_REFERENTIAL_CODE);
   }

   /**
    * ISO 639-2 alpha-3 default referential accessor.
    *
    * @return {@link Iso6392Alpha3Referential} referential instance
    */
   public static Iso6392Alpha3Referential iso6392Alpha3Referential() {
      return Referentials.referential(Iso6392Alpha3Referential.DEFAULT_REFERENTIAL_CODE);
   }

   /**
    * Checked ISO 3166-1 alpha-2 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso31661Alpha2} country
    *
    * @throws UnknownEntryException if country is unknown in referential
    */
   public static Iso31661Alpha2 iso31661Alpha2(EntryCode countryCode) {
      return iso31661Alpha2Referential().entry(countryCode);
   }

   /**
    * Checked ISO 3166-1 alpha-2 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso31661Alpha2} country
    *
    * @throws UnknownEntryException if country is unknown in referential
    */
   public static Iso31661Alpha2 iso31661Alpha2(String countryCode) {
      return iso31661Alpha2(Iso31661Alpha2Code.of(countryCode));
   }

   /**
    * Optional ISO 3166-1 alpha-2 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso31661Alpha2} country or {@link Optional#empty} if country is unknown in referential
    */
   public static Optional<Iso31661Alpha2> optionalIso31661Alpha2(EntryCode countryCode) {
      return iso31661Alpha2Referential().optionalEntry(countryCode);
   }

   /**
    * Optional ISO 3166-1 alpha-2 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso31661Alpha2} country or {@link Optional#empty} if country is unknown in referential
    */
   public static Optional<Iso31661Alpha2> optionalIso31661Alpha2(String countryCode) {
      return optionalIso31661Alpha2(Iso31661Alpha2Code.of(countryCode));
   }

   /**
    * Checked ISO 3166-1 alpha-3 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso31661Alpha3} country
    *
    * @throws UnknownEntryException if country is unknown in referential
    */
   public static Iso31661Alpha3 iso31661Alpha3(EntryCode countryCode) {
      return iso31661Alpha3Referential().entry(countryCode);
   }

   /**
    * Checked ISO 3166-1 alpha-3 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso31661Alpha3} country
    *
    * @throws UnknownEntryException if country is unknown in referential
    */
   public static Iso31661Alpha3 iso31661Alpha3(String countryCode) {
      return iso31661Alpha3(Iso31661Alpha3Code.of(countryCode));
   }

   /**
    * Optional ISO 3166-1 alpha-3 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso31661Alpha3} country or {@link Optional#empty} if country is unknown in referential
    */
   public static Optional<Iso31661Alpha3> optionalIso31661Alpha3(EntryCode countryCode) {
      return iso31661Alpha3Referential().optionalEntry(countryCode);
   }

   /**
    * Optional ISO 3166-1 alpha-3 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso31661Alpha3} country or {@link Optional#empty} if country is unknown in referential
    */
   public static Optional<Iso31661Alpha3> optionalIso31661Alpha3(String countryCode) {
      return optionalIso31661Alpha3(Iso31661Alpha3Code.of(countryCode));
   }

   /**
    * Checked ISO 3166-1 numeric country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso31661Numeric} country
    *
    * @throws UnknownEntryException if country is unknown in referential
    */
   public static Iso31661Numeric iso31661Numeric(EntryCode countryCode) {
      return iso31661NumericReferential().entry(countryCode);
   }

   /**
    * Checked ISO 3166-1 numeric country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso31661Numeric} country
    *
    * @throws UnknownEntryException if country is unknown in referential
    */
   public static Iso31661Numeric iso31661Numeric(String countryCode) {
      return iso31661Numeric(Iso31661NumericCode.of(countryCode));
   }

   /**
    * Optional ISO 3166-1 numeric country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso31661Numeric} country or {@link Optional#empty} if country is unknown in referential
    */
   public static Optional<Iso31661Numeric> optionalIso31661Numeric(EntryCode countryCode) {
      return iso31661NumericReferential().optionalEntry(countryCode);
   }

   /**
    * Optional ISO 3166-1 numeric country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso31661Numeric} country or {@link Optional#empty} if country is unknown in referential
    */
   public static Optional<Iso31661Numeric> optionalIso31661Numeric(String countryCode) {
      return optionalIso31661Numeric(Iso31661NumericCode.of(countryCode));
   }

   /**
    * Checked ISO 3166-2 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso31662} country
    *
    * @throws UnknownEntryException if country is unknown in referential
    */
   public static Iso31662 iso31662(EntryCode countryCode) {
      return iso31662Referential().entry(countryCode);
   }

   /**
    * Checked ISO 3166-2 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso31662} country
    *
    * @throws UnknownEntryException if country is unknown in referential
    */
   public static Iso31662 iso31662(String countryCode) {
      return iso31662(Iso31662Code.of(countryCode));
   }

   /**
    * Optional ISO 3166-2 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso31662} country or {@link Optional#empty} if country is unknown in referential
    */
   public static Optional<Iso31662> optionalIso31662(EntryCode countryCode) {
      return iso31662Referential().optionalEntry(countryCode);
   }

   /**
    * Optional ISO 3166-2 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso31662} country or {@link Optional#empty} if country is unknown in referential
    */
   public static Optional<Iso31662> optionalIso31662(String countryCode) {
      return optionalIso31662(Iso31662Code.of(countryCode));
   }

   /**
    * ISO 3166-2 subdivisions indexed by country.
    *
    * @param filter entry filter
    *
    * @return {@link Iso31662Referential} referential map indexed by country
    */
   public static Map<Iso31661Alpha2Code, List<Iso31662>> iso31662SubdivisionsByCountry(Predicate<Iso31662> filter) {
      notNull(filter, "filter");

      return iso31662Referential()
            .entries()
            .stream()
            .filter(filter)
            .collect(groupingBy(subdivision -> subdivision.country().entryCode(),
                                mapping(identity(), toList())));
   }

   /**
    * ISO 3166-2 subdivisions indexed by country.
    *
    * @return {@link Iso31662Referential} referential map indexed by country
    */
   public static Map<Iso31661Alpha2Code, List<Iso31662>> iso31662SubdivisionsByCountry() {
      return iso31662SubdivisionsByCountry(__ -> true);
   }

   /**
    * Checked ISO 3166-3 alpha-4 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso31663Alpha4} country
    *
    * @throws UnknownEntryException if country is unknown in referential
    */
   public static Iso31663Alpha4 iso31663Alpha4(EntryCode countryCode) {
      return iso31663Alpha4Referential().entry(countryCode);
   }

   /**
    * Checked ISO 3166-3 alpha-4 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso31663Alpha4} country
    *
    * @throws UnknownEntryException if country is unknown in referential
    */
   public static Iso31663Alpha4 iso31663Alpha4(String countryCode) {
      return iso31663Alpha4(Iso31663Alpha4Code.of(countryCode));
   }

   /**
    * Optional ISO 3166-3 alpha-4 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso31663Alpha4} country or {@link Optional#empty} if country is unknown in referential
    */
   public static Optional<Iso31663Alpha4> optionalIso31663Alpha4(EntryCode countryCode) {
      return iso31663Alpha4Referential().optionalEntry(countryCode);
   }

   /**
    * Optional ISO 3166-3 alpha-4 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso31663Alpha4} country or {@link Optional#empty} if country is unknown in referential
    */
   public static Optional<Iso31663Alpha4> optionalIso31663Alpha4(String countryCode) {
      return optionalIso31663Alpha4(Iso31663Alpha4Code.of(countryCode));
   }

   /**
    * Checked ISO 639-1 alpha-2 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso6391Alpha2} country
    *
    * @throws UnknownEntryException if country is unknown in referential
    */
   public static Iso6391Alpha2 iso6391Alpha2(EntryCode countryCode) {
      return iso6391Alpha2Referential().entry(countryCode);
   }

   /**
    * Checked ISO 639-1 alpha-2 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso6391Alpha2} country
    *
    * @throws UnknownEntryException if country is unknown in referential
    */
   public static Iso6391Alpha2 iso6391Alpha2(String countryCode) {
      return iso6391Alpha2(Iso6391Alpha2Code.of(countryCode));
   }

   /**
    * Optional ISO 639-1 alpha-2 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso6391Alpha2} country or {@link Optional#empty} if country is unknown in referential
    */
   public static Optional<Iso6391Alpha2> optionalIso6391Alpha2(EntryCode countryCode) {
      return iso6391Alpha2Referential().optionalEntry(countryCode);
   }

   /**
    * Optional ISO 639-1 alpha-2 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso6391Alpha2} country or {@link Optional#empty} if country is unknown in referential
    */
   public static Optional<Iso6391Alpha2> optionalIso6391Alpha2(String countryCode) {
      return optionalIso6391Alpha2(Iso6391Alpha2Code.of(countryCode));
   }

   /**
    * Checked ISO 639-2 alpha-3 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso6392Alpha3} country
    *
    * @throws UnknownEntryException if country is unknown in referential
    */
   public static Iso6392Alpha3 iso6392Alpha3(EntryCode countryCode) {
      return iso6392Alpha3Referential().entry(countryCode);
   }

   /**
    * Checked ISO 639-2 alpha-3 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso6392Alpha3} country
    *
    * @throws UnknownEntryException if country is unknown in referential
    */
   public static Iso6392Alpha3 iso6392Alpha3(String countryCode) {
      return iso6392Alpha3(Iso6392Alpha3Code.of(countryCode));
   }

   /**
    * Optional 639-2 alpha-3 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso6392Alpha3} country or {@link Optional#empty} if country is unknown in referential
    */
   public static Optional<Iso6392Alpha3> optionalIso6392Alpha3(EntryCode countryCode) {
      return iso6392Alpha3Referential().optionalEntry(countryCode);
   }

   /**
    * Optional 639-2 alpha-3 country accessor.
    *
    * @param countryCode country code
    *
    * @return {@link Iso6392Alpha3} country or {@link Optional#empty} if country is unknown in referential
    */
   public static Optional<Iso6392Alpha3> optionalIso6392Alpha3(String countryCode) {
      return optionalIso6392Alpha3(Iso6392Alpha3Code.of(countryCode));
   }

   /**
    * Utility class to compute ISO user-assigned code ranges.
    */
   public static final class IsoUserAssignedCodes {

      private IsoUserAssignedCodes() {}

      public static final List<Pair<Iso31661Alpha2Code, Iso31661Alpha2Code>>
            ISO_31661_ALPHA2_USER_ASSIGNED_RANGES =
            Arrays.asList(Pair.of(Iso31661Alpha2Code.of("AA"), Iso31661Alpha2Code.of("AA")),
                          Pair.of(Iso31661Alpha2Code.of("QM"), Iso31661Alpha2Code.of("QZ")),
                          Pair.of(Iso31661Alpha2Code.of("XA"), Iso31661Alpha2Code.of("XZ")),
                          Pair.of(Iso31661Alpha2Code.of("ZZ"), Iso31661Alpha2Code.of("ZZ")));
      public static final List<Pair<Iso31661Alpha3Code, Iso31661Alpha3Code>>
            ISO_31661_ALPHA3_USER_ASSIGNED_RANGES =
            Arrays.asList(Pair.of(Iso31661Alpha3Code.of("AAA"), Iso31661Alpha3Code.of("AAZ")),
                          Pair.of(Iso31661Alpha3Code.of("QMA"), Iso31661Alpha3Code.of("QZZ")),
                          Pair.of(Iso31661Alpha3Code.of("XAA"), Iso31661Alpha3Code.of("XZZ")),
                          Pair.of(Iso31661Alpha3Code.of("ZZA"), Iso31661Alpha3Code.of("ZZZ")));

      public static final List<Pair<Iso6392Alpha3Code, Iso6392Alpha3Code>> ISO_6392_ALPHA3_RESERVED_CODES =
            Arrays.asList(Pair.of(Iso6392Alpha3Code.of("qaa"), Iso6392Alpha3Code.of("qtz")));

      public static List<String> alpha2Range(String start, String end, char minRange, char maxRange) {
         if (start.length() != end.length() || start.length() != 2) {
            throw new IllegalArgumentException("start/end code lengths mismatch");
         }
         if (start.chars().anyMatch(c -> c < minRange || c > maxRange)) {
            throw new IllegalArgumentException("start must only contain ranged characters");
         }
         if (end.chars().anyMatch(c -> c < minRange || c > maxRange)) {
            throw new IllegalArgumentException("end must only contain ranged characters");
         }

         List<String> countryCodes = new ArrayList<>();

         for (char c0 = start.charAt(0); c0 <= end.charAt(0); c0++) {
            for (char c1 = (c0 == start.charAt(0) ? start.charAt(1) : minRange);
                 c1 <= (c0 == end.charAt(0) ? end.charAt(1) : maxRange);
                 c1++) {
               countryCodes.add(new String(new char[] { c0, c1 }));
            }
         }

         return countryCodes;
      }

      public static List<String> alpha3Range(String start, String end, char minRange, char maxRange) {
         if (start.length() != end.length() || start.length() != 3) {
            throw new IllegalArgumentException("start/end code lengths mismatch");
         }
         if (start.chars().anyMatch(c -> c < minRange || c > maxRange)) {
            throw new IllegalArgumentException("start must only contain ranged characters");
         }
         if (end.chars().anyMatch(c -> c < minRange || c > maxRange)) {
            throw new IllegalArgumentException("end must only contain ranged characters");
         }

         List<String> countryCodes = new ArrayList<>();

         for (char c0 = start.charAt(0); c0 <= end.charAt(0); c0++) {
            for (char c1 = (c0 == start.charAt(0) ? start.charAt(1) : minRange);
                 c1 <= (c0 == end.charAt(0) ? end.charAt(1) : maxRange);
                 c1++) {
               for (char c2 = (c1 == start.charAt(1) ? start.charAt(2) : minRange);
                    c2 <= (c1 == end.charAt(1) ? end.charAt(2) : maxRange);
                    c2++) {
                  countryCodes.add(new String(new char[] { c0, c1, c2 }));
               }
            }
         }

         return countryCodes;
      }

   }
}
