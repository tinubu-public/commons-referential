/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Collections.emptyMap;

import java.util.Map;
import java.util.Optional;

import com.tinubu.commons.referential.DefaultEntryCodeMapping.DefaultEntryCodeMappingBuilder;

/**
 * Entry data.
 * <p>
 * Data contents depends on each referential.
 */
public interface EntryData {

   /**
    * Cross referential entry code mapping.
    *
    * @return cross referential entry code mapping
    */
   default EntryCodeMapping entryCodeMapping() {
      return new DefaultEntryCodeMappingBuilder().build();
   }

   /**
    * Mapped entry code for specified {@link ReferentialCode}.
    *
    * @param referentialCode referential code
    *
    * @return mapped entry code mappings for specified referential code or {@link Optional#empty}
    *       if there's no matching entry
    */
   default Optional<EntryCode> entryCode(ReferentialCode<?, ?> referentialCode) {
      notNull(referentialCode, "referentialCode");

      return entryCodeMapping().mapping(referentialCode);
   }

   /**
    * Custom attributes for entry, indexed by attribute name.
    *
    * @return entry custom attributes, never {@code null}
    */
   default Map<String, Object> attributes() {
      return emptyMap();
   }

   /**
    * Flag useful to mark entries that are active. This flag is typically used in custom referentials to
    * differentiate legacy entries from up-to-date entries to facilitate migration. Official referentials
    * should always set this value to {@code true}.
    */
   default boolean active() {
      return true;
   }
}
