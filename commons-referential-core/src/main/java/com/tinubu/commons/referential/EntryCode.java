/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential;

import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * {@link Entry} identification code.
 */
public class EntryCode implements Comparable<EntryCode> {

   private final String entryCode;

   protected EntryCode(String entryCode) {
      this.entryCode = validateEntryCode(entryCode);
   }

   /**
    * Overridable entry code format validator. Entry code parameter has been pre-validated and is not blank.
    *
    * @param entryCode entry code to validate
    *
    * @return entry code
    */
   protected boolean validEntryCodeFormat(String entryCode) {
      return true;
   }

   public static EntryCode of(String entryCode) {
      return new EntryCode(entryCode);
   }

   public String entryCode() {
      return entryCode;
   }

   private String validateEntryCode(String entryCode) {
      notBlank(entryCode, "entryCode");
      if (!validEntryCodeFormat(entryCode)) {
         throw new IllegalArgumentException(String.format("Invalid 'entryCode' code format : %s", entryCode));
      }
      return entryCode;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || (getClass() != o.getClass()
                        && EntryCode.class != o.getClass()
                        && getClass() != EntryCode.class)) return false;
      EntryCode entryCode1 = (EntryCode) o;
      return entryCode.equals(entryCode1.entryCode);
   }

   @Override
   public int hashCode() {
      return Objects.hash(entryCode);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", getClass().getSimpleName() + "[", "]")
            .add("entryCode='" + entryCode + "'")
            .toString();
   }

   @Override
   public int compareTo(EntryCode entryCode) {
      notNull(this.entryCode, "entryCode");

      return this.entryCode.compareTo(entryCode.entryCode);
   }
}
