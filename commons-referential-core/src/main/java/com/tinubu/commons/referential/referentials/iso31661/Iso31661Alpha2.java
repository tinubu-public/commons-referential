/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.iso31661;

import com.tinubu.commons.referential.AbstractEntry;
import com.tinubu.commons.referential.Entry;
import com.tinubu.commons.referential.EntryData;
import com.tinubu.commons.referential.TranslatableName;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2.Iso31661Alpha2Data;

/**
 * Represents a ISO 3166-1 alpha-2 country.
 * Each country is associated with {@link Iso31661Alpha2Data}.
 */
public class Iso31661Alpha2 extends AbstractEntry<Iso31661Alpha2Referential, Iso31661Alpha2, Iso31661Alpha2Data>
      implements Entry<Iso31661Alpha2> {

   Iso31661Alpha2(Iso31661Alpha2Referential referential, Iso31661Alpha2Data entryData) {
      super(referential, entryData);
   }

   @Override
   public Iso31661Alpha2Code entryCode() {
      return entryData.iso31661Alpha2();
   }

   @Override
   public TranslatableName entryName() {
      return entryData.name();
   }

   /**
    * Defines ISO 3166-1 alpha-2 data.
    * Most fields are optional because of user-assigned codes.
    */
   public interface Iso31661Alpha2Data extends EntryData {

      /** Entry code. */
      Iso31661Alpha2Code iso31661Alpha2();

      /** Name translations for this entry. */
      TranslatableName name();

      /** Common name translations for this entry. */
      TranslatableName commonName();

      /** Official name translations for this entry. */
      TranslatableName officialName();

   }

}
