/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential;

/**
 * Error thrown if a {@link Entry} is not referenced in a {@link Referential}.
 */
public class UnknownEntryException extends IllegalArgumentException {
   private static final String DEFAULT_MESSAGE = "Unknown '%2$s' entry code in '%1$s' referential";

   private final ReferentialCode referentialcode;
   private final EntryCode entryCode;

   public UnknownEntryException(ReferentialCode referentialcode, EntryCode entryCode) {
      super(String.format(DEFAULT_MESSAGE, referentialcode.referentialCode(), entryCode.entryCode()));
      this.referentialcode = referentialcode;
      this.entryCode = entryCode;
   }

   public EntryCode entryCode() {
      return entryCode;
   }

   public ReferentialCode referentialcode() {
      return referentialcode;
   }
}
