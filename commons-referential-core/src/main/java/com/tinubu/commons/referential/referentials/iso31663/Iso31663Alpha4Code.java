/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.iso31663;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.referential.EntryCode;

/**
 * ISO 3166-3 alpha-4 country code.
 * Country code format is {@code <ALPHA><ALPHA><ALPHA><ALPHA>} in uppercase.
 */
public class Iso31663Alpha4Code extends EntryCode {

   private Iso31663Alpha4Code(String entryCode) {
      super(entryCode);
   }

   /**
    * Creates ISO 3166-3 alpha-4 code from entry code.
    * Entry code is case-insensitive.
    */
   public static Iso31663Alpha4Code of(String entryCode) {
      return new Iso31663Alpha4Code(notNull(entryCode, "entryCode").toUpperCase());
   }

   @Override
   protected boolean validEntryCodeFormat(String entryCode) {
      return entryCode.length() == 4 && entryCode.chars().allMatch(c -> c >= 'A' && c <= 'Z');
   }

   /* Generated codes at 2021-10-07T21:40:13+02:00 using default loader. */
   public static final Iso31663Alpha4Code AIDJ = Iso31663Alpha4Code.of("AIDJ");
   public static final Iso31663Alpha4Code ANHH = Iso31663Alpha4Code.of("ANHH");
   public static final Iso31663Alpha4Code BQAQ = Iso31663Alpha4Code.of("BQAQ");
   public static final Iso31663Alpha4Code BUMM = Iso31663Alpha4Code.of("BUMM");
   public static final Iso31663Alpha4Code BYAA = Iso31663Alpha4Code.of("BYAA");
   public static final Iso31663Alpha4Code CSHH = Iso31663Alpha4Code.of("CSHH");
   public static final Iso31663Alpha4Code CSXX = Iso31663Alpha4Code.of("CSXX");
   public static final Iso31663Alpha4Code CTKI = Iso31663Alpha4Code.of("CTKI");
   public static final Iso31663Alpha4Code DDDE = Iso31663Alpha4Code.of("DDDE");
   public static final Iso31663Alpha4Code DYBJ = Iso31663Alpha4Code.of("DYBJ");
   public static final Iso31663Alpha4Code FQHH = Iso31663Alpha4Code.of("FQHH");
   public static final Iso31663Alpha4Code FXFR = Iso31663Alpha4Code.of("FXFR");
   public static final Iso31663Alpha4Code GEHH = Iso31663Alpha4Code.of("GEHH");
   public static final Iso31663Alpha4Code HVBF = Iso31663Alpha4Code.of("HVBF");
   public static final Iso31663Alpha4Code JTUM = Iso31663Alpha4Code.of("JTUM");
   public static final Iso31663Alpha4Code MIUM = Iso31663Alpha4Code.of("MIUM");
   public static final Iso31663Alpha4Code NHVU = Iso31663Alpha4Code.of("NHVU");
   public static final Iso31663Alpha4Code NQAQ = Iso31663Alpha4Code.of("NQAQ");
   public static final Iso31663Alpha4Code NTHH = Iso31663Alpha4Code.of("NTHH");
   public static final Iso31663Alpha4Code PCHH = Iso31663Alpha4Code.of("PCHH");
   public static final Iso31663Alpha4Code PUUM = Iso31663Alpha4Code.of("PUUM");
   public static final Iso31663Alpha4Code PZPA = Iso31663Alpha4Code.of("PZPA");
   public static final Iso31663Alpha4Code RHZW = Iso31663Alpha4Code.of("RHZW");
   public static final Iso31663Alpha4Code SKIN = Iso31663Alpha4Code.of("SKIN");
   public static final Iso31663Alpha4Code SUHH = Iso31663Alpha4Code.of("SUHH");
   public static final Iso31663Alpha4Code TPTL = Iso31663Alpha4Code.of("TPTL");
   public static final Iso31663Alpha4Code VDVN = Iso31663Alpha4Code.of("VDVN");
   public static final Iso31663Alpha4Code WKUM = Iso31663Alpha4Code.of("WKUM");
   public static final Iso31663Alpha4Code YDYE = Iso31663Alpha4Code.of("YDYE");
   public static final Iso31663Alpha4Code YUCS = Iso31663Alpha4Code.of("YUCS");
   public static final Iso31663Alpha4Code ZRCD = Iso31663Alpha4Code.of("ZRCD");

}


