/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.loaders;

import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.tinubu.commons.referential.EntryData;
import com.tinubu.commons.referential.EntryDataLoader;

/**
 * Programmatically configurable generic referential loader.
 *
 * @param <D> entry data type managed by this loader
 */
public class ConfigurableReferentialLoader<D extends EntryData> implements EntryDataLoader<D> {

   private final List<D> entryDatas;
   private boolean changed = false;

   private ConfigurableReferentialLoader(ConfigurableReferentialLoaderBuilder<D> builder) {
      this.entryDatas = noNullElements(builder.entryDatas, "entryDatas");
   }

   @Override
   public List<D> loadEntryDatas() {
      changed = false;

      return entryDatas;
   }

   @Override
   public boolean hasChanged() {
      return changed;
   }

   public ConfigurableReferentialLoader<D> addEntryData(D entryDatas) {
      this.entryDatas.add(notNull(entryDatas, "entryDatas"));
      changed = true;

      return this;
   }

   public ConfigurableReferentialLoader<D> addEntryDatas(List<D> entryDatas) {
      this.entryDatas.addAll(noNullElements(entryDatas, "entryDatas"));
      changed = true;

      return this;
   }

   @SuppressWarnings("unchecked")
   public ConfigurableReferentialLoader<D> addEntryDatas(D... entryDatas) {
      return addEntryDatas(Arrays.asList(noNullElements(entryDatas, "entryDatas")));
   }

   public static class ConfigurableReferentialLoaderBuilder<D extends EntryData> {
      private List<D> entryDatas = new ArrayList<>();

      public ConfigurableReferentialLoaderBuilder<D> entryDatas(List<D> entryDatas) {
         this.entryDatas = new ArrayList<>(entryDatas);

         return this;
      }

      @SuppressWarnings("unchecked")
      public ConfigurableReferentialLoaderBuilder<D> entryDatas(D... entryDatas) {
         return entryDatas(Arrays.asList(noNullElements(entryDatas, "entryDatas")));
      }

      public ConfigurableReferentialLoaderBuilder<D> addEntryData(D entryDatas) {
         this.entryDatas.add(notNull(entryDatas, "entryDatas"));

         return this;
      }

      public ConfigurableReferentialLoaderBuilder<D> addEntryDatas(List<D> entryDatas) {
         this.entryDatas.addAll(noNullElements(entryDatas, "entryDatas"));

         return this;
      }

      @SuppressWarnings("unchecked")
      public ConfigurableReferentialLoaderBuilder<D> addEntryDatas(D... entryDatas) {
         return addEntryDatas(Arrays.asList(noNullElements(entryDatas, "entryDatas")));
      }

      public ConfigurableReferentialLoader<D> build() {
         return new ConfigurableReferentialLoader<>(this);
      }

   }
}
