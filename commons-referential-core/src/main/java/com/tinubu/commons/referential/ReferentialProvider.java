/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential;

/**
 * Defines the {@link Referential} registration information. Referentials are lazily loaded, so
 * their {@link #referentialCode() code} and {@link #referentialCode() class} must be known
 * prior to {@link #build instantiation}.
 */
public interface ReferentialProvider {

   /**
    * Unique {@link ReferentialCode referential code} used for instantiation. The same referential can be
    * registered with different codes, if the loader differs for example.
    *
    * @return provided referential code
    */
   ReferentialCode<? extends Entry<?>, ? extends Referential<?>> referentialCode();

   /**
    * {@link Referential} class instantiated by this provider.
    *
    * @return provided referential class
    */
   Class<? extends Referential<?>> referentialClass();

   /**
    * Builds a {@link Referential} instance.
    *
    * @return new provided referential instance
    */
   Referential<?> build();

}
