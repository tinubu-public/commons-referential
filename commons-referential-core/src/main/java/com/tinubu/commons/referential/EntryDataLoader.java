/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.function.Predicate;

/**
 * Generic loader of {@link EntryData}. The loader implementations are uncorrelated with {@link
 * Referential}, a given referential will only require entry data type to match its requirements.
 * A single loader can be used by several referentials depending on implementation. Multiple loaders can
 * be used to instantiate the same referential, to customize data access method for example.
 *
 * @param <D> entry data type
 */
public interface EntryDataLoader<D extends EntryData> {

   /**
    * Loads a list of entry data.
    *
    * @return a list of entry data
    *
    * @throws EntryDataLoadingException if loading fails for any reason
    */
   List<D> loadEntryDatas();

   /**
    * Loads a list of entry data with convenient filtering.
    *
    * @return a list of entry data
    *
    * @throws EntryDataLoadingException if loading fails for any reason
    */
   default List<D> loadEntryDatas(Predicate<D> entryDataFilter) {
      notNull(entryDataFilter, "entryDataFilter");

      return loadEntryDatas().stream().filter(entryDataFilter).collect(toList());
   }

   /**
    * Returns {@code true} if data has changed in the underlying data provider.
    *
    * @return {@code true} if data has changed in the underlying data provider
    */
   boolean hasChanged();

}
