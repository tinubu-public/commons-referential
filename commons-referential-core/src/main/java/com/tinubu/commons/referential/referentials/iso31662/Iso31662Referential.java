/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.iso31662;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.referential.AbstractReferential;
import com.tinubu.commons.referential.EntryDataLoader;
import com.tinubu.commons.referential.Referential;
import com.tinubu.commons.referential.ReferentialCode;
import com.tinubu.commons.referential.referentials.iso31662.Iso31662.Iso31662Data;

/**
 * ISO 3166-2 country referential.
 */
public class Iso31662Referential extends AbstractReferential<Iso31662, Iso31662Data> implements Referential<Iso31662> {

   public static final ReferentialCode<Iso31662, Iso31662Referential> DEFAULT_REFERENTIAL_CODE =
         ReferentialCode.of("ISO_31662");

   private static final String DISPLAY_NAME = "ISO 3166-2";

   private Iso31662Referential(ReferentialCode<Iso31662, Iso31662Referential> referentialCode,
                               EntryDataLoader<Iso31662Data> entryDataLoader) {
      super(referentialCode, entryDataLoader);
   }

   @Override
   public String displayName() {
      return DISPLAY_NAME;
   }

   @Override
   protected Iso31662 loadEntry(Iso31662Data entryData) {
      notNull(entryData, "entryData");

      return new Iso31662(this, entryData);
   }

   public static class Iso31662ReferentialBuilder {

      private ReferentialCode<Iso31662, Iso31662Referential> referentialCode = DEFAULT_REFERENTIAL_CODE;
      private EntryDataLoader<Iso31662Data> entryDataLoader;

      public Iso31662ReferentialBuilder referentialCode(ReferentialCode<Iso31662, Iso31662Referential> referentialCode) {
         this.referentialCode = referentialCode;
         return this;
      }

      public Iso31662ReferentialBuilder referentialLoader(EntryDataLoader<Iso31662Data> entryDataLoader) {
         this.entryDataLoader = entryDataLoader;
         return this;
      }

      public ReferentialCode<Iso31662, Iso31662Referential> referentialCode() {
         return referentialCode;
      }

      public Class<Iso31662Referential> referentialClass() {
         return Iso31662Referential.class;
      }

      public Iso31662Referential build() {
         return new Iso31662Referential(referentialCode, entryDataLoader);
      }
   }

}
